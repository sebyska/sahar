let slideSettings = {
    slidesToShow: 2,
    slidesToScroll: 1,
    centerPadding: '20px',
    draggable: false,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                vertical: true,
                centerMode: true,
                arrows: false,
                verticalSwiping: true,
                infinite: false
            }
        }
    ]
};

/*function cocoInit(cocoenItems) {
    cocoenItems.each(function (index, elem) {
        if (!$(elem).find('.cocoen-drag').length) {
            console.log('init cocoen');
            new Cocoen(elem);
        }
    });
}*/


(function ($) {
    $(function () {
        $('.slider-items').slick(slideSettings);
        /*let mySwiper = new Swiper('.swiper-container', {
            loop: true,
            simulateTouch: false,
            width: 390,
            height: 370,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });*/

        if ($('.metro-list').length) {
            $('.metro-list').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                centerMode: true,
                variableWidth: true,
                autoplay: true,
                autoplaySpeed: 1500,
                arrows: false
            });
        }
        $('.works-block_body .tab-links').on('click', function (event) {
            console.log(event);
            $('.slider-items').slick('unslick').slick(slideSettings);
        });

        $(".b_a_slider").twentytwenty({
            before_label: 'До',
            after_label: 'После',
        });
    });
}(jQuery));