$(window).scroll(function(){
	var PW = $(window).scrollTop();
	var posTM = $('.cont_logo').height();
	if(PW > posTM){
		$('.cont_menu').addClass('fix_top_menu');
	}else{
		$('.cont_menu').removeClass('fix_top_menu');
	};
});

$(window).ready(function(){
	/* script for tabs */
	$('.my_tabs_ul > li').click(function(){
		if($(this).hasClass('active') == false){
			$('.my_tabs_ul > li').removeClass('active');
			$('.my_tabs_cont > div').removeClass('active');
			var id_tab = $(this).attr('id');
			if(id_tab){
				$('.my_tabs_ul > li#' + id_tab).addClass('active');
				$('.my_tabs_cont > div#my_tabs_cont_' + id_tab).addClass('active');
				$('body,html').animate({scrollTop: 523}, 1000);
				return false;
			};
		};
	});
	$('#show_all_video_reviews').click(function(){
		$('#video_reviews').click();
		$('body,html').animate({scrollTop: 523}, 1500);
		return false;
	});
	$('#show_all_text_reviews').click(function(){
		$('#text_reviews').click();
		$('body,html').animate({scrollTop: 523}, 3000);
		return false;
	});
	$('.show_all_all_reviews').click(function(){
		$('#all_reviews').click();
		$('body,html').animate({scrollTop: 523}, 2000);
		return false;
	});
	
	/* sctipt for modal */
	$('.modal_open').click(function(){
		var modal_class = $(this).attr('id');
		$('#my_modal_' + modal_class).addClass('open');
	});
	$('.form_close').click(function(){
		$('.my_modal').removeClass('open');
	});
	$('.call_form_close').click(function(){
		$('.my_modal').removeClass('open');
	});
	$('.my_modal_fon').click(function(){
		$('.my_modal').removeClass('open');
	});
	
	$('#fp_polite_check').click(function(){
		if($('.fp_polite_input').prop("checked")){
			$('.fp_polite_input').prop('checked', false);
			$('.fp_polite_input').removeAttr("checked");
			$('#fp_polite_check').removeClass("active");
		}else{
			$('.fp_polite_input').prop('checked', true);
			$('.fp_polite_input').attr("checked","checked");
			$('#fp_polite_check').addClass("active");
		};
	});
	
	$('#pre_submit_banner').click(function(){
		var fpf_name = $('#fpf_name').val();
		var fpf_phone = $('#fpf_phone').val();
		if($('#fp_polite_check').hasClass("active")){
			$('#fp_polite_check').removeClass('error');
		}else{
			$('#fp_polite_check').addClass('error');
		};
		if(fpf_name == ''){
			$('#fpf_name').addClass('error');
		}else{
			$('#fpf_name').removeClass('error');
		};
		if(fpf_phone == ''){
			$('#fpf_phone').addClass('error');
		}else{
			$('#fpf_phone').removeClass('error');
		};
		if($('#fp_polite_check').hasClass("active") && fpf_phone != '' && fpf_name != ''){
			$('#submit_banner').click();
		};
	});
	
	$('#fp_polite_check_cf').click(function(){
		if($('.fp_polite_input_cf').prop("checked")){
			$('.fp_polite_input_cf').prop('checked', false);
			$('.fp_polite_input_cf').removeAttr("checked");
			$('#fp_polite_check_cf').removeClass("active");
		}else{
			$('.fp_polite_input_cf').prop('checked', true);
			$('.fp_polite_input_cf').attr("checked","checked");
			$('#fp_polite_check_cf').addClass("active");
		};
	});
	
	$('#pre_submit_banner_cf').click(function(){
		var fpf_name_cf = $('#fpf_name_cf').val();
		var fpf_phone_cf = $('#fpf_phone_cf').val();
		if($('#fp_polite_check_cf').hasClass("active")){
			$('#fp_polite_check_cf').removeClass('error');
		}else{
			$('#fp_polite_check_cf').addClass('error');
		};
		if(fpf_name_cf == ''){
			$('#fpf_name_cf').addClass('error');
		}else{
			$('#fpf_name_cf').removeClass('error');
		};
		if(fpf_phone_cf == ''){
			$('#fpf_phone_cf').addClass('error');
		}else{
			$('#fpf_phone_cf').removeClass('error');
		};
		if($('#fp_polite_check_cf').hasClass("active") && fpf_phone_cf != '' && fpf_name_cf != ''){
			$('#submit_banner_cf').click();
		};
	});
	
	$('.open_mobile_top_menu_h').click(function(){
		$(this).toggleClass('active');
		$('.mobile_top_menu_h').toggleClass('active');
	});
	
	$('.top_menu li').click(function(){
		$(this).toggleClass('active');
		$(this).find('ul').toggleClass('active');
	});
});


































