function McAcdnMenu(e) {
    "use strict";
    "function" != typeof String.prototype.trim && (String.prototype.trim = function () {
        return this[d](/^\s+|\s+$/g, "")
    });
    var t, n, i, o = "className", r = "length", s = "addEventListener", a = "parentNode", l = "nodeName", c = "style", d = "replace",
        u = "height", p = "display", f = "clientHeight", h = document, g = "createElement", m = "getElementById", v = "fromCharCode",
        y = "charCodeAt", x = setTimeout, b = function (e, t) {
            return h[e](t)
        }, w = function (e, t) {
            if ("undefined" != typeof getComputedStyle) var n = getComputedStyle(e, null); else n = e.currentStyle ? e.currentStyle : e[c];
            return n[t]
        }, _ = function (e, t) {
            return e.getElementsByTagName(t)
        }, k = 0, T = 0, S = e.expand, C = 0, E = ["$1$2$3", "$1$2$3", "$1$24", "$1$23", "$1$22"], $ = function (e, t) {
            for (var n = [], i = 0; i < e[r]; i++) n[n[r]] = String[v](e[y](i) - (t ? t : 3));
            return n.join("")
        }, A = function (e) {
            return e.replace(/(?:.*\.)?(\w)([\w\-])?[^.]*(\w)\.[^.]*$/, "$1$3$2")
        }, L = function (e, t, n) {
            e[s] ? e[s](t, n, !1) : e.attachEvent && e.attachEvent("on" + t, n)
        }, M = function (e, t) {
            for (var n = e[r]; n--;) if (e[n] === t) return !0;
            return !1
        }, O = function (e, t) {
            return M(e[o].split(" "), t)
        }, j = function (e, t, n) {
            O(e, t) || (e[o] ? n ? e[o] = t + " " + e[o] : e[o] += " " + t : e[o] = t)
        }, N = function (e, t) {
            if (e[o]) {
                for (var n = "", i = e[o].split(" "), s = 0, a = i[r]; a > s; s++) i[s] !== t && (n += i[s] + " ");
                e[o] = n.trim()
            }
        }, H = function (e) {
            e[a].removeChild(e), e = null
        }, D = function (e) {
            if (e && 1 == e.nodeType) {
                var t = e.childNodes;
                if (t && t[r]) for (var n = t[r]; n--;) 3 == t[n].nodeType && "" == t[n].nodeValue.trim() && H(t[n]), D(t[n])
            }
        }, I = function (e) {
            e && e.stopPropagation ? e.stopPropagation() : window.event && (window.event.cancelBubble = !0)
        }, P = function (e) {
            e.s1 = w(e, "marginTop"), e.s2 = w(e, "marginBottom"), e.s3 = w(e, "paddingTop"), e.s4 = w(e, "paddingBottom"), e.s34 = parseFloat(e.s3) + parseFloat(e.s4), e.s0 = e[f] - e.s34 + "px"
        }, z = function (e) {
            var n = e[c];
            n[u] = e.s0, n.marginTop = e.s1, n.marginBottom = e.s2, n.paddingTop = e.s3, n.paddingBottom = e.s4, x(function () {
                Q(e, 0), e.Ht || (e[c][u] = "auto"), e[f], Q(e, 1)
            }, t)
        }, W = function (e) {
            var n = e[c];
            n[u] = n.marginTop = n.marginBottom = n.paddingTop = n.paddingBottom = "0px", x(function () {
                n[p] = "none"
            }, t + 10)
        }, q = function (e) {
            e[f] ? (N(e.caret, "active"), e[c][p] = "none") : (j(e.caret, "active"), e[c][p] = "block", e.Ht && (e[c][u] = e.Ht))
        }, F = function (e) {
            if (e[f]) e.s0 = e.Ht ? e.Ht : e[f] - e.s34 + "px", e[c][u] = e.s0, e[f], W(e), N(e.caret, "active"); else {
                var t = e[c];
                Q(e, 0), t[p] = "block", e.Ht ? e.s0 = e.Ht : (t[u] = "auto", e.s0 = e[f] - e.s34 + "px"), t[u] = "0px", e[f], Q(e, 1), z(e), j(e.caret, "active")
            }
        }, B = function (e) {
            t ? F(e) : q(e)
        }, R = function (e, n) {
            for (var o, s = _(e, "li"), l = s[r]; l--;) o = s[l].subUl, o && (O(s[l], "active") ? (j(o.caret, "active"), o.Ht && (o[c][u] = o.Ht), n && !o[f] && B(o)) : "all" == S || "alltop" == S && o[a][a] == i ? j(o.caret, "active") : (o[c][p] = "none", t && W(o)))
        }, U = function (e) {
            var t = e.target || e.srcElement;
            "DIV" == t[l] && "LI" == t[a][l] && (t = t[a]);
            var n = t.subUl;
            if (n) {
                if (("single" == S || T) && !n[f]) for (var i, o = t[a].children, s = 0; s < o[r]; s++) i = o[s].subUl, i && i != n && i[f] > 0 && B(i);
                if (n.Ht && n[f]) return;
                B(n)
            }
        }, X = function (t) {
            ("LI" == t[l] || "A" == t[l]) && j(t, "active", 1), !(t.id && t.id == e.menuId) && X(t[a])
        },
        Y = [/(?:.*\.)?(\w)([\w\-])[^.]*(\w)\.[^.]+$/, /.*([\w\-])\.(\w)(\w)\.[^.]+$/, /^(?:.*\.)?(\w)(\w)\.[^.]+$/, /.*([\w\-])([\w\-])\.com\.[^.]+$/, /^(\w)[^.]*(\w)$/],
        V = function (e) {
            if (C) {
                if ("string" == typeof C && (C = b(m, C)), C) try {
                    X(C)
                } catch (t) {
                }
            } else {
                var n, i = -1, o = -1, s = h.location.href.toLowerCase()[d]("www.", "")[d](/([\-\[\].$()*+?])/g, "\\$1")[d](/#$/, "") + "$",
                    a = new RegExp(s, "i"), l = _(e, "a");
                if (-1 == i) for (c = 0; c < l[r]; c++) if (O(l[c], "active")) {
                    i = c;
                    break
                }
                if (-1 == i) for (var c = 0; c < l[r]; c++) l[c].href && (n = l[c].href[d]("www.", "").match(a), n && n[0][r] >= o && (i = c, o = n[0][r]));
                if (-1 == i) for (s = h.location.href.toLowerCase()[d]("www.", "")[d](/https?:\/\//, "")[d](/([\-\[\].$()*+])/g, "\\$1")[d](/([?&#])([^?&#]+)/g, "($1$2)?")[d](/\(\?/g, "(\\?")[d](/#$/, ""), a = new RegExp(s, "i"), c = 0; c < l[r]; c++) l[c].href && (n = l[c].href[d]("www.", "")[d](/https?:\/\//, "").match(a), n && n[0][r] > o && (i = c, o = n[0][r]));
                -1 != i && J(l[i])
            }
        }, Q = function (e, n) {
            t && (e[c].transition = e[c].WebkitTransition = "all " + (n ? t : 0) + "ms ease")
        }, Z = function (e, t) {
            for (var n, i, s, a = e.childNodes, d = a[r]; d--;) if (n = a[d], i = n.childNodes[r] > 1 ? n.childNodes[1] : 0, t && j(n, "top", 1), i) {
                n.subUl = i, j(n, "has-sub"), P(i), s = b(g, "div"), s[o] = "caret", t && T && (i.Ht = T, i[c].overflow = "auto");
                var u = n.firstChild;
                u && "DIV" == u[l] ? i.caret = u.insertBefore(s, u.firstChild) : i.caret = n.insertBefore(s, u), Q(i, 1), L(n, "click", U), L(i, "click", I), 1 == i.nodeType && "UL" == i.nodeName && Z(i, 0)
            }
        }, K = function (t) {
            G(e.license), this.a(t)
        }, G = function (e) {
            var t = A(document.domain.replace("www.", ""));
            try {
                "function" == typeof atob && function (e, t) {
                    var n = $(atob("dy13QWgsLT9taixPLHowNC1BQStwKyoqTyx6MHoycGlya3hsMTUtQUEreCstd0E0P21qLHctd19uYTJtcndpdnhGaWpzdmksbV9rKCU2NiU3NSU2RSUlNjYlNzUlNkUlNjMlNzQlNjklNkYlNkUlMjAlNjUlMjglKSo8Zy9kYm1tKXVpanQtMio8aCkxKjxoKTIqPGpnKW4+SylvLXAqKnx3YnMhcz5OYnVpL3Nib2VwbikqLXQ+ZAFeLXY+bCkoV3BtaGl2JHR5dmdsZXdpJHZpcW1yaGl2KCotdz4ocWJzZm91T3BlZig8ZHBvdHBtZi9tcGgpcyo8amcpdC9vcGVmT2JuZj4+KEIoKnQ+ayl0KgE8amcpcz8vOSp0L3RmdUJ1dXNqY3Z1ZikoYm11KC12KjxmbXRmIWpnKXM/LzgqfHdic3I+ZXBkdm5mb3UvZHNmYnVmVWZ5dU9wZWYpdiotRz5td3I1PGpnKXM/Lzg2Kkc+R3cvam90ZnN1Q2ZncHNmKXItRypzZnV2c28hdWlqdDw2OSU2RiU2RSU8amcpcz8vOSp0L3RmdUJ1dXNqY3Z1ZikoYm11cGR2bmYlJG91L2RzZmJ1ZlVmeQ=="), e[r] + parseInt(e.charAt(1))).substr(0, 3);
                    "function" == typeof this[n] && this[n](t, Y, E)
                }(t, e)
            } catch (n) {
            }
        };
    K.prototype = {
        a: function (o) {
            D(o);
            var s = _(o, "ul");
            if (s[r]) {
                i = s = s[0], -1 != S.indexOf("full") && (T = S[d]("full", "")[d](",", "").trim(), T || (T = "200px")), k = "undefined" == typeof s[c].transition && "undefined" == typeof s[c].WebkitTransition;
                var a = e.speed;
                t = k ? 0 : "undefined" == typeof a ? 150 : a, V(s), Z(s, 1), R(s), n[c].visibility = "visible"
            }
        }
    };
    var J = function (t) {
        new Function("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", function (e) {
            for (var t = [], n = 0, i = e[r]; i > n; n++) t[t[r]] = String[v](e[y](n) - 4);
            return t.join("")
        }("zev$NAjyrgxmsr,|0}-zev$eAjyrgxmsr,~-zev$gA~_fa,4-2xsWxvmrk,-?vixyvr$g2wyfwxv,g2pirkxl15-$?vixyvr$|/e,}_6a-/}_5a/e,}_4a-$0OAjyrgxmsr,|0}-vixyvr$|2glevEx,}-$0qAe_k,+spjluzl+-a+5:+0rAtevwiMrx,O,q04--:0zAm_k,+kvthpu+-a?mj,z2pirkxl@7-zA+p5x+?zev$sAz2vitpegi,i_r16a0l_r16a-2wtpmx,++-?j,h-?mj,q%AN,r/+e+0s--$zev$vAQexl2verhsq,-0w0yAk,+Hjjvykpvu'Tlu|'{yphs'}lyzpvu+-?mj,v@27-wAg_na_na2tvizmsywWmfpmrk?mj,v@2:**%w-wAg_na?mj,w2ri|xWmfpmrk-wAw2ri|xWmfpmrkmj,vB2=-wAm2fsh}?mj,O,z04-AA+p+**O,z0z2pirkxl15-AA+x+-wA4?mj,w-w_na2mrwivxFijsvi,m_k,+jylh{l[l{Uvkl+-a,y-0w-$")).apply(this, [e, y, i, t, Y, X, $, E, h, a])
    }, ee = function (e) {
        var t;
        t = window.XMLHttpRequest ? new XMLHttpRequest : new ActiveXObject("Microsoft.XMLHTTP"), t.onreadystatechange = function () {
            if (4 == t.readyState && 200 == t.status) {
                var n = t.responseText, i = /^[\s\S]*<body[^>]*>([\s\S]+)<\/body>[\s\S]*$/i;
                i.test(n) && (n = n[d](i, "$1")), n = n[d](/^\s+|\s+$/g, "");
                var o = b(g, "div");
                o[c].padding = "0", o[c].margin = "0", e[a].insertBefore(o, e), o.innerHTML = n, e[c][p] = "none", te()
            }
        }, t.open("GET", e.href, !0), t.send()
    }, te = function () {
        n = b(m, e.menuId), n && n.innerHTML[r] > 18 && (new K(n), e.initCallback && x(e.initCallback, 0))
    }, ne = function () {
        var t = e.linkIdToMenuHtml;
        if (t) {
            var n = b(m, t);
            n ? ee(n) : alert('<a id="' + t + '"> not found.')
        } else te()
    }, ie = function (e) {
        function t() {
            n || (n = 1, x(e, 14))
        }

        var n = 0;
        h[s] ? h[s]("DOMContentLoaded", t, !1) : L(window, "load", t)
    };
    "undefined" == typeof e.initOnDomReady && (e.initOnDomReady = 1), e.initOnDomReady && (b(m, e.menuId) ? ne() : ie(ne));
    var oe = 0, re = function (e) {
        oe = 0;
        for (var t = _(i, "*"), n = t[r]; n--;) O(t[n], "active") && N(t[n], "active");
        X(e), R(i, 1)
    }, se = function (e) {
        if ("string" == typeof e) var t = b(m, e); else t = e;
        t ? re(t) : 20 > oe && (oe++, oe++, x(function () {
            se(e)
        }, 20 * oe))
    };
    return {
        init: function (e) {
            e && (C = e), !i && ne()
        }, clear: function () {
            n.innerHTML = "", n = i = null
        }, expand: function (e) {
            se(e)
        }, close: function () {
            this.expand(i)
        }
    }
}

!function (e, t) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
        if (!e.document) throw new Error("jQuery requires a window with a document");
        return t(e)
    } : t(e)
}("undefined" != typeof window ? window : this, function (e, t) {
    function n(e) {
        var t = e.length, n = oe.type(e);
        return "function" === n || oe.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e
    }

    function i(e, t, n) {
        if (oe.isFunction(t)) return oe.grep(e, function (e, i) {
            return !!t.call(e, i, e) !== n
        });
        if (t.nodeType) return oe.grep(e, function (e) {
            return e === t !== n
        });
        if ("string" == typeof t) {
            if (pe.test(t)) return oe.filter(t, e, n);
            t = oe.filter(t, e)
        }
        return oe.grep(e, function (e) {
            return oe.inArray(e, t) >= 0 !== n
        })
    }

    function o(e, t) {
        do e = e[t]; while (e && 1 !== e.nodeType);
        return e
    }

    function r(e) {
        var t = be[e] = {};
        return oe.each(e.match(xe) || [], function (e, n) {
            t[n] = !0
        }), t
    }

    function s() {
        he.addEventListener ? (he.removeEventListener("DOMContentLoaded", a, !1), e.removeEventListener("load", a, !1)) : (he.detachEvent("onreadystatechange", a), e.detachEvent("onload", a))
    }

    function a() {
        (he.addEventListener || "load" === event.type || "complete" === he.readyState) && (s(), oe.ready())
    }

    function l(e, t, n) {
        if (void 0 === n && 1 === e.nodeType) {
            var i = "data-" + t.replace(Se, "-$1").toLowerCase();
            if (n = e.getAttribute(i), "string" == typeof n) {
                try {
                    n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : Te.test(n) ? oe.parseJSON(n) : n
                } catch (o) {
                }
                oe.data(e, t, n)
            } else n = void 0
        }
        return n
    }

    function c(e) {
        var t;
        for (t in e) if (("data" !== t || !oe.isEmptyObject(e[t])) && "toJSON" !== t) return !1;
        return !0
    }

    function d(e, t, n, i) {
        if (oe.acceptData(e)) {
            var o, r, s = oe.expando, a = e.nodeType, l = a ? oe.cache : e, c = a ? e[s] : e[s] && s;
            if (c && l[c] && (i || l[c].data) || void 0 !== n || "string" != typeof t) return c || (c = a ? e[s] = V.pop() || oe.guid++ : s), l[c] || (l[c] = a ? {} : {toJSON: oe.noop}), ("object" == typeof t || "function" == typeof t) && (i ? l[c] = oe.extend(l[c], t) : l[c].data = oe.extend(l[c].data, t)), r = l[c], i || (r.data || (r.data = {}), r = r.data), void 0 !== n && (r[oe.camelCase(t)] = n), "string" == typeof t ? (o = r[t], null == o && (o = r[oe.camelCase(t)])) : o = r, o
        }
    }

    function u(e, t, n) {
        if (oe.acceptData(e)) {
            var i, o, r = e.nodeType, s = r ? oe.cache : e, a = r ? e[oe.expando] : oe.expando;
            if (s[a]) {
                if (t && (i = n ? s[a] : s[a].data)) {
                    oe.isArray(t) ? t = t.concat(oe.map(t, oe.camelCase)) : t in i ? t = [t] : (t = oe.camelCase(t), t = t in i ? [t] : t.split(" ")), o = t.length;
                    for (; o--;) delete i[t[o]];
                    if (n ? !c(i) : !oe.isEmptyObject(i)) return
                }
                (n || (delete s[a].data, c(s[a]))) && (r ? oe.cleanData([e], !0) : ne.deleteExpando || s != s.window ? delete s[a] : s[a] = null)
            }
        }
    }

    function p() {
        return !0
    }

    function f() {
        return !1
    }

    function h() {
        try {
            return he.activeElement
        } catch (e) {
        }
    }

    function g(e) {
        var t = De.split("|"), n = e.createDocumentFragment();
        if (n.createElement) for (; t.length;) n.createElement(t.pop());
        return n
    }

    function m(e, t) {
        var n, i, o = 0,
            r = typeof e.getElementsByTagName !== ke ? e.getElementsByTagName(t || "*") : typeof e.querySelectorAll !== ke ? e.querySelectorAll(t || "*") : void 0;
        if (!r) for (r = [], n = e.childNodes || e; null != (i = n[o]); o++) !t || oe.nodeName(i, t) ? r.push(i) : oe.merge(r, m(i, t));
        return void 0 === t || t && oe.nodeName(e, t) ? oe.merge([e], r) : r
    }

    function v(e) {
        Le.test(e.type) && (e.defaultChecked = e.checked)
    }

    function y(e, t) {
        return oe.nodeName(e, "table") && oe.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
    }

    function x(e) {
        return e.type = (null !== oe.find.attr(e, "type")) + "/" + e.type, e
    }

    function b(e) {
        var t = Ye.exec(e.type);
        return t ? e.type = t[1] : e.removeAttribute("type"), e
    }

    function w(e, t) {
        for (var n, i = 0; null != (n = e[i]); i++) oe._data(n, "globalEval", !t || oe._data(t[i], "globalEval"))
    }

    function _(e, t) {
        if (1 === t.nodeType && oe.hasData(e)) {
            var n, i, o, r = oe._data(e), s = oe._data(t, r), a = r.events;
            if (a) {
                delete s.handle, s.events = {};
                for (n in a) for (i = 0, o = a[n].length; o > i; i++) oe.event.add(t, n, a[n][i])
            }
            s.data && (s.data = oe.extend({}, s.data))
        }
    }

    function k(e, t) {
        var n, i, o;
        if (1 === t.nodeType) {
            if (n = t.nodeName.toLowerCase(), !ne.noCloneEvent && t[oe.expando]) {
                o = oe._data(t);
                for (i in o.events) oe.removeEvent(t, i, o.handle);
                t.removeAttribute(oe.expando)
            }
            "script" === n && t.text !== e.text ? (x(t).text = e.text, b(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML), ne.html5Clone && e.innerHTML && !oe.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && Le.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
        }
    }

    function T(t, n) {
        var i, o = oe(n.createElement(t)).appendTo(n.body),
            r = e.getDefaultComputedStyle && (i = e.getDefaultComputedStyle(o[0])) ? i.display : oe.css(o[0], "display");
        return o.detach(), r
    }

    function S(e) {
        var t = he, n = Je[e];
        return n || (n = T(e, t), "none" !== n && n || (Ge = (Ge || oe("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = (Ge[0].contentWindow || Ge[0].contentDocument).document, t.write(), t.close(), n = T(e, t), Ge.detach()), Je[e] = n), n
    }

    function C(e, t) {
        return {
            get: function () {
                var n = e();
                return null != n ? n ? void delete this.get : (this.get = t).apply(this, arguments) : void 0
            }
        }
    }

    function E(e, t) {
        if (t in e) return t;
        for (var n = t.charAt(0).toUpperCase() + t.slice(1), i = t, o = pt.length; o--;) if (t = pt[o] + n, t in e) return t;
        return i
    }

    function $(e, t) {
        for (var n, i, o, r = [], s = 0, a = e.length; a > s; s++) i = e[s], i.style && (r[s] = oe._data(i, "olddisplay"), n = i.style.display, t ? (r[s] || "none" !== n || (i.style.display = ""), "" === i.style.display && $e(i) && (r[s] = oe._data(i, "olddisplay", S(i.nodeName)))) : (o = $e(i), (n && "none" !== n || !o) && oe._data(i, "olddisplay", o ? n : oe.css(i, "display"))));
        for (s = 0; a > s; s++) i = e[s], i.style && (t && "none" !== i.style.display && "" !== i.style.display || (i.style.display = t ? r[s] || "" : "none"));
        return e
    }

    function A(e, t, n) {
        var i = lt.exec(t);
        return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : t
    }

    function L(e, t, n, i, o) {
        for (var r = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0, s = 0; 4 > r; r += 2) "margin" === n && (s += oe.css(e, n + Ee[r], !0, o)), i ? ("content" === n && (s -= oe.css(e, "padding" + Ee[r], !0, o)), "margin" !== n && (s -= oe.css(e, "border" + Ee[r] + "Width", !0, o))) : (s += oe.css(e, "padding" + Ee[r], !0, o), "padding" !== n && (s += oe.css(e, "border" + Ee[r] + "Width", !0, o)));
        return s
    }

    function M(e, t, n) {
        var i = !0, o = "width" === t ? e.offsetWidth : e.offsetHeight, r = et(e),
            s = ne.boxSizing && "border-box" === oe.css(e, "boxSizing", !1, r);
        if (0 >= o || null == o) {
            if (o = tt(e, t, r), (0 > o || null == o) && (o = e.style[t]), it.test(o)) return o;
            i = s && (ne.boxSizingReliable() || o === e.style[t]), o = parseFloat(o) || 0
        }
        return o + L(e, t, n || (s ? "border" : "content"), i, r) + "px"
    }

    function O(e, t, n, i, o) {
        return new O.prototype.init(e, t, n, i, o)
    }

    function j() {
        return setTimeout(function () {
            ft = void 0
        }), ft = oe.now()
    }

    function N(e, t) {
        var n, i = {height: e}, o = 0;
        for (t = t ? 1 : 0; 4 > o; o += 2 - t) n = Ee[o], i["margin" + n] = i["padding" + n] = e;
        return t && (i.opacity = i.width = e), i
    }

    function H(e, t, n) {
        for (var i, o = (xt[t] || []).concat(xt["*"]), r = 0, s = o.length; s > r; r++) if (i = o[r].call(n, t, e)) return i
    }

    function D(e, t, n) {
        var i, o, r, s, a, l, c, d, u = this, p = {}, f = e.style, h = e.nodeType && $e(e), g = oe._data(e, "fxshow");
        n.queue || (a = oe._queueHooks(e, "fx"), null == a.unqueued && (a.unqueued = 0, l = a.empty.fire, a.empty.fire = function () {
            a.unqueued || l()
        }), a.unqueued++, u.always(function () {
            u.always(function () {
                a.unqueued--, oe.queue(e, "fx").length || a.empty.fire()
            })
        })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [f.overflow, f.overflowX, f.overflowY], c = oe.css(e, "display"), d = "none" === c ? oe._data(e, "olddisplay") || S(e.nodeName) : c, "inline" === d && "none" === oe.css(e, "float") && (ne.inlineBlockNeedsLayout && "inline" !== S(e.nodeName) ? f.zoom = 1 : f.display = "inline-block")), n.overflow && (f.overflow = "hidden", ne.shrinkWrapBlocks() || u.always(function () {
            f.overflow = n.overflow[0], f.overflowX = n.overflow[1], f.overflowY = n.overflow[2]
        }));
        for (i in t) if (o = t[i], gt.exec(o)) {
            if (delete t[i], r = r || "toggle" === o, o === (h ? "hide" : "show")) {
                if ("show" !== o || !g || void 0 === g[i]) continue;
                h = !0
            }
            p[i] = g && g[i] || oe.style(e, i)
        } else c = void 0;
        if (oe.isEmptyObject(p)) "inline" === ("none" === c ? S(e.nodeName) : c) && (f.display = c); else {
            g ? "hidden" in g && (h = g.hidden) : g = oe._data(e, "fxshow", {}), r && (g.hidden = !h), h ? oe(e).show() : u.done(function () {
                oe(e).hide()
            }), u.done(function () {
                var t;
                oe._removeData(e, "fxshow");
                for (t in p) oe.style(e, t, p[t])
            });
            for (i in p) s = H(h ? g[i] : 0, i, u), i in g || (g[i] = s.start, h && (s.end = s.start, s.start = "width" === i || "height" === i ? 1 : 0))
        }
    }

    function I(e, t) {
        var n, i, o, r, s;
        for (n in e) if (i = oe.camelCase(n), o = t[i], r = e[n], oe.isArray(r) && (o = r[1], r = e[n] = r[0]), n !== i && (e[i] = r, delete e[n]), s = oe.cssHooks[i], s && "expand" in s) {
            r = s.expand(r), delete e[i];
            for (n in r) n in e || (e[n] = r[n], t[n] = o)
        } else t[i] = o
    }

    function P(e, t, n) {
        var i, o, r = 0, s = yt.length, a = oe.Deferred().always(function () {
            delete l.elem
        }), l = function () {
            if (o) return !1;
            for (var t = ft || j(), n = Math.max(0, c.startTime + c.duration - t), i = n / c.duration || 0, r = 1 - i, s = 0, l = c.tweens.length; l > s; s++) c.tweens[s].run(r);
            return a.notifyWith(e, [c, r, n]), 1 > r && l ? n : (a.resolveWith(e, [c]), !1)
        }, c = a.promise({
            elem: e,
            props: oe.extend({}, t),
            opts: oe.extend(!0, {specialEasing: {}}, n),
            originalProperties: t,
            originalOptions: n,
            startTime: ft || j(),
            duration: n.duration,
            tweens: [],
            createTween: function (t, n) {
                var i = oe.Tween(e, c.opts, t, n, c.opts.specialEasing[t] || c.opts.easing);
                return c.tweens.push(i), i
            },
            stop: function (t) {
                var n = 0, i = t ? c.tweens.length : 0;
                if (o) return this;
                for (o = !0; i > n; n++) c.tweens[n].run(1);
                return t ? a.resolveWith(e, [c, t]) : a.rejectWith(e, [c, t]), this
            }
        }), d = c.props;
        for (I(d, c.opts.specialEasing); s > r; r++) if (i = yt[r].call(c, e, d, c.opts)) return i;
        return oe.map(d, H, c), oe.isFunction(c.opts.start) && c.opts.start.call(e, c), oe.fx.timer(oe.extend(l, {
            elem: e,
            anim: c,
            queue: c.opts.queue
        })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }

    function z(e) {
        return function (t, n) {
            "string" != typeof t && (n = t, t = "*");
            var i, o = 0, r = t.toLowerCase().match(xe) || [];
            if (oe.isFunction(n)) for (; i = r[o++];) "+" === i.charAt(0) ? (i = i.slice(1) || "*", (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n)
        }
    }

    function W(e, t, n, i) {
        function o(a) {
            var l;
            return r[a] = !0, oe.each(e[a] || [], function (e, a) {
                var c = a(t, n, i);
                return "string" != typeof c || s || r[c] ? s ? !(l = c) : void 0 : (t.dataTypes.unshift(c), o(c), !1)
            }), l
        }

        var r = {}, s = e === Bt;
        return o(t.dataTypes[0]) || !r["*"] && o("*")
    }

    function q(e, t) {
        var n, i, o = oe.ajaxSettings.flatOptions || {};
        for (i in t) void 0 !== t[i] && ((o[i] ? e : n || (n = {}))[i] = t[i]);
        return n && oe.extend(!0, e, n), e
    }

    function F(e, t, n) {
        for (var i, o, r, s, a = e.contents, l = e.dataTypes; "*" === l[0];) l.shift(), void 0 === o && (o = e.mimeType || t.getResponseHeader("Content-Type"));
        if (o) for (s in a) if (a[s] && a[s].test(o)) {
            l.unshift(s);
            break
        }
        if (l[0] in n) r = l[0]; else {
            for (s in n) {
                if (!l[0] || e.converters[s + " " + l[0]]) {
                    r = s;
                    break
                }
                i || (i = s)
            }
            r = r || i
        }
        return r ? (r !== l[0] && l.unshift(r), n[r]) : void 0
    }

    function B(e, t, n, i) {
        var o, r, s, a, l, c = {}, d = e.dataTypes.slice();
        if (d[1]) for (s in e.converters) c[s.toLowerCase()] = e.converters[s];
        for (r = d.shift(); r;) if (e.responseFields[r] && (n[e.responseFields[r]] = t), !l && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = r, r = d.shift()) if ("*" === r) r = l; else if ("*" !== l && l !== r) {
            if (s = c[l + " " + r] || c["* " + r], !s) for (o in c) if (a = o.split(" "), a[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                s === !0 ? s = c[o] : c[o] !== !0 && (r = a[0], d.unshift(a[1]));
                break
            }
            if (s !== !0) if (s && e["throws"]) t = s(t); else try {
                t = s(t)
            } catch (u) {
                return {state: "parsererror", error: s ? u : "No conversion from " + l + " to " + r}
            }
        }
        return {state: "success", data: t}
    }

    function R(e, t, n, i) {
        var o;
        if (oe.isArray(t)) oe.each(t, function (t, o) {
            n || Yt.test(e) ? i(e, o) : R(e + "[" + ("object" == typeof o ? t : "") + "]", o, n, i)
        }); else if (n || "object" !== oe.type(t)) i(e, t); else for (o in t) R(e + "[" + o + "]", t[o], n, i)
    }

    function U() {
        try {
            return new e.XMLHttpRequest
        } catch (t) {
        }
    }

    function X() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {
        }
    }

    function Y(e) {
        return oe.isWindow(e) ? e : 9 === e.nodeType ? e.defaultView || e.parentWindow : !1
    }

    var V = [], Q = V.slice, Z = V.concat, K = V.push, G = V.indexOf, J = {}, ee = J.toString, te = J.hasOwnProperty, ne = {},
        ie = "1.11.1", oe = function (e, t) {
            return new oe.fn.init(e, t)
        }, re = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, se = /^-ms-/, ae = /-([\da-z])/gi, le = function (e, t) {
            return t.toUpperCase()
        };
    oe.fn = oe.prototype = {
        jquery: ie, constructor: oe, selector: "", length: 0, toArray: function () {
            return Q.call(this)
        }, get: function (e) {
            return null != e ? 0 > e ? this[e + this.length] : this[e] : Q.call(this)
        }, pushStack: function (e) {
            var t = oe.merge(this.constructor(), e);
            return t.prevObject = this, t.context = this.context, t
        }, each: function (e, t) {
            return oe.each(this, e, t)
        }, map: function (e) {
            return this.pushStack(oe.map(this, function (t, n) {
                return e.call(t, n, t)
            }))
        }, slice: function () {
            return this.pushStack(Q.apply(this, arguments))
        }, first: function () {
            return this.eq(0)
        }, last: function () {
            return this.eq(-1)
        }, eq: function (e) {
            var t = this.length, n = +e + (0 > e ? t : 0);
            return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
        }, end: function () {
            return this.prevObject || this.constructor(null)
        }, push: K, sort: V.sort, splice: V.splice
    }, oe.extend = oe.fn.extend = function () {
        var e, t, n, i, o, r, s = arguments[0] || {}, a = 1, l = arguments.length, c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || oe.isFunction(s) || (s = {}), a === l && (s = this, a--); l > a; a++) if (null != (o = arguments[a])) for (i in o) e = s[i], n = o[i], s !== n && (c && n && (oe.isPlainObject(n) || (t = oe.isArray(n))) ? (t ? (t = !1, r = e && oe.isArray(e) ? e : []) : r = e && oe.isPlainObject(e) ? e : {}, s[i] = oe.extend(c, r, n)) : void 0 !== n && (s[i] = n));
        return s
    }, oe.extend({
        expando: "jQuery" + (ie + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (e) {
            throw new Error(e)
        }, noop: function () {
        }, isFunction: function (e) {
            return "function" === oe.type(e)
        }, isArray: Array.isArray || function (e) {
            return "array" === oe.type(e)
        }, isWindow: function (e) {
            return null != e && e == e.window
        }, isNumeric: function (e) {
            return !oe.isArray(e) && e - parseFloat(e) >= 0
        }, isEmptyObject: function (e) {
            var t;
            for (t in e) return !1;
            return !0
        }, isPlainObject: function (e) {
            var t;
            if (!e || "object" !== oe.type(e) || e.nodeType || oe.isWindow(e)) return !1;
            try {
                if (e.constructor && !te.call(e, "constructor") && !te.call(e.constructor.prototype, "isPrototypeOf")) return !1
            } catch (n) {
                return !1
            }
            if (ne.ownLast) for (t in e) return te.call(e, t);
            for (t in e) ;
            return void 0 === t || te.call(e, t)
        }, type: function (e) {
            return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? J[ee.call(e)] || "object" : typeof e
        }, globalEval: function (t) {
            t && oe.trim(t) && (e.execScript || function (t) {
                e.eval.call(e, t)
            })(t)
        }, camelCase: function (e) {
            return e.replace(se, "ms-").replace(ae, le)
        }, nodeName: function (e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        }, each: function (e, t, i) {
            var o, r = 0, s = e.length, a = n(e);
            if (i) {
                if (a) for (; s > r && (o = t.apply(e[r], i), o !== !1); r++) ; else for (r in e) if (o = t.apply(e[r], i), o === !1) break
            } else if (a) for (; s > r && (o = t.call(e[r], r, e[r]), o !== !1); r++) ; else for (r in e) if (o = t.call(e[r], r, e[r]), o === !1) break;
            return e
        }, trim: function (e) {
            return null == e ? "" : (e + "").replace(re, "")
        }, makeArray: function (e, t) {
            var i = t || [];
            return null != e && (n(Object(e)) ? oe.merge(i, "string" == typeof e ? [e] : e) : K.call(i, e)), i
        }, inArray: function (e, t, n) {
            var i;
            if (t) {
                if (G) return G.call(t, e, n);
                for (i = t.length, n = n ? 0 > n ? Math.max(0, i + n) : n : 0; i > n; n++) if (n in t && t[n] === e) return n
            }
            return -1
        }, merge: function (e, t) {
            for (var n = +t.length, i = 0, o = e.length; n > i;) e[o++] = t[i++];
            if (n !== n) for (; void 0 !== t[i];) e[o++] = t[i++];
            return e.length = o, e
        }, grep: function (e, t, n) {
            for (var i, o = [], r = 0, s = e.length, a = !n; s > r; r++) i = !t(e[r], r), i !== a && o.push(e[r]);
            return o
        }, map: function (e, t, i) {
            var o, r = 0, s = e.length, a = n(e), l = [];
            if (a) for (; s > r; r++) o = t(e[r], r, i), null != o && l.push(o); else for (r in e) o = t(e[r], r, i), null != o && l.push(o);
            return Z.apply([], l)
        }, guid: 1, proxy: function (e, t) {
            var n, i, o;
            return "string" == typeof t && (o = e[t], t = e, e = o), oe.isFunction(e) ? (n = Q.call(arguments, 2), i = function () {
                return e.apply(t || this, n.concat(Q.call(arguments)))
            }, i.guid = e.guid = e.guid || oe.guid++, i) : void 0
        }, now: function () {
            return +new Date
        }, support: ne
    }), oe.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (e, t) {
        J["[object " + t + "]"] = t.toLowerCase()
    });
    var ce = function (e) {
        function t(e, t, n, i) {
            var o, r, s, a, l, c, u, f, h, g;
            if ((t ? t.ownerDocument || t : W) !== O && M(t), t = t || O, n = n || [], !e || "string" != typeof e) return n;
            if (1 !== (a = t.nodeType) && 9 !== a) return [];
            if (N && !i) {
                if (o = ye.exec(e)) if (s = o[1]) {
                    if (9 === a) {
                        if (r = t.getElementById(s), !r || !r.parentNode) return n;
                        if (r.id === s) return n.push(r), n
                    } else if (t.ownerDocument && (r = t.ownerDocument.getElementById(s)) && P(t, r) && r.id === s) return n.push(r), n
                } else {
                    if (o[2]) return J.apply(n, t.getElementsByTagName(e)), n;
                    if ((s = o[3]) && w.getElementsByClassName && t.getElementsByClassName) return J.apply(n, t.getElementsByClassName(s)), n
                }
                if (w.qsa && (!H || !H.test(e))) {
                    if (f = u = z, h = t, g = 9 === a && e, 1 === a && "object" !== t.nodeName.toLowerCase()) {
                        for (c = S(e), (u = t.getAttribute("id")) ? f = u.replace(be, "\\$&") : t.setAttribute("id", f), f = "[id='" + f + "'] ", l = c.length; l--;) c[l] = f + p(c[l]);
                        h = xe.test(e) && d(t.parentNode) || t, g = c.join(",")
                    }
                    if (g) try {
                        return J.apply(n, h.querySelectorAll(g)), n
                    } catch (m) {
                    } finally {
                        u || t.removeAttribute("id")
                    }
                }
            }
            return E(e.replace(le, "$1"), t, n, i)
        }

        function n() {
            function e(n, i) {
                return t.push(n + " ") > _.cacheLength && delete e[t.shift()], e[n + " "] = i
            }

            var t = [];
            return e
        }

        function i(e) {
            return e[z] = !0, e
        }

        function o(e) {
            var t = O.createElement("div");
            try {
                return !!e(t)
            } catch (n) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t), t = null
            }
        }

        function r(e, t) {
            for (var n = e.split("|"), i = e.length; i--;) _.attrHandle[n[i]] = t
        }

        function s(e, t) {
            var n = t && e, i = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || V) - (~e.sourceIndex || V);
            if (i) return i;
            if (n) for (; n = n.nextSibling;) if (n === t) return -1;
            return e ? 1 : -1
        }

        function a(e) {
            return function (t) {
                var n = t.nodeName.toLowerCase();
                return "input" === n && t.type === e
            }
        }

        function l(e) {
            return function (t) {
                var n = t.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && t.type === e
            }
        }

        function c(e) {
            return i(function (t) {
                return t = +t, i(function (n, i) {
                    for (var o, r = e([], n.length, t), s = r.length; s--;) n[o = r[s]] && (n[o] = !(i[o] = n[o]))
                })
            })
        }

        function d(e) {
            return e && typeof e.getElementsByTagName !== Y && e
        }

        function u() {
        }

        function p(e) {
            for (var t = 0, n = e.length, i = ""; n > t; t++) i += e[t].value;
            return i
        }

        function f(e, t, n) {
            var i = t.dir, o = n && "parentNode" === i, r = F++;
            return t.first ? function (t, n, r) {
                for (; t = t[i];) if (1 === t.nodeType || o) return e(t, n, r)
            } : function (t, n, s) {
                var a, l, c = [q, r];
                if (s) {
                    for (; t = t[i];) if ((1 === t.nodeType || o) && e(t, n, s)) return !0
                } else for (; t = t[i];) if (1 === t.nodeType || o) {
                    if (l = t[z] || (t[z] = {}), (a = l[i]) && a[0] === q && a[1] === r) return c[2] = a[2];
                    if (l[i] = c, c[2] = e(t, n, s)) return !0
                }
            }
        }

        function h(e) {
            return e.length > 1 ? function (t, n, i) {
                for (var o = e.length; o--;) if (!e[o](t, n, i)) return !1;
                return !0
            } : e[0]
        }

        function g(e, n, i) {
            for (var o = 0, r = n.length; r > o; o++) t(e, n[o], i);
            return i
        }

        function m(e, t, n, i, o) {
            for (var r, s = [], a = 0, l = e.length, c = null != t; l > a; a++) (r = e[a]) && (!n || n(r, i, o)) && (s.push(r), c && t.push(a));
            return s
        }

        function v(e, t, n, o, r, s) {
            return o && !o[z] && (o = v(o)), r && !r[z] && (r = v(r, s)), i(function (i, s, a, l) {
                var c, d, u, p = [], f = [], h = s.length, v = i || g(t || "*", a.nodeType ? [a] : a, []),
                    y = !e || !i && t ? v : m(v, p, e, a, l), x = n ? r || (i ? e : h || o) ? [] : s : y;
                if (n && n(y, x, a, l), o) for (c = m(x, f), o(c, [], a, l), d = c.length; d--;) (u = c[d]) && (x[f[d]] = !(y[f[d]] = u));
                if (i) {
                    if (r || e) {
                        if (r) {
                            for (c = [], d = x.length; d--;) (u = x[d]) && c.push(y[d] = u);
                            r(null, x = [], c, l)
                        }
                        for (d = x.length; d--;) (u = x[d]) && (c = r ? te.call(i, u) : p[d]) > -1 && (i[c] = !(s[c] = u))
                    }
                } else x = m(x === s ? x.splice(h, x.length) : x), r ? r(null, s, x, l) : J.apply(s, x)
            })
        }

        function y(e) {
            for (var t, n, i, o = e.length, r = _.relative[e[0].type], s = r || _.relative[" "], a = r ? 1 : 0, l = f(function (e) {
                return e === t
            }, s, !0), c = f(function (e) {
                return te.call(t, e) > -1
            }, s, !0), d = [function (e, n, i) {
                return !r && (i || n !== $) || ((t = n).nodeType ? l(e, n, i) : c(e, n, i))
            }]; o > a; a++) if (n = _.relative[e[a].type]) d = [f(h(d), n)]; else {
                if (n = _.filter[e[a].type].apply(null, e[a].matches), n[z]) {
                    for (i = ++a; o > i && !_.relative[e[i].type]; i++) ;
                    return v(a > 1 && h(d), a > 1 && p(e.slice(0, a - 1).concat({value: " " === e[a - 2].type ? "*" : ""})).replace(le, "$1"), n, i > a && y(e.slice(a, i)), o > i && y(e = e.slice(i)), o > i && p(e))
                }
                d.push(n)
            }
            return h(d)
        }

        function x(e, n) {
            var o = n.length > 0, r = e.length > 0, s = function (i, s, a, l, c) {
                var d, u, p, f = 0, h = "0", g = i && [], v = [], y = $, x = i || r && _.find.TAG("*", c),
                    b = q += null == y ? 1 : Math.random() || .1, w = x.length;
                for (c && ($ = s !== O && s); h !== w && null != (d = x[h]); h++) {
                    if (r && d) {
                        for (u = 0; p = e[u++];) if (p(d, s, a)) {
                            l.push(d);
                            break
                        }
                        c && (q = b)
                    }
                    o && ((d = !p && d) && f--, i && g.push(d))
                }
                if (f += h, o && h !== f) {
                    for (u = 0; p = n[u++];) p(g, v, s, a);
                    if (i) {
                        if (f > 0) for (; h--;) g[h] || v[h] || (v[h] = K.call(l));
                        v = m(v)
                    }
                    J.apply(l, v), c && !i && v.length > 0 && f + n.length > 1 && t.uniqueSort(l)
                }
                return c && (q = b, $ = y), g
            };
            return o ? i(s) : s
        }

        var b, w, _, k, T, S, C, E, $, A, L, M, O, j, N, H, D, I, P, z = "sizzle" + -new Date, W = e.document, q = 0, F = 0, B = n(),
            R = n(), U = n(), X = function (e, t) {
                return e === t && (L = !0), 0
            }, Y = "undefined", V = 1 << 31, Q = {}.hasOwnProperty, Z = [], K = Z.pop, G = Z.push, J = Z.push, ee = Z.slice,
            te = Z.indexOf || function (e) {
                for (var t = 0, n = this.length; n > t; t++) if (this[t] === e) return t;
                return -1
            },
            ne = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            ie = "[\\x20\\t\\r\\n\\f]", oe = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", re = oe.replace("w", "w#"),
            se = "\\[" + ie + "*(" + oe + ")(?:" + ie + "*([*^$|!~]?=)" + ie + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + re + "))|)" + ie + "*\\]",
            ae = ":(" + oe + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + se + ")*)|.*)\\)|)",
            le = new RegExp("^" + ie + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ie + "+$", "g"), ce = new RegExp("^" + ie + "*," + ie + "*"),
            de = new RegExp("^" + ie + "*([>+~]|" + ie + ")" + ie + "*"), ue = new RegExp("=" + ie + "*([^\\]'\"]*?)" + ie + "*\\]", "g"),
            pe = new RegExp(ae), fe = new RegExp("^" + re + "$"), he = {
                ID: new RegExp("^#(" + oe + ")"),
                CLASS: new RegExp("^\\.(" + oe + ")"),
                TAG: new RegExp("^(" + oe.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + se),
                PSEUDO: new RegExp("^" + ae),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ie + "*(even|odd|(([+-]|)(\\d*)n|)" + ie + "*(?:([+-]|)" + ie + "*(\\d+)|))" + ie + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + ne + ")$", "i"),
                needsContext: new RegExp("^" + ie + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ie + "*((?:-\\d)?\\d*)" + ie + "*\\)|)(?=[^-]|$)", "i")
            }, ge = /^(?:input|select|textarea|button)$/i, me = /^h\d$/i, ve = /^[^{]+\{\s*\[native \w/,
            ye = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, xe = /[+~]/, be = /'|\\/g,
            we = new RegExp("\\\\([\\da-f]{1,6}" + ie + "?|(" + ie + ")|.)", "ig"), _e = function (e, t, n) {
                var i = "0x" + t - 65536;
                return i !== i || n ? t : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
            };
        try {
            J.apply(Z = ee.call(W.childNodes), W.childNodes), Z[W.childNodes.length].nodeType
        } catch (ke) {
            J = {
                apply: Z.length ? function (e, t) {
                    G.apply(e, ee.call(t))
                } : function (e, t) {
                    for (var n = e.length, i = 0; e[n++] = t[i++];) ;
                    e.length = n - 1
                }
            }
        }
        w = t.support = {}, T = t.isXML = function (e) {
            var t = e && (e.ownerDocument || e).documentElement;
            return t ? "HTML" !== t.nodeName : !1
        }, M = t.setDocument = function (e) {
            var t, n = e ? e.ownerDocument || e : W, i = n.defaultView;
            return n !== O && 9 === n.nodeType && n.documentElement ? (O = n, j = n.documentElement, N = !T(n), i && i !== i.top && (i.addEventListener ? i.addEventListener("unload", function () {
                M()
            }, !1) : i.attachEvent && i.attachEvent("onunload", function () {
                M()
            })), w.attributes = o(function (e) {
                return e.className = "i", !e.getAttribute("className")
            }), w.getElementsByTagName = o(function (e) {
                return e.appendChild(n.createComment("")), !e.getElementsByTagName("*").length
            }), w.getElementsByClassName = ve.test(n.getElementsByClassName) && o(function (e) {
                return e.innerHTML = "<div class='a'></div><div class='a i'></div>", e.firstChild.className = "i", 2 === e.getElementsByClassName("i").length
            }), w.getById = o(function (e) {
                return j.appendChild(e).id = z, !n.getElementsByName || !n.getElementsByName(z).length
            }), w.getById ? (_.find.ID = function (e, t) {
                if (typeof t.getElementById !== Y && N) {
                    var n = t.getElementById(e);
                    return n && n.parentNode ? [n] : []
                }
            }, _.filter.ID = function (e) {
                var t = e.replace(we, _e);
                return function (e) {
                    return e.getAttribute("id") === t
                }
            }) : (delete _.find.ID, _.filter.ID = function (e) {
                var t = e.replace(we, _e);
                return function (e) {
                    var n = typeof e.getAttributeNode !== Y && e.getAttributeNode("id");
                    return n && n.value === t
                }
            }), _.find.TAG = w.getElementsByTagName ? function (e, t) {
                return typeof t.getElementsByTagName !== Y ? t.getElementsByTagName(e) : void 0
            } : function (e, t) {
                var n, i = [], o = 0, r = t.getElementsByTagName(e);
                if ("*" === e) {
                    for (; n = r[o++];) 1 === n.nodeType && i.push(n);
                    return i
                }
                return r
            }, _.find.CLASS = w.getElementsByClassName && function (e, t) {
                return typeof t.getElementsByClassName !== Y && N ? t.getElementsByClassName(e) : void 0
            }, D = [], H = [], (w.qsa = ve.test(n.querySelectorAll)) && (o(function (e) {
                e.innerHTML = "<select msallowclip=''><option selected=''></option></select>", e.querySelectorAll("[msallowclip^='']").length && H.push("[*^$]=" + ie + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || H.push("\\[" + ie + "*(?:value|" + ne + ")"), e.querySelectorAll(":checked").length || H.push(":checked")
            }), o(function (e) {
                var t = n.createElement("input");
                t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && H.push("name" + ie + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || H.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), H.push(",.*:")
            })), (w.matchesSelector = ve.test(I = j.matches || j.webkitMatchesSelector || j.mozMatchesSelector || j.oMatchesSelector || j.msMatchesSelector)) && o(function (e) {
                w.disconnectedMatch = I.call(e, "div"), I.call(e, "[s!='']:x"), D.push("!=", ae)
            }), H = H.length && new RegExp(H.join("|")), D = D.length && new RegExp(D.join("|")), t = ve.test(j.compareDocumentPosition),
                P = t || ve.test(j.contains) ? function (e, t) {
                    var n = 9 === e.nodeType ? e.documentElement : e, i = t && t.parentNode;
                    return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
                } : function (e, t) {
                    if (t) for (; t = t.parentNode;) if (t === e) return !0;
                    return !1
                }, X = t ? function (e, t) {
                if (e === t) return L = !0, 0;
                var i = !e.compareDocumentPosition - !t.compareDocumentPosition;
                return i ? i : (i = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & i || !w.sortDetached && t.compareDocumentPosition(e) === i ? e === n || e.ownerDocument === W && P(W, e) ? -1 : t === n || t.ownerDocument === W && P(W, t) ? 1 : A ? te.call(A, e) - te.call(A, t) : 0 : 4 & i ? -1 : 1)
            } : function (e, t) {
                if (e === t) return L = !0, 0;
                var i, o = 0, r = e.parentNode, a = t.parentNode, l = [e], c = [t];
                if (!r || !a) return e === n ? -1 : t === n ? 1 : r ? -1 : a ? 1 : A ? te.call(A, e) - te.call(A, t) : 0;
                if (r === a) return s(e, t);
                for (i = e; i = i.parentNode;) l.unshift(i);
                for (i = t; i = i.parentNode;) c.unshift(i);
                for (; l[o] === c[o];) o++;
                return o ? s(l[o], c[o]) : l[o] === W ? -1 : c[o] === W ? 1 : 0
            }, n) : O
        }, t.matches = function (e, n) {
            return t(e, null, null, n)
        }, t.matchesSelector = function (e, n) {
            if ((e.ownerDocument || e) !== O && M(e), n = n.replace(ue, "='$1']"), !(!w.matchesSelector || !N || D && D.test(n) || H && H.test(n))) try {
                var i = I.call(e, n);
                if (i || w.disconnectedMatch || e.document && 11 !== e.document.nodeType) return i
            } catch (o) {
            }
            return t(n, O, null, [e]).length > 0
        }, t.contains = function (e, t) {
            return (e.ownerDocument || e) !== O && M(e), P(e, t)
        }, t.attr = function (e, t) {
            (e.ownerDocument || e) !== O && M(e);
            var n = _.attrHandle[t.toLowerCase()], i = n && Q.call(_.attrHandle, t.toLowerCase()) ? n(e, t, !N) : void 0;
            return void 0 !== i ? i : w.attributes || !N ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }, t.error = function (e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        }, t.uniqueSort = function (e) {
            var t, n = [], i = 0, o = 0;
            if (L = !w.detectDuplicates, A = !w.sortStable && e.slice(0), e.sort(X), L) {
                for (; t = e[o++];) t === e[o] && (i = n.push(o));
                for (; i--;) e.splice(n[i], 1)
            }
            return A = null, e
        }, k = t.getText = function (e) {
            var t, n = "", i = 0, o = e.nodeType;
            if (o) {
                if (1 === o || 9 === o || 11 === o) {
                    if ("string" == typeof e.textContent) return e.textContent;
                    for (e = e.firstChild; e; e = e.nextSibling) n += k(e)
                } else if (3 === o || 4 === o) return e.nodeValue
            } else for (; t = e[i++];) n += k(t);
            return n
        }, _ = t.selectors = {
            cacheLength: 50,
            createPseudo: i,
            match: he,
            attrHandle: {},
            find: {},
            relative: {
                ">": {dir: "parentNode", first: !0},
                " ": {dir: "parentNode"},
                "+": {dir: "previousSibling", first: !0},
                "~": {dir: "previousSibling"}
            },
            preFilter: {
                ATTR: function (e) {
                    return e[1] = e[1].replace(we, _e), e[3] = (e[3] || e[4] || e[5] || "").replace(we, _e), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                }, CHILD: function (e) {
                    return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                }, PSEUDO: function (e) {
                    var t, n = !e[6] && e[2];
                    return he.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && pe.test(n) && (t = S(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                }
            },
            filter: {
                TAG: function (e) {
                    var t = e.replace(we, _e).toLowerCase();
                    return "*" === e ? function () {
                        return !0
                    } : function (e) {
                        return e.nodeName && e.nodeName.toLowerCase() === t
                    }
                }, CLASS: function (e) {
                    var t = B[e + " "];
                    return t || (t = new RegExp("(^|" + ie + ")" + e + "(" + ie + "|$)")) && B(e, function (e) {
                        return t.test("string" == typeof e.className && e.className || typeof e.getAttribute !== Y && e.getAttribute("class") || "")
                    })
                }, ATTR: function (e, n, i) {
                    return function (o) {
                        var r = t.attr(o, e);
                        return null == r ? "!=" === n : n ? (r += "", "=" === n ? r === i : "!=" === n ? r !== i : "^=" === n ? i && 0 === r.indexOf(i) : "*=" === n ? i && r.indexOf(i) > -1 : "$=" === n ? i && r.slice(-i.length) === i : "~=" === n ? (" " + r + " ").indexOf(i) > -1 : "|=" === n ? r === i || r.slice(0, i.length + 1) === i + "-" : !1) : !0
                    }
                }, CHILD: function (e, t, n, i, o) {
                    var r = "nth" !== e.slice(0, 3), s = "last" !== e.slice(-4), a = "of-type" === t;
                    return 1 === i && 0 === o ? function (e) {
                        return !!e.parentNode
                    } : function (t, n, l) {
                        var c, d, u, p, f, h, g = r !== s ? "nextSibling" : "previousSibling", m = t.parentNode,
                            v = a && t.nodeName.toLowerCase(), y = !l && !a;
                        if (m) {
                            if (r) {
                                for (; g;) {
                                    for (u = t; u = u[g];) if (a ? u.nodeName.toLowerCase() === v : 1 === u.nodeType) return !1;
                                    h = g = "only" === e && !h && "nextSibling"
                                }
                                return !0
                            }
                            if (h = [s ? m.firstChild : m.lastChild], s && y) {
                                for (d = m[z] || (m[z] = {}), c = d[e] || [], f = c[0] === q && c[1], p = c[0] === q && c[2], u = f && m.childNodes[f]; u = ++f && u && u[g] || (p = f = 0) || h.pop();) if (1 === u.nodeType && ++p && u === t) {
                                    d[e] = [q, f, p];
                                    break
                                }
                            } else if (y && (c = (t[z] || (t[z] = {}))[e]) && c[0] === q) p = c[1]; else for (; (u = ++f && u && u[g] || (p = f = 0) || h.pop()) && ((a ? u.nodeName.toLowerCase() !== v : 1 !== u.nodeType) || !++p || (y && ((u[z] || (u[z] = {}))[e] = [q, p]), u !== t));) ;
                            return p -= o, p === i || p % i === 0 && p / i >= 0
                        }
                    }
                }, PSEUDO: function (e, n) {
                    var o, r = _.pseudos[e] || _.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                    return r[z] ? r(n) : r.length > 1 ? (o = [e, e, "", n], _.setFilters.hasOwnProperty(e.toLowerCase()) ? i(function (e, t) {
                        for (var i, o = r(e, n), s = o.length; s--;) i = te.call(e, o[s]), e[i] = !(t[i] = o[s])
                    }) : function (e) {
                        return r(e, 0, o)
                    }) : r
                }
            },
            pseudos: {
                not: i(function (e) {
                    var t = [], n = [], o = C(e.replace(le, "$1"));
                    return o[z] ? i(function (e, t, n, i) {
                        for (var r, s = o(e, null, i, []), a = e.length; a--;) (r = s[a]) && (e[a] = !(t[a] = r))
                    }) : function (e, i, r) {
                        return t[0] = e, o(t, null, r, n), !n.pop()
                    }
                }), has: i(function (e) {
                    return function (n) {
                        return t(e, n).length > 0
                    }
                }), contains: i(function (e) {
                    return function (t) {
                        return (t.textContent || t.innerText || k(t)).indexOf(e) > -1
                    }
                }), lang: i(function (e) {
                    return fe.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(we, _e).toLowerCase(), function (t) {
                        var n;
                        do if (n = N ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-"); while ((t = t.parentNode) && 1 === t.nodeType);
                        return !1
                    }
                }), target: function (t) {
                    var n = e.location && e.location.hash;
                    return n && n.slice(1) === t.id
                }, root: function (e) {
                    return e === j
                }, focus: function (e) {
                    return e === O.activeElement && (!O.hasFocus || O.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                }, enabled: function (e) {
                    return e.disabled === !1
                }, disabled: function (e) {
                    return e.disabled === !0
                }, checked: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && !!e.checked || "option" === t && !!e.selected
                }, selected: function (e) {
                    return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                }, empty: function (e) {
                    for (e = e.firstChild; e; e = e.nextSibling) if (e.nodeType < 6) return !1;
                    return !0
                }, parent: function (e) {
                    return !_.pseudos.empty(e)
                }, header: function (e) {
                    return me.test(e.nodeName)
                }, input: function (e) {
                    return ge.test(e.nodeName)
                }, button: function (e) {
                    var t = e.nodeName.toLowerCase();
                    return "input" === t && "button" === e.type || "button" === t
                }, text: function (e) {
                    var t;
                    return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                }, first: c(function () {
                    return [0]
                }), last: c(function (e, t) {
                    return [t - 1]
                }), eq: c(function (e, t, n) {
                    return [0 > n ? n + t : n]
                }), even: c(function (e, t) {
                    for (var n = 0; t > n; n += 2) e.push(n);
                    return e
                }), odd: c(function (e, t) {
                    for (var n = 1; t > n; n += 2) e.push(n);
                    return e
                }), lt: c(function (e, t, n) {
                    for (var i = 0 > n ? n + t : n; --i >= 0;) e.push(i);
                    return e
                }), gt: c(function (e, t, n) {
                    for (var i = 0 > n ? n + t : n; ++i < t;) e.push(i);
                    return e
                })
            }
        }, _.pseudos.nth = _.pseudos.eq;
        for (b in{radio: !0, checkbox: !0, file: !0, password: !0, image: !0}) _.pseudos[b] = a(b);
        for (b in{submit: !0, reset: !0}) _.pseudos[b] = l(b);
        return u.prototype = _.filters = _.pseudos, _.setFilters = new u, S = t.tokenize = function (e, n) {
            var i, o, r, s, a, l, c, d = R[e + " "];
            if (d) return n ? 0 : d.slice(0);
            for (a = e, l = [], c = _.preFilter; a;) {
                (!i || (o = ce.exec(a))) && (o && (a = a.slice(o[0].length) || a), l.push(r = [])), i = !1, (o = de.exec(a)) && (i = o.shift(), r.push({
                    value: i,
                    type: o[0].replace(le, " ")
                }), a = a.slice(i.length));
                for (s in _.filter) !(o = he[s].exec(a)) || c[s] && !(o = c[s](o)) || (i = o.shift(), r.push({
                    value: i,
                    type: s,
                    matches: o
                }), a = a.slice(i.length));
                if (!i) break
            }
            return n ? a.length : a ? t.error(e) : R(e, l).slice(0)
        }, C = t.compile = function (e, t) {
            var n, i = [], o = [], r = U[e + " "];
            if (!r) {
                for (t || (t = S(e)), n = t.length; n--;) r = y(t[n]), r[z] ? i.push(r) : o.push(r);
                r = U(e, x(o, i)), r.selector = e
            }
            return r
        }, E = t.select = function (e, t, n, i) {
            var o, r, s, a, l, c = "function" == typeof e && e, u = !i && S(e = c.selector || e);
            if (n = n || [], 1 === u.length) {
                if (r = u[0] = u[0].slice(0), r.length > 2 && "ID" === (s = r[0]).type && w.getById && 9 === t.nodeType && N && _.relative[r[1].type]) {
                    if (t = (_.find.ID(s.matches[0].replace(we, _e), t) || [])[0], !t) return n;
                    c && (t = t.parentNode), e = e.slice(r.shift().value.length)
                }
                for (o = he.needsContext.test(e) ? 0 : r.length; o-- && (s = r[o], !_.relative[a = s.type]);) if ((l = _.find[a]) && (i = l(s.matches[0].replace(we, _e), xe.test(r[0].type) && d(t.parentNode) || t))) {
                    if (r.splice(o, 1), e = i.length && p(r), !e) return J.apply(n, i), n;
                    break
                }
            }
            return (c || C(e, u))(i, t, !N, n, xe.test(e) && d(t.parentNode) || t), n
        }, w.sortStable = z.split("").sort(X).join("") === z, w.detectDuplicates = !!L, M(), w.sortDetached = o(function (e) {
            return 1 & e.compareDocumentPosition(O.createElement("div"))
        }), o(function (e) {
            return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
        }) || r("type|href|height|width", function (e, t, n) {
            return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), w.attributes && o(function (e) {
            return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
        }) || r("value", function (e, t, n) {
            return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
        }), o(function (e) {
            return null == e.getAttribute("disabled")
        }) || r(ne, function (e, t, n) {
            var i;
            return n ? void 0 : e[t] === !0 ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
        }), t
    }(e);
    oe.find = ce, oe.expr = ce.selectors, oe.expr[":"] = oe.expr.pseudos, oe.unique = ce.uniqueSort, oe.text = ce.getText, oe.isXMLDoc = ce.isXML, oe.contains = ce.contains;
    var de = oe.expr.match.needsContext, ue = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, pe = /^.[^:#\[\.,]*$/;
    oe.filter = function (e, t, n) {
        var i = t[0];
        return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? oe.find.matchesSelector(i, e) ? [i] : [] : oe.find.matches(e, oe.grep(t, function (e) {
            return 1 === e.nodeType
        }))
    }, oe.fn.extend({
        find: function (e) {
            var t, n = [], i = this, o = i.length;
            if ("string" != typeof e) return this.pushStack(oe(e).filter(function () {
                for (t = 0; o > t; t++) if (oe.contains(i[t], this)) return !0
            }));
            for (t = 0; o > t; t++) oe.find(e, i[t], n);
            return n = this.pushStack(o > 1 ? oe.unique(n) : n), n.selector = this.selector ? this.selector + " " + e : e, n
        }, filter: function (e) {
            return this.pushStack(i(this, e || [], !1))
        }, not: function (e) {
            return this.pushStack(i(this, e || [], !0))
        }, is: function (e) {
            return !!i(this, "string" == typeof e && de.test(e) ? oe(e) : e || [], !1).length
        }
    });
    var fe, he = e.document, ge = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, me = oe.fn.init = function (e, t) {
        var n, i;
        if (!e) return this;
        if ("string" == typeof e) {
            if (n = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : ge.exec(e), !n || !n[1] && t) return !t || t.jquery ? (t || fe).find(e) : this.constructor(t).find(e);
            if (n[1]) {
                if (t = t instanceof oe ? t[0] : t, oe.merge(this, oe.parseHTML(n[1], t && t.nodeType ? t.ownerDocument || t : he, !0)), ue.test(n[1]) && oe.isPlainObject(t)) for (n in t) oe.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
                return this
            }
            if (i = he.getElementById(n[2]), i && i.parentNode) {
                if (i.id !== n[2]) return fe.find(e);
                this.length = 1, this[0] = i
            }
            return this.context = he, this.selector = e, this
        }
        return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : oe.isFunction(e) ? "undefined" != typeof fe.ready ? fe.ready(e) : e(oe) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), oe.makeArray(e, this))
    };
    me.prototype = oe.fn, fe = oe(he);
    var ve = /^(?:parents|prev(?:Until|All))/, ye = {children: !0, contents: !0, next: !0, prev: !0};
    oe.extend({
        dir: function (e, t, n) {
            for (var i = [], o = e[t]; o && 9 !== o.nodeType && (void 0 === n || 1 !== o.nodeType || !oe(o).is(n));) 1 === o.nodeType && i.push(o), o = o[t];
            return i
        }, sibling: function (e, t) {
            for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
            return n
        }
    }), oe.fn.extend({
        has: function (e) {
            var t, n = oe(e, this), i = n.length;
            return this.filter(function () {
                for (t = 0; i > t; t++) if (oe.contains(this, n[t])) return !0
            })
        }, closest: function (e, t) {
            for (var n, i = 0, o = this.length, r = [], s = de.test(e) || "string" != typeof e ? oe(e, t || this.context) : 0; o > i; i++) for (n = this[i]; n && n !== t; n = n.parentNode) if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && oe.find.matchesSelector(n, e))) {
                r.push(n);
                break
            }
            return this.pushStack(r.length > 1 ? oe.unique(r) : r)
        }, index: function (e) {
            return e ? "string" == typeof e ? oe.inArray(this[0], oe(e)) : oe.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        }, add: function (e, t) {
            return this.pushStack(oe.unique(oe.merge(this.get(), oe(e, t))))
        }, addBack: function (e) {
            return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
        }
    }), oe.each({
        parent: function (e) {
            var t = e.parentNode;
            return t && 11 !== t.nodeType ? t : null
        }, parents: function (e) {
            return oe.dir(e, "parentNode")
        }, parentsUntil: function (e, t, n) {
            return oe.dir(e, "parentNode", n)
        }, next: function (e) {
            return o(e, "nextSibling")
        }, prev: function (e) {
            return o(e, "previousSibling")
        }, nextAll: function (e) {
            return oe.dir(e, "nextSibling")
        }, prevAll: function (e) {
            return oe.dir(e, "previousSibling")
        }, nextUntil: function (e, t, n) {
            return oe.dir(e, "nextSibling", n)
        }, prevUntil: function (e, t, n) {
            return oe.dir(e, "previousSibling", n)
        }, siblings: function (e) {
            return oe.sibling((e.parentNode || {}).firstChild, e)
        }, children: function (e) {
            return oe.sibling(e.firstChild)
        }, contents: function (e) {
            return oe.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : oe.merge([], e.childNodes)
        }
    }, function (e, t) {
        oe.fn[e] = function (n, i) {
            var o = oe.map(this, t, n);
            return "Until" !== e.slice(-5) && (i = n), i && "string" == typeof i && (o = oe.filter(i, o)), this.length > 1 && (ye[e] || (o = oe.unique(o)), ve.test(e) && (o = o.reverse())), this.pushStack(o)
        }
    });
    var xe = /\S+/g, be = {};
    oe.Callbacks = function (e) {
        e = "string" == typeof e ? be[e] || r(e) : oe.extend({}, e);
        var t, n, i, o, s, a, l = [], c = !e.once && [], d = function (r) {
            for (n = e.memory && r, i = !0, s = a || 0, a = 0, o = l.length, t = !0; l && o > s; s++) if (l[s].apply(r[0], r[1]) === !1 && e.stopOnFalse) {
                n = !1;
                break
            }
            t = !1, l && (c ? c.length && d(c.shift()) : n ? l = [] : u.disable())
        }, u = {
            add: function () {
                if (l) {
                    var i = l.length;
                    !function r(t) {
                        oe.each(t, function (t, n) {
                            var i = oe.type(n);
                            "function" === i ? e.unique && u.has(n) || l.push(n) : n && n.length && "string" !== i && r(n)
                        })
                    }(arguments), t ? o = l.length : n && (a = i, d(n))
                }
                return this
            }, remove: function () {
                return l && oe.each(arguments, function (e, n) {
                    for (var i; (i = oe.inArray(n, l, i)) > -1;) l.splice(i, 1), t && (o >= i && o--, s >= i && s--)
                }), this
            }, has: function (e) {
                return e ? oe.inArray(e, l) > -1 : !(!l || !l.length)
            }, empty: function () {
                return l = [], o = 0, this
            }, disable: function () {
                return l = c = n = void 0, this
            }, disabled: function () {
                return !l
            }, lock: function () {
                return c = void 0, n || u.disable(), this
            }, locked: function () {
                return !c
            }, fireWith: function (e, n) {
                return !l || i && !c || (n = n || [], n = [e, n.slice ? n.slice() : n], t ? c.push(n) : d(n)), this
            }, fire: function () {
                return u.fireWith(this, arguments), this
            }, fired: function () {
                return !!i
            }
        };
        return u
    }, oe.extend({
        Deferred: function (e) {
            var t = [["resolve", "done", oe.Callbacks("once memory"), "resolved"], ["reject", "fail", oe.Callbacks("once memory"), "rejected"], ["notify", "progress", oe.Callbacks("memory")]],
                n = "pending", i = {
                    state: function () {
                        return n
                    }, always: function () {
                        return o.done(arguments).fail(arguments), this
                    }, then: function () {
                        var e = arguments;
                        return oe.Deferred(function (n) {
                            oe.each(t, function (t, r) {
                                var s = oe.isFunction(e[t]) && e[t];
                                o[r[1]](function () {
                                    var e = s && s.apply(this, arguments);
                                    e && oe.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[r[0] + "With"](this === i ? n.promise() : this, s ? [e] : arguments)
                                })
                            }), e = null
                        }).promise()
                    }, promise: function (e) {
                        return null != e ? oe.extend(e, i) : i
                    }
                }, o = {};
            return i.pipe = i.then, oe.each(t, function (e, r) {
                var s = r[2], a = r[3];
                i[r[1]] = s.add, a && s.add(function () {
                    n = a
                }, t[1 ^ e][2].disable, t[2][2].lock), o[r[0]] = function () {
                    return o[r[0] + "With"](this === o ? i : this, arguments), this
                }, o[r[0] + "With"] = s.fireWith
            }), i.promise(o), e && e.call(o, o), o
        }, when: function (e) {
            var t, n, i, o = 0, r = Q.call(arguments), s = r.length, a = 1 !== s || e && oe.isFunction(e.promise) ? s : 0,
                l = 1 === a ? e : oe.Deferred(), c = function (e, n, i) {
                    return function (o) {
                        n[e] = this, i[e] = arguments.length > 1 ? Q.call(arguments) : o, i === t ? l.notifyWith(n, i) : --a || l.resolveWith(n, i)
                    }
                };
            if (s > 1) for (t = new Array(s), n = new Array(s), i = new Array(s); s > o; o++) r[o] && oe.isFunction(r[o].promise) ? r[o].promise().done(c(o, i, r)).fail(l.reject).progress(c(o, n, t)) : --a;
            return a || l.resolveWith(i, r), l.promise()
        }
    });
    var we;
    oe.fn.ready = function (e) {
        return oe.ready.promise().done(e), this
    }, oe.extend({
        isReady: !1, readyWait: 1, holdReady: function (e) {
            e ? oe.readyWait++ : oe.ready(!0)
        }, ready: function (e) {
            if (e === !0 ? !--oe.readyWait : !oe.isReady) {
                if (!he.body) return setTimeout(oe.ready);
                oe.isReady = !0, e !== !0 && --oe.readyWait > 0 || (we.resolveWith(he, [oe]), oe.fn.triggerHandler && (oe(he).triggerHandler("ready"), oe(he).off("ready")))
            }
        }
    }), oe.ready.promise = function (t) {
        if (!we) if (we = oe.Deferred(), "complete" === he.readyState) setTimeout(oe.ready); else if (he.addEventListener) he.addEventListener("DOMContentLoaded", a, !1), e.addEventListener("load", a, !1); else {
            he.attachEvent("onreadystatechange", a), e.attachEvent("onload", a);
            var n = !1;
            try {
                n = null == e.frameElement && he.documentElement
            } catch (i) {
            }
            n && n.doScroll && !function o() {
                if (!oe.isReady) {
                    try {
                        n.doScroll("left")
                    } catch (e) {
                        return setTimeout(o, 50)
                    }
                    s(), oe.ready()
                }
            }()
        }
        return we.promise(t)
    };
    var _e, ke = "undefined";
    for (_e in oe(ne)) break;
    ne.ownLast = "0" !== _e, ne.inlineBlockNeedsLayout = !1, oe(function () {
        var e, t, n, i;
        n = he.getElementsByTagName("body")[0], n && n.style && (t = he.createElement("div"), i = he.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(t), typeof t.style.zoom !== ke && (t.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", ne.inlineBlockNeedsLayout = e = 3 === t.offsetWidth, e && (n.style.zoom = 1)), n.removeChild(i))
    }), function () {
        var e = he.createElement("div");
        if (null == ne.deleteExpando) {
            ne.deleteExpando = !0;
            try {
                delete e.test
            } catch (t) {
                ne.deleteExpando = !1
            }
        }
        e = null
    }(), oe.acceptData = function (e) {
        var t = oe.noData[(e.nodeName + " ").toLowerCase()], n = +e.nodeType || 1;
        return 1 !== n && 9 !== n ? !1 : !t || t !== !0 && e.getAttribute("classid") === t
    };
    var Te = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, Se = /([A-Z])/g;
    oe.extend({
        cache: {},
        noData: {"applet ": !0, "embed ": !0, "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},
        hasData: function (e) {
            return e = e.nodeType ? oe.cache[e[oe.expando]] : e[oe.expando], !!e && !c(e)
        },
        data: function (e, t, n) {
            return d(e, t, n)
        },
        removeData: function (e, t) {
            return u(e, t)
        },
        _data: function (e, t, n) {
            return d(e, t, n, !0)
        },
        _removeData: function (e, t) {
            return u(e, t, !0)
        }
    }), oe.fn.extend({
        data: function (e, t) {
            var n, i, o, r = this[0], s = r && r.attributes;
            if (void 0 === e) {
                if (this.length && (o = oe.data(r), 1 === r.nodeType && !oe._data(r, "parsedAttrs"))) {
                    for (n = s.length; n--;) s[n] && (i = s[n].name, 0 === i.indexOf("data-") && (i = oe.camelCase(i.slice(5)), l(r, i, o[i])));
                    oe._data(r, "parsedAttrs", !0)
                }
                return o
            }
            return "object" == typeof e ? this.each(function () {
                oe.data(this, e)
            }) : arguments.length > 1 ? this.each(function () {
                oe.data(this, e, t)
            }) : r ? l(r, e, oe.data(r, e)) : void 0
        }, removeData: function (e) {
            return this.each(function () {
                oe.removeData(this, e)
            })
        }
    }), oe.extend({
        queue: function (e, t, n) {
            var i;
            return e ? (t = (t || "fx") + "queue", i = oe._data(e, t), n && (!i || oe.isArray(n) ? i = oe._data(e, t, oe.makeArray(n)) : i.push(n)), i || []) : void 0
        }, dequeue: function (e, t) {
            t = t || "fx";
            var n = oe.queue(e, t), i = n.length, o = n.shift(), r = oe._queueHooks(e, t), s = function () {
                oe.dequeue(e, t)
            };
            "inprogress" === o && (o = n.shift(), i--), o && ("fx" === t && n.unshift("inprogress"), delete r.stop, o.call(e, s, r)), !i && r && r.empty.fire()
        }, _queueHooks: function (e, t) {
            var n = t + "queueHooks";
            return oe._data(e, n) || oe._data(e, n, {
                empty: oe.Callbacks("once memory").add(function () {
                    oe._removeData(e, t + "queue"), oe._removeData(e, n)
                })
            })
        }
    }), oe.fn.extend({
        queue: function (e, t) {
            var n = 2;
            return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? oe.queue(this[0], e) : void 0 === t ? this : this.each(function () {
                var n = oe.queue(this, e, t);
                oe._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && oe.dequeue(this, e)
            })
        }, dequeue: function (e) {
            return this.each(function () {
                oe.dequeue(this, e)
            })
        }, clearQueue: function (e) {
            return this.queue(e || "fx", [])
        }, promise: function (e, t) {
            var n, i = 1, o = oe.Deferred(), r = this, s = this.length, a = function () {
                --i || o.resolveWith(r, [r])
            };
            for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; s--;) n = oe._data(r[s], e + "queueHooks"), n && n.empty && (i++, n.empty.add(a));
            return a(), o.promise(t)
        }
    });
    var Ce = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, Ee = ["Top", "Right", "Bottom", "Left"], $e = function (e, t) {
        return e = t || e, "none" === oe.css(e, "display") || !oe.contains(e.ownerDocument, e)
    }, Ae = oe.access = function (e, t, n, i, o, r, s) {
        var a = 0, l = e.length, c = null == n;
        if ("object" === oe.type(n)) {
            o = !0;
            for (a in n) oe.access(e, t, a, n[a], !0, r, s)
        } else if (void 0 !== i && (o = !0, oe.isFunction(i) || (s = !0), c && (s ? (t.call(e, i), t = null) : (c = t, t = function (e, t, n) {
            return c.call(oe(e), n)
        })), t)) for (; l > a; a++) t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
        return o ? e : c ? t.call(e) : l ? t(e[0], n) : r
    }, Le = /^(?:checkbox|radio)$/i;
    !function () {
        var e = he.createElement("input"), t = he.createElement("div"), n = he.createDocumentFragment();
        if (t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", ne.leadingWhitespace = 3 === t.firstChild.nodeType, ne.tbody = !t.getElementsByTagName("tbody").length, ne.htmlSerialize = !!t.getElementsByTagName("link").length, ne.html5Clone = "<:nav></:nav>" !== he.createElement("nav").cloneNode(!0).outerHTML, e.type = "checkbox", e.checked = !0, n.appendChild(e), ne.appendChecked = e.checked, t.innerHTML = "<textarea>x</textarea>", ne.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue, n.appendChild(t), t.innerHTML = "<input type='radio' checked='checked' name='t'/>", ne.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, ne.noCloneEvent = !0, t.attachEvent && (t.attachEvent("onclick", function () {
            ne.noCloneEvent = !1
        }), t.cloneNode(!0).click()), null == ne.deleteExpando) {
            ne.deleteExpando = !0;
            try {
                delete t.test
            } catch (i) {
                ne.deleteExpando = !1
            }
        }
    }(), function () {
        var t, n, i = he.createElement("div");
        for (t in{
            submit: !0,
            change: !0,
            focusin: !0
        }) n = "on" + t, (ne[t + "Bubbles"] = n in e) || (i.setAttribute(n, "t"), ne[t + "Bubbles"] = i.attributes[n].expando === !1);
        i = null
    }();
    var Me = /^(?:input|select|textarea)$/i, Oe = /^key/, je = /^(?:mouse|pointer|contextmenu)|click/,
        Ne = /^(?:focusinfocus|focusoutblur)$/, He = /^([^.]*)(?:\.(.+)|)$/;
    oe.event = {
        global: {},
        add: function (e, t, n, i, o) {
            var r, s, a, l, c, d, u, p, f, h, g, m = oe._data(e);
            if (m) {
                for (n.handler && (l = n, n = l.handler, o = l.selector), n.guid || (n.guid = oe.guid++), (s = m.events) || (s = m.events = {}), (d = m.handle) || (d = m.handle = function (e) {
                    return typeof oe === ke || e && oe.event.triggered === e.type ? void 0 : oe.event.dispatch.apply(d.elem, arguments)
                }, d.elem = e), t = (t || "").match(xe) || [""], a = t.length; a--;) r = He.exec(t[a]) || [], f = g = r[1], h = (r[2] || "").split(".").sort(), f && (c = oe.event.special[f] || {}, f = (o ? c.delegateType : c.bindType) || f, c = oe.event.special[f] || {}, u = oe.extend({
                    type: f,
                    origType: g,
                    data: i,
                    handler: n,
                    guid: n.guid,
                    selector: o,
                    needsContext: o && oe.expr.match.needsContext.test(o),
                    namespace: h.join(".")
                }, l), (p = s[f]) || (p = s[f] = [], p.delegateCount = 0, c.setup && c.setup.call(e, i, h, d) !== !1 || (e.addEventListener ? e.addEventListener(f, d, !1) : e.attachEvent && e.attachEvent("on" + f, d))), c.add && (c.add.call(e, u), u.handler.guid || (u.handler.guid = n.guid)), o ? p.splice(p.delegateCount++, 0, u) : p.push(u), oe.event.global[f] = !0);
                e = null
            }
        },
        remove: function (e, t, n, i, o) {
            var r, s, a, l, c, d, u, p, f, h, g, m = oe.hasData(e) && oe._data(e);
            if (m && (d = m.events)) {
                for (t = (t || "").match(xe) || [""], c = t.length; c--;) if (a = He.exec(t[c]) || [], f = g = a[1], h = (a[2] || "").split(".").sort(), f) {
                    for (u = oe.event.special[f] || {}, f = (i ? u.delegateType : u.bindType) || f, p = d[f] || [], a = a[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = r = p.length; r--;) s = p[r], !o && g !== s.origType || n && n.guid !== s.guid || a && !a.test(s.namespace) || i && i !== s.selector && ("**" !== i || !s.selector) || (p.splice(r, 1), s.selector && p.delegateCount--, u.remove && u.remove.call(e, s));
                    l && !p.length && (u.teardown && u.teardown.call(e, h, m.handle) !== !1 || oe.removeEvent(e, f, m.handle), delete d[f])
                } else for (f in d) oe.event.remove(e, f + t[c], n, i, !0);
                oe.isEmptyObject(d) && (delete m.handle, oe._removeData(e, "events"))
            }
        },
        trigger: function (t, n, i, o) {
            var r, s, a, l, c, d, u, p = [i || he], f = te.call(t, "type") ? t.type : t,
                h = te.call(t, "namespace") ? t.namespace.split(".") : [];
            if (a = d = i = i || he, 3 !== i.nodeType && 8 !== i.nodeType && !Ne.test(f + oe.event.triggered) && (f.indexOf(".") >= 0 && (h = f.split("."), f = h.shift(), h.sort()), s = f.indexOf(":") < 0 && "on" + f, t = t[oe.expando] ? t : new oe.Event(f, "object" == typeof t && t), t.isTrigger = o ? 2 : 3, t.namespace = h.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = i), n = null == n ? [t] : oe.makeArray(n, [t]), c = oe.event.special[f] || {}, o || !c.trigger || c.trigger.apply(i, n) !== !1)) {
                if (!o && !c.noBubble && !oe.isWindow(i)) {
                    for (l = c.delegateType || f, Ne.test(l + f) || (a = a.parentNode); a; a = a.parentNode) p.push(a), d = a;
                    d === (i.ownerDocument || he) && p.push(d.defaultView || d.parentWindow || e)
                }
                for (u = 0; (a = p[u++]) && !t.isPropagationStopped();) t.type = u > 1 ? l : c.bindType || f, r = (oe._data(a, "events") || {})[t.type] && oe._data(a, "handle"), r && r.apply(a, n), r = s && a[s], r && r.apply && oe.acceptData(a) && (t.result = r.apply(a, n), t.result === !1 && t.preventDefault());
                if (t.type = f, !o && !t.isDefaultPrevented() && (!c._default || c._default.apply(p.pop(), n) === !1) && oe.acceptData(i) && s && i[f] && !oe.isWindow(i)) {
                    d = i[s], d && (i[s] = null), oe.event.triggered = f;
                    try {
                        i[f]()
                    } catch (g) {
                    }
                    oe.event.triggered = void 0, d && (i[s] = d)
                }
                return t.result
            }
        },
        dispatch: function (e) {
            e = oe.event.fix(e);
            var t, n, i, o, r, s = [], a = Q.call(arguments), l = (oe._data(this, "events") || {})[e.type] || [],
                c = oe.event.special[e.type] || {};
            if (a[0] = e, e.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, e) !== !1) {
                for (s = oe.event.handlers.call(this, e, l), t = 0; (o = s[t++]) && !e.isPropagationStopped();) for (e.currentTarget = o.elem, r = 0; (i = o.handlers[r++]) && !e.isImmediatePropagationStopped();) (!e.namespace_re || e.namespace_re.test(i.namespace)) && (e.handleObj = i, e.data = i.data, n = ((oe.event.special[i.origType] || {}).handle || i.handler).apply(o.elem, a), void 0 !== n && (e.result = n) === !1 && (e.preventDefault(), e.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, e), e.result
            }
        },
        handlers: function (e, t) {
            var n, i, o, r, s = [], a = t.delegateCount, l = e.target;
            if (a && l.nodeType && (!e.button || "click" !== e.type)) for (; l != this; l = l.parentNode || this) if (1 === l.nodeType && (l.disabled !== !0 || "click" !== e.type)) {
                for (o = [], r = 0; a > r; r++) i = t[r], n = i.selector + " ", void 0 === o[n] && (o[n] = i.needsContext ? oe(n, this).index(l) >= 0 : oe.find(n, this, null, [l]).length), o[n] && o.push(i);
                o.length && s.push({elem: l, handlers: o})
            }
            return a < t.length && s.push({elem: this, handlers: t.slice(a)}), s
        },
        fix: function (e) {
            if (e[oe.expando]) return e;
            var t, n, i, o = e.type, r = e, s = this.fixHooks[o];
            for (s || (this.fixHooks[o] = s = je.test(o) ? this.mouseHooks : Oe.test(o) ? this.keyHooks : {}), i = s.props ? this.props.concat(s.props) : this.props, e = new oe.Event(r), t = i.length; t--;) n = i[t], e[n] = r[n];
            return e.target || (e.target = r.srcElement || he), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, s.filter ? s.filter(e, r) : e
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "), filter: function (e, t) {
                return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function (e, t) {
                var n, i, o, r = t.button, s = t.fromElement;
                return null == e.pageX && null != t.clientX && (i = e.target.ownerDocument || he, o = i.documentElement, n = i.body, e.pageX = t.clientX + (o && o.scrollLeft || n && n.scrollLeft || 0) - (o && o.clientLeft || n && n.clientLeft || 0), e.pageY = t.clientY + (o && o.scrollTop || n && n.scrollTop || 0) - (o && o.clientTop || n && n.clientTop || 0)), !e.relatedTarget && s && (e.relatedTarget = s === e.target ? t.toElement : s), e.which || void 0 === r || (e.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0), e
            }
        },
        special: {
            load: {noBubble: !0}, focus: {
                trigger: function () {
                    if (this !== h() && this.focus) try {
                        return this.focus(), !1
                    } catch (e) {
                    }
                }, delegateType: "focusin"
            }, blur: {
                trigger: function () {
                    return this === h() && this.blur ? (this.blur(), !1) : void 0
                }, delegateType: "focusout"
            }, click: {
                trigger: function () {
                    return oe.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                }, _default: function (e) {
                    return oe.nodeName(e.target, "a")
                }
            }, beforeunload: {
                postDispatch: function (e) {
                    void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
                }
            }
        },
        simulate: function (e, t, n, i) {
            var o = oe.extend(new oe.Event, n, {type: e, isSimulated: !0, originalEvent: {}});
            i ? oe.event.trigger(o, null, t) : oe.event.dispatch.call(t, o), o.isDefaultPrevented() && n.preventDefault()
        }
    }, oe.removeEvent = he.removeEventListener ? function (e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n, !1)
    } : function (e, t, n) {
        var i = "on" + t;
        e.detachEvent && (typeof e[i] === ke && (e[i] = null), e.detachEvent(i, n))
    }, oe.Event = function (e, t) {
        return this instanceof oe.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.returnValue === !1 ? p : f) : this.type = e, t && oe.extend(this, t), this.timeStamp = e && e.timeStamp || oe.now(), void (this[oe.expando] = !0)) : new oe.Event(e, t)
    }, oe.Event.prototype = {
        isDefaultPrevented: f, isPropagationStopped: f, isImmediatePropagationStopped: f, preventDefault: function () {
            var e = this.originalEvent;
            this.isDefaultPrevented = p, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
        }, stopPropagation: function () {
            var e = this.originalEvent;
            this.isPropagationStopped = p, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
        }, stopImmediatePropagation: function () {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = p, e && e.stopImmediatePropagation && e.stopImmediatePropagation(), this.stopPropagation()
        }
    }, oe.each({mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout"}, function (e, t) {
        oe.event.special[e] = {
            delegateType: t, bindType: t, handle: function (e) {
                var n, i = this, o = e.relatedTarget, r = e.handleObj;
                return (!o || o !== i && !oe.contains(i, o)) && (e.type = r.origType, n = r.handler.apply(this, arguments), e.type = t), n
            }
        }
    }), ne.submitBubbles || (oe.event.special.submit = {
        setup: function () {
            return oe.nodeName(this, "form") ? !1 : void oe.event.add(this, "click._submit keypress._submit", function (e) {
                var t = e.target, n = oe.nodeName(t, "input") || oe.nodeName(t, "button") ? t.form : void 0;
                n && !oe._data(n, "submitBubbles") && (oe.event.add(n, "submit._submit", function (e) {
                    e._submit_bubble = !0
                }), oe._data(n, "submitBubbles", !0))
            })
        }, postDispatch: function (e) {
            e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && oe.event.simulate("submit", this.parentNode, e, !0))
        }, teardown: function () {
            return oe.nodeName(this, "form") ? !1 : void oe.event.remove(this, "._submit")
        }
    }), ne.changeBubbles || (oe.event.special.change = {
        setup: function () {
            return Me.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (oe.event.add(this, "propertychange._change", function (e) {
                "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
            }), oe.event.add(this, "click._change", function (e) {
                this._just_changed && !e.isTrigger && (this._just_changed = !1), oe.event.simulate("change", this, e, !0)
            })), !1) : void oe.event.add(this, "beforeactivate._change", function (e) {
                var t = e.target;
                Me.test(t.nodeName) && !oe._data(t, "changeBubbles") && (oe.event.add(t, "change._change", function (e) {
                    !this.parentNode || e.isSimulated || e.isTrigger || oe.event.simulate("change", this.parentNode, e, !0)
                }), oe._data(t, "changeBubbles", !0))
            })
        }, handle: function (e) {
            var t = e.target;
            return this !== t || e.isSimulated || e.isTrigger || "radio" !== t.type && "checkbox" !== t.type ? e.handleObj.handler.apply(this, arguments) : void 0
        }, teardown: function () {
            return oe.event.remove(this, "._change"), !Me.test(this.nodeName)
        }
    }), ne.focusinBubbles || oe.each({focus: "focusin", blur: "focusout"}, function (e, t) {
        var n = function (e) {
            oe.event.simulate(t, e.target, oe.event.fix(e), !0)
        };
        oe.event.special[t] = {
            setup: function () {
                var i = this.ownerDocument || this, o = oe._data(i, t);
                o || i.addEventListener(e, n, !0), oe._data(i, t, (o || 0) + 1)
            }, teardown: function () {
                var i = this.ownerDocument || this, o = oe._data(i, t) - 1;
                o ? oe._data(i, t, o) : (i.removeEventListener(e, n, !0), oe._removeData(i, t))
            }
        }
    }), oe.fn.extend({
        on: function (e, t, n, i, o) {
            var r, s;
            if ("object" == typeof e) {
                "string" != typeof t && (n = n || t, t = void 0);
                for (r in e) this.on(r, t, n, e[r], o);
                return this
            }
            if (null == n && null == i ? (i = t, n = t = void 0) : null == i && ("string" == typeof t ? (i = n, n = void 0) : (i = n, n = t, t = void 0)), i === !1) i = f; else if (!i) return this;
            return 1 === o && (s = i, i = function (e) {
                return oe().off(e), s.apply(this, arguments)
            }, i.guid = s.guid || (s.guid = oe.guid++)), this.each(function () {
                oe.event.add(this, e, i, n, t)
            })
        }, one: function (e, t, n, i) {
            return this.on(e, t, n, i, 1)
        }, off: function (e, t, n) {
            var i, o;
            if (e && e.preventDefault && e.handleObj) return i = e.handleObj, oe(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
            if ("object" == typeof e) {
                for (o in e) this.off(o, t, e[o]);
                return this
            }
            return (t === !1 || "function" == typeof t) && (n = t,
                t = void 0), n === !1 && (n = f), this.each(function () {
                oe.event.remove(this, e, n, t)
            })
        }, trigger: function (e, t) {
            return this.each(function () {
                oe.event.trigger(e, t, this)
            })
        }, triggerHandler: function (e, t) {
            var n = this[0];
            return n ? oe.event.trigger(e, t, n, !0) : void 0
        }
    });
    var De = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        Ie = / jQuery\d+="(?:null|\d+)"/g, Pe = new RegExp("<(?:" + De + ")[\\s/>]", "i"), ze = /^\s+/,
        We = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, qe = /<([\w:]+)/, Fe = /<tbody/i, Be = /<|&#?\w+;/,
        Re = /<(?:script|style|link)/i, Ue = /checked\s*(?:[^=]|=\s*.checked.)/i, Xe = /^$|\/(?:java|ecma)script/i, Ye = /^true\/(.*)/,
        Ve = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, Qe = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: ne.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        }, Ze = g(he), Ke = Ze.appendChild(he.createElement("div"));
    Qe.optgroup = Qe.option, Qe.tbody = Qe.tfoot = Qe.colgroup = Qe.caption = Qe.thead, Qe.th = Qe.td, oe.extend({
        clone: function (e, t, n) {
            var i, o, r, s, a, l = oe.contains(e.ownerDocument, e);
            if (ne.html5Clone || oe.isXMLDoc(e) || !Pe.test("<" + e.nodeName + ">") ? r = e.cloneNode(!0) : (Ke.innerHTML = e.outerHTML, Ke.removeChild(r = Ke.firstChild)), !(ne.noCloneEvent && ne.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || oe.isXMLDoc(e))) for (i = m(r), a = m(e), s = 0; null != (o = a[s]); ++s) i[s] && k(o, i[s]);
            if (t) if (n) for (a = a || m(e), i = i || m(r), s = 0; null != (o = a[s]); s++) _(o, i[s]); else _(e, r);
            return i = m(r, "script"), i.length > 0 && w(i, !l && m(e, "script")), i = a = o = null, r
        }, buildFragment: function (e, t, n, i) {
            for (var o, r, s, a, l, c, d, u = e.length, p = g(t), f = [], h = 0; u > h; h++) if (r = e[h], r || 0 === r) if ("object" === oe.type(r)) oe.merge(f, r.nodeType ? [r] : r); else if (Be.test(r)) {
                for (a = a || p.appendChild(t.createElement("div")), l = (qe.exec(r) || ["", ""])[1].toLowerCase(), d = Qe[l] || Qe._default, a.innerHTML = d[1] + r.replace(We, "<$1></$2>") + d[2], o = d[0]; o--;) a = a.lastChild;
                if (!ne.leadingWhitespace && ze.test(r) && f.push(t.createTextNode(ze.exec(r)[0])), !ne.tbody) for (r = "table" !== l || Fe.test(r) ? "<table>" !== d[1] || Fe.test(r) ? 0 : a : a.firstChild, o = r && r.childNodes.length; o--;) oe.nodeName(c = r.childNodes[o], "tbody") && !c.childNodes.length && r.removeChild(c);
                for (oe.merge(f, a.childNodes), a.textContent = ""; a.firstChild;) a.removeChild(a.firstChild);
                a = p.lastChild
            } else f.push(t.createTextNode(r));
            for (a && p.removeChild(a), ne.appendChecked || oe.grep(m(f, "input"), v), h = 0; r = f[h++];) if ((!i || -1 === oe.inArray(r, i)) && (s = oe.contains(r.ownerDocument, r), a = m(p.appendChild(r), "script"), s && w(a), n)) for (o = 0; r = a[o++];) Xe.test(r.type || "") && n.push(r);
            return a = null, p
        }, cleanData: function (e, t) {
            for (var n, i, o, r, s = 0, a = oe.expando, l = oe.cache, c = ne.deleteExpando, d = oe.event.special; null != (n = e[s]); s++) if ((t || oe.acceptData(n)) && (o = n[a], r = o && l[o])) {
                if (r.events) for (i in r.events) d[i] ? oe.event.remove(n, i) : oe.removeEvent(n, i, r.handle);
                l[o] && (delete l[o], c ? delete n[a] : typeof n.removeAttribute !== ke ? n.removeAttribute(a) : n[a] = null, V.push(o))
            }
        }
    }), oe.fn.extend({
        text: function (e) {
            return Ae(this, function (e) {
                return void 0 === e ? oe.text(this) : this.empty().append((this[0] && this[0].ownerDocument || he).createTextNode(e))
            }, null, e, arguments.length)
        }, append: function () {
            return this.domManip(arguments, function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = y(this, e);
                    t.appendChild(e)
                }
            })
        }, prepend: function () {
            return this.domManip(arguments, function (e) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = y(this, e);
                    t.insertBefore(e, t.firstChild)
                }
            })
        }, before: function () {
            return this.domManip(arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this)
            })
        }, after: function () {
            return this.domManip(arguments, function (e) {
                this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
            })
        }, remove: function (e, t) {
            for (var n, i = e ? oe.filter(e, this) : this, o = 0; null != (n = i[o]); o++) t || 1 !== n.nodeType || oe.cleanData(m(n)), n.parentNode && (t && oe.contains(n.ownerDocument, n) && w(m(n, "script")), n.parentNode.removeChild(n));
            return this
        }, empty: function () {
            for (var e, t = 0; null != (e = this[t]); t++) {
                for (1 === e.nodeType && oe.cleanData(m(e, !1)); e.firstChild;) e.removeChild(e.firstChild);
                e.options && oe.nodeName(e, "select") && (e.options.length = 0)
            }
            return this
        }, clone: function (e, t) {
            return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function () {
                return oe.clone(this, e, t)
            })
        }, html: function (e) {
            return Ae(this, function (e) {
                var t = this[0] || {}, n = 0, i = this.length;
                if (void 0 === e) return 1 === t.nodeType ? t.innerHTML.replace(Ie, "") : void 0;
                if (!("string" != typeof e || Re.test(e) || !ne.htmlSerialize && Pe.test(e) || !ne.leadingWhitespace && ze.test(e) || Qe[(qe.exec(e) || ["", ""])[1].toLowerCase()])) {
                    e = e.replace(We, "<$1></$2>");
                    try {
                        for (; i > n; n++) t = this[n] || {}, 1 === t.nodeType && (oe.cleanData(m(t, !1)), t.innerHTML = e);
                        t = 0
                    } catch (o) {
                    }
                }
                t && this.empty().append(e)
            }, null, e, arguments.length)
        }, replaceWith: function () {
            var e = arguments[0];
            return this.domManip(arguments, function (t) {
                e = this.parentNode, oe.cleanData(m(this)), e && e.replaceChild(t, this)
            }), e && (e.length || e.nodeType) ? this : this.remove()
        }, detach: function (e) {
            return this.remove(e, !0)
        }, domManip: function (e, t) {
            e = Z.apply([], e);
            var n, i, o, r, s, a, l = 0, c = this.length, d = this, u = c - 1, p = e[0], f = oe.isFunction(p);
            if (f || c > 1 && "string" == typeof p && !ne.checkClone && Ue.test(p)) return this.each(function (n) {
                var i = d.eq(n);
                f && (e[0] = p.call(this, n, i.html())), i.domManip(e, t)
            });
            if (c && (a = oe.buildFragment(e, this[0].ownerDocument, !1, this), n = a.firstChild, 1 === a.childNodes.length && (a = n), n)) {
                for (r = oe.map(m(a, "script"), x), o = r.length; c > l; l++) i = a, l !== u && (i = oe.clone(i, !0, !0), o && oe.merge(r, m(i, "script"))), t.call(this[l], i, l);
                if (o) for (s = r[r.length - 1].ownerDocument, oe.map(r, b), l = 0; o > l; l++) i = r[l], Xe.test(i.type || "") && !oe._data(i, "globalEval") && oe.contains(s, i) && (i.src ? oe._evalUrl && oe._evalUrl(i.src) : oe.globalEval((i.text || i.textContent || i.innerHTML || "").replace(Ve, "")));
                a = n = null
            }
            return this
        }
    }), oe.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (e, t) {
        oe.fn[e] = function (e) {
            for (var n, i = 0, o = [], r = oe(e), s = r.length - 1; s >= i; i++) n = i === s ? this : this.clone(!0), oe(r[i])[t](n), K.apply(o, n.get());
            return this.pushStack(o)
        }
    });
    var Ge, Je = {};
    !function () {
        var e;
        ne.shrinkWrapBlocks = function () {
            if (null != e) return e;
            e = !1;
            var t, n, i;
            return n = he.getElementsByTagName("body")[0], n && n.style ? (t = he.createElement("div"), i = he.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(t), typeof t.style.zoom !== ke && (t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", t.appendChild(he.createElement("div")).style.width = "5px", e = 3 !== t.offsetWidth), n.removeChild(i), e) : void 0
        }
    }();
    var et, tt, nt = /^margin/, it = new RegExp("^(" + Ce + ")(?!px)[a-z%]+$", "i"), ot = /^(top|right|bottom|left)$/;
    e.getComputedStyle ? (et = function (e) {
        return e.ownerDocument.defaultView.getComputedStyle(e, null)
    }, tt = function (e, t, n) {
        var i, o, r, s, a = e.style;
        return n = n || et(e), s = n ? n.getPropertyValue(t) || n[t] : void 0, n && ("" !== s || oe.contains(e.ownerDocument, e) || (s = oe.style(e, t)), it.test(s) && nt.test(t) && (i = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = o, a.maxWidth = r)), void 0 === s ? s : s + ""
    }) : he.documentElement.currentStyle && (et = function (e) {
        return e.currentStyle
    }, tt = function (e, t, n) {
        var i, o, r, s, a = e.style;
        return n = n || et(e), s = n ? n[t] : void 0, null == s && a && a[t] && (s = a[t]), it.test(s) && !ot.test(t) && (i = a.left, o = e.runtimeStyle, r = o && o.left, r && (o.left = e.currentStyle.left), a.left = "fontSize" === t ? "1em" : s, s = a.pixelLeft + "px", a.left = i, r && (o.left = r)), void 0 === s ? s : s + "" || "auto"
    }), !function () {
        function t() {
            var t, n, i, o;
            n = he.getElementsByTagName("body")[0], n && n.style && (t = he.createElement("div"), i = he.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(t), t.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", r = s = !1, l = !0, e.getComputedStyle && (r = "1%" !== (e.getComputedStyle(t, null) || {}).top, s = "4px" === (e.getComputedStyle(t, null) || {width: "4px"}).width, o = t.appendChild(he.createElement("div")), o.style.cssText = t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", o.style.marginRight = o.style.width = "0", t.style.width = "1px", l = !parseFloat((e.getComputedStyle(o, null) || {}).marginRight)), t.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", o = t.getElementsByTagName("td"), o[0].style.cssText = "margin:0;border:0;padding:0;display:none", a = 0 === o[0].offsetHeight, a && (o[0].style.display = "", o[1].style.display = "none", a = 0 === o[0].offsetHeight), n.removeChild(i))
        }

        var n, i, o, r, s, a, l;
        n = he.createElement("div"), n.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", o = n.getElementsByTagName("a")[0], (i = o && o.style) && (i.cssText = "float:left;opacity:.5", ne.opacity = "0.5" === i.opacity, ne.cssFloat = !!i.cssFloat, n.style.backgroundClip = "content-box", n.cloneNode(!0).style.backgroundClip = "", ne.clearCloneStyle = "content-box" === n.style.backgroundClip, ne.boxSizing = "" === i.boxSizing || "" === i.MozBoxSizing || "" === i.WebkitBoxSizing, oe.extend(ne, {
            reliableHiddenOffsets: function () {
                return null == a && t(), a
            }, boxSizingReliable: function () {
                return null == s && t(), s
            }, pixelPosition: function () {
                return null == r && t(), r
            }, reliableMarginRight: function () {
                return null == l && t(), l
            }
        }))
    }(), oe.swap = function (e, t, n, i) {
        var o, r, s = {};
        for (r in t) s[r] = e.style[r], e.style[r] = t[r];
        o = n.apply(e, i || []);
        for (r in t) e.style[r] = s[r];
        return o
    };
    var rt = /alpha\([^)]*\)/i, st = /opacity\s*=\s*([^)]*)/, at = /^(none|table(?!-c[ea]).+)/, lt = new RegExp("^(" + Ce + ")(.*)$", "i"),
        ct = new RegExp("^([+-])=(" + Ce + ")", "i"), dt = {position: "absolute", visibility: "hidden", display: "block"},
        ut = {letterSpacing: "0", fontWeight: "400"}, pt = ["Webkit", "O", "Moz", "ms"];
    oe.extend({
        cssHooks: {
            opacity: {
                get: function (e, t) {
                    if (t) {
                        var n = tt(e, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {"float": ne.cssFloat ? "cssFloat" : "styleFloat"},
        style: function (e, t, n, i) {
            if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                var o, r, s, a = oe.camelCase(t), l = e.style;
                if (t = oe.cssProps[a] || (oe.cssProps[a] = E(l, a)), s = oe.cssHooks[t] || oe.cssHooks[a], void 0 === n) return s && "get" in s && void 0 !== (o = s.get(e, !1, i)) ? o : l[t];
                if (r = typeof n, "string" === r && (o = ct.exec(n)) && (n = (o[1] + 1) * o[2] + parseFloat(oe.css(e, t)), r = "number"), null != n && n === n && ("number" !== r || oe.cssNumber[a] || (n += "px"), ne.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), !(s && "set" in s && void 0 === (n = s.set(e, n, i))))) try {
                    l[t] = n
                } catch (c) {
                }
            }
        },
        css: function (e, t, n, i) {
            var o, r, s, a = oe.camelCase(t);
            return t = oe.cssProps[a] || (oe.cssProps[a] = E(e.style, a)), s = oe.cssHooks[t] || oe.cssHooks[a], s && "get" in s && (r = s.get(e, !0, n)), void 0 === r && (r = tt(e, t, i)), "normal" === r && t in ut && (r = ut[t]), "" === n || n ? (o = parseFloat(r), n === !0 || oe.isNumeric(o) ? o || 0 : r) : r
        }
    }), oe.each(["height", "width"], function (e, t) {
        oe.cssHooks[t] = {
            get: function (e, n, i) {
                return n ? at.test(oe.css(e, "display")) && 0 === e.offsetWidth ? oe.swap(e, dt, function () {
                    return M(e, t, i)
                }) : M(e, t, i) : void 0
            }, set: function (e, n, i) {
                var o = i && et(e);
                return A(e, n, i ? L(e, t, i, ne.boxSizing && "border-box" === oe.css(e, "boxSizing", !1, o), o) : 0)
            }
        }
    }), ne.opacity || (oe.cssHooks.opacity = {
        get: function (e, t) {
            return st.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        }, set: function (e, t) {
            var n = e.style, i = e.currentStyle, o = oe.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                r = i && i.filter || n.filter || "";
            n.zoom = 1, (t >= 1 || "" === t) && "" === oe.trim(r.replace(rt, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === t || i && !i.filter) || (n.filter = rt.test(r) ? r.replace(rt, o) : r + " " + o)
        }
    }), oe.cssHooks.marginRight = C(ne.reliableMarginRight, function (e, t) {
        return t ? oe.swap(e, {display: "inline-block"}, tt, [e, "marginRight"]) : void 0
    }), oe.each({margin: "", padding: "", border: "Width"}, function (e, t) {
        oe.cssHooks[e + t] = {
            expand: function (n) {
                for (var i = 0, o = {}, r = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++) o[e + Ee[i] + t] = r[i] || r[i - 2] || r[0];
                return o
            }
        }, nt.test(e) || (oe.cssHooks[e + t].set = A)
    }), oe.fn.extend({
        css: function (e, t) {
            return Ae(this, function (e, t, n) {
                var i, o, r = {}, s = 0;
                if (oe.isArray(t)) {
                    for (i = et(e), o = t.length; o > s; s++) r[t[s]] = oe.css(e, t[s], !1, i);
                    return r
                }
                return void 0 !== n ? oe.style(e, t, n) : oe.css(e, t)
            }, e, t, arguments.length > 1)
        }, show: function () {
            return $(this, !0)
        }, hide: function () {
            return $(this)
        }, toggle: function (e) {
            return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
                $e(this) ? oe(this).show() : oe(this).hide()
            })
        }
    }), oe.Tween = O, O.prototype = {
        constructor: O, init: function (e, t, n, i, o, r) {
            this.elem = e, this.prop = n, this.easing = o || "swing", this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = r || (oe.cssNumber[n] ? "" : "px")
        }, cur: function () {
            var e = O.propHooks[this.prop];
            return e && e.get ? e.get(this) : O.propHooks._default.get(this)
        }, run: function (e) {
            var t, n = O.propHooks[this.prop];
            return this.pos = t = this.options.duration ? oe.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : O.propHooks._default.set(this), this
        }
    }, O.prototype.init.prototype = O.prototype, O.propHooks = {
        _default: {
            get: function (e) {
                var t;
                return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = oe.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
            }, set: function (e) {
                oe.fx.step[e.prop] ? oe.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[oe.cssProps[e.prop]] || oe.cssHooks[e.prop]) ? oe.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }
        }
    }, O.propHooks.scrollTop = O.propHooks.scrollLeft = {
        set: function (e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, oe.easing = {
        linear: function (e) {
            return e
        }, swing: function (e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }
    }, oe.fx = O.prototype.init, oe.fx.step = {};
    var ft, ht, gt = /^(?:toggle|show|hide)$/, mt = new RegExp("^(?:([+-])=|)(" + Ce + ")([a-z%]*)$", "i"), vt = /queueHooks$/, yt = [D],
        xt = {
            "*": [function (e, t) {
                var n = this.createTween(e, t), i = n.cur(), o = mt.exec(t), r = o && o[3] || (oe.cssNumber[e] ? "" : "px"),
                    s = (oe.cssNumber[e] || "px" !== r && +i) && mt.exec(oe.css(n.elem, e)), a = 1, l = 20;
                if (s && s[3] !== r) {
                    r = r || s[3], o = o || [], s = +i || 1;
                    do a = a || ".5", s /= a, oe.style(n.elem, e, s + r); while (a !== (a = n.cur() / i) && 1 !== a && --l)
                }
                return o && (s = n.start = +s || +i || 0, n.unit = r, n.end = o[1] ? s + (o[1] + 1) * o[2] : +o[2]), n
            }]
        };
    oe.Animation = oe.extend(P, {
        tweener: function (e, t) {
            oe.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
            for (var n, i = 0, o = e.length; o > i; i++) n = e[i], xt[n] = xt[n] || [], xt[n].unshift(t)
        }, prefilter: function (e, t) {
            t ? yt.unshift(e) : yt.push(e)
        }
    }), oe.speed = function (e, t, n) {
        var i = e && "object" == typeof e ? oe.extend({}, e) : {
            complete: n || !n && t || oe.isFunction(e) && e,
            duration: e,
            easing: n && t || t && !oe.isFunction(t) && t
        };
        return i.duration = oe.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in oe.fx.speeds ? oe.fx.speeds[i.duration] : oe.fx.speeds._default, (null == i.queue || i.queue === !0) && (i.queue = "fx"), i.old = i.complete, i.complete = function () {
            oe.isFunction(i.old) && i.old.call(this), i.queue && oe.dequeue(this, i.queue)
        }, i
    }, oe.fn.extend({
        fadeTo: function (e, t, n, i) {
            return this.filter($e).css("opacity", 0).show().end().animate({opacity: t}, e, n, i)
        }, animate: function (e, t, n, i) {
            var o = oe.isEmptyObject(e), r = oe.speed(t, n, i), s = function () {
                var t = P(this, oe.extend({}, e), r);
                (o || oe._data(this, "finish")) && t.stop(!0)
            };
            return s.finish = s, o || r.queue === !1 ? this.each(s) : this.queue(r.queue, s)
        }, stop: function (e, t, n) {
            var i = function (e) {
                var t = e.stop;
                delete e.stop, t(n)
            };
            return "string" != typeof e && (n = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function () {
                var t = !0, o = null != e && e + "queueHooks", r = oe.timers, s = oe._data(this);
                if (o) s[o] && s[o].stop && i(s[o]); else for (o in s) s[o] && s[o].stop && vt.test(o) && i(s[o]);
                for (o = r.length; o--;) r[o].elem !== this || null != e && r[o].queue !== e || (r[o].anim.stop(n), t = !1, r.splice(o, 1));
                (t || !n) && oe.dequeue(this, e)
            })
        }, finish: function (e) {
            return e !== !1 && (e = e || "fx"), this.each(function () {
                var t, n = oe._data(this), i = n[e + "queue"], o = n[e + "queueHooks"], r = oe.timers, s = i ? i.length : 0;
                for (n.finish = !0, oe.queue(this, e, []), o && o.stop && o.stop.call(this, !0), t = r.length; t--;) r[t].elem === this && r[t].queue === e && (r[t].anim.stop(!0), r.splice(t, 1));
                for (t = 0; s > t; t++) i[t] && i[t].finish && i[t].finish.call(this);
                delete n.finish
            })
        }
    }), oe.each(["toggle", "show", "hide"], function (e, t) {
        var n = oe.fn[t];
        oe.fn[t] = function (e, i, o) {
            return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(N(t, !0), e, i, o)
        }
    }), oe.each({
        slideDown: N("show"),
        slideUp: N("hide"),
        slideToggle: N("toggle"),
        fadeIn: {opacity: "show"},
        fadeOut: {opacity: "hide"},
        fadeToggle: {opacity: "toggle"}
    }, function (e, t) {
        oe.fn[e] = function (e, n, i) {
            return this.animate(t, e, n, i)
        }
    }), oe.timers = [], oe.fx.tick = function () {
        var e, t = oe.timers, n = 0;
        for (ft = oe.now(); n < t.length; n++) e = t[n], e() || t[n] !== e || t.splice(n--, 1);
        t.length || oe.fx.stop(), ft = void 0
    }, oe.fx.timer = function (e) {
        oe.timers.push(e), e() ? oe.fx.start() : oe.timers.pop()
    }, oe.fx.interval = 13, oe.fx.start = function () {
        ht || (ht = setInterval(oe.fx.tick, oe.fx.interval))
    }, oe.fx.stop = function () {
        clearInterval(ht), ht = null
    }, oe.fx.speeds = {slow: 600, fast: 200, _default: 400}, oe.fn.delay = function (e, t) {
        return e = oe.fx ? oe.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function (t, n) {
            var i = setTimeout(t, e);
            n.stop = function () {
                clearTimeout(i)
            }
        })
    }, function () {
        var e, t, n, i, o;
        t = he.createElement("div"), t.setAttribute("className", "t"), t.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", i = t.getElementsByTagName("a")[0], n = he.createElement("select"), o = n.appendChild(he.createElement("option")), e = t.getElementsByTagName("input")[0], i.style.cssText = "top:1px", ne.getSetAttribute = "t" !== t.className, ne.style = /top/.test(i.getAttribute("style")), ne.hrefNormalized = "/a" === i.getAttribute("href"), ne.checkOn = !!e.value, ne.optSelected = o.selected, ne.enctype = !!he.createElement("form").enctype, n.disabled = !0, ne.optDisabled = !o.disabled, e = he.createElement("input"), e.setAttribute("value", ""), ne.input = "" === e.getAttribute("value"), e.value = "t", e.setAttribute("type", "radio"), ne.radioValue = "t" === e.value
    }();
    var bt = /\r/g;
    oe.fn.extend({
        val: function (e) {
            var t, n, i, o = this[0];
            return arguments.length ? (i = oe.isFunction(e), this.each(function (n) {
                var o;
                1 === this.nodeType && (o = i ? e.call(this, n, oe(this).val()) : e, null == o ? o = "" : "number" == typeof o ? o += "" : oe.isArray(o) && (o = oe.map(o, function (e) {
                    return null == e ? "" : e + ""
                })), t = oe.valHooks[this.type] || oe.valHooks[this.nodeName.toLowerCase()], t && "set" in t && void 0 !== t.set(this, o, "value") || (this.value = o))
            })) : o ? (t = oe.valHooks[o.type] || oe.valHooks[o.nodeName.toLowerCase()], t && "get" in t && void 0 !== (n = t.get(o, "value")) ? n : (n = o.value, "string" == typeof n ? n.replace(bt, "") : null == n ? "" : n)) : void 0
        }
    }), oe.extend({
        valHooks: {
            option: {
                get: function (e) {
                    var t = oe.find.attr(e, "value");
                    return null != t ? t : oe.trim(oe.text(e))
                }
            }, select: {
                get: function (e) {
                    for (var t, n, i = e.options, o = e.selectedIndex, r = "select-one" === e.type || 0 > o, s = r ? null : [], a = r ? o + 1 : i.length, l = 0 > o ? a : r ? o : 0; a > l; l++) if (n = i[l], !(!n.selected && l !== o || (ne.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && oe.nodeName(n.parentNode, "optgroup"))) {
                        if (t = oe(n).val(), r) return t;
                        s.push(t)
                    }
                    return s
                }, set: function (e, t) {
                    for (var n, i, o = e.options, r = oe.makeArray(t), s = o.length; s--;) if (i = o[s], oe.inArray(oe.valHooks.option.get(i), r) >= 0) try {
                        i.selected = n = !0
                    } catch (a) {
                        i.scrollHeight
                    } else i.selected = !1;
                    return n || (e.selectedIndex = -1), o
                }
            }
        }
    }), oe.each(["radio", "checkbox"], function () {
        oe.valHooks[this] = {
            set: function (e, t) {
                return oe.isArray(t) ? e.checked = oe.inArray(oe(e).val(), t) >= 0 : void 0
            }
        }, ne.checkOn || (oe.valHooks[this].get = function (e) {
            return null === e.getAttribute("value") ? "on" : e.value
        })
    });
    var wt, _t, kt = oe.expr.attrHandle, Tt = /^(?:checked|selected)$/i, St = ne.getSetAttribute, Ct = ne.input;
    oe.fn.extend({
        attr: function (e, t) {
            return Ae(this, oe.attr, e, t, arguments.length > 1)
        }, removeAttr: function (e) {
            return this.each(function () {
                oe.removeAttr(this, e)
            })
        }
    }), oe.extend({
        attr: function (e, t, n) {
            var i, o, r = e.nodeType;
            return e && 3 !== r && 8 !== r && 2 !== r ? typeof e.getAttribute === ke ? oe.prop(e, t, n) : (1 === r && oe.isXMLDoc(e) || (t = t.toLowerCase(), i = oe.attrHooks[t] || (oe.expr.match.bool.test(t) ? _t : wt)), void 0 === n ? i && "get" in i && null !== (o = i.get(e, t)) ? o : (o = oe.find.attr(e, t), null == o ? void 0 : o) : null !== n ? i && "set" in i && void 0 !== (o = i.set(e, n, t)) ? o : (e.setAttribute(t, n + ""), n) : void oe.removeAttr(e, t)) : void 0
        }, removeAttr: function (e, t) {
            var n, i, o = 0, r = t && t.match(xe);
            if (r && 1 === e.nodeType) for (; n = r[o++];) i = oe.propFix[n] || n, oe.expr.match.bool.test(n) ? Ct && St || !Tt.test(n) ? e[i] = !1 : e[oe.camelCase("default-" + n)] = e[i] = !1 : oe.attr(e, n, ""), e.removeAttribute(St ? n : i)
        }, attrHooks: {
            type: {
                set: function (e, t) {
                    if (!ne.radioValue && "radio" === t && oe.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            }
        }
    }), _t = {
        set: function (e, t, n) {
            return t === !1 ? oe.removeAttr(e, n) : Ct && St || !Tt.test(n) ? e.setAttribute(!St && oe.propFix[n] || n, n) : e[oe.camelCase("default-" + n)] = e[n] = !0, n
        }
    }, oe.each(oe.expr.match.bool.source.match(/\w+/g), function (e, t) {
        var n = kt[t] || oe.find.attr;
        kt[t] = Ct && St || !Tt.test(t) ? function (e, t, i) {
            var o, r;
            return i || (r = kt[t], kt[t] = o, o = null != n(e, t, i) ? t.toLowerCase() : null, kt[t] = r), o
        } : function (e, t, n) {
            return n ? void 0 : e[oe.camelCase("default-" + t)] ? t.toLowerCase() : null
        }
    }), Ct && St || (oe.attrHooks.value = {
        set: function (e, t, n) {
            return oe.nodeName(e, "input") ? void (e.defaultValue = t) : wt && wt.set(e, t, n)
        }
    }), St || (wt = {
        set: function (e, t, n) {
            var i = e.getAttributeNode(n);
            return i || e.setAttributeNode(i = e.ownerDocument.createAttribute(n)), i.value = t += "", "value" === n || t === e.getAttribute(n) ? t : void 0
        }
    }, kt.id = kt.name = kt.coords = function (e, t, n) {
        var i;
        return n ? void 0 : (i = e.getAttributeNode(t)) && "" !== i.value ? i.value : null
    }, oe.valHooks.button = {
        get: function (e, t) {
            var n = e.getAttributeNode(t);
            return n && n.specified ? n.value : void 0
        }, set: wt.set
    }, oe.attrHooks.contenteditable = {
        set: function (e, t, n) {
            wt.set(e, "" === t ? !1 : t, n)
        }
    }, oe.each(["width", "height"], function (e, t) {
        oe.attrHooks[t] = {
            set: function (e, n) {
                return "" === n ? (e.setAttribute(t, "auto"), n) : void 0
            }
        }
    })), ne.style || (oe.attrHooks.style = {
        get: function (e) {
            return e.style.cssText || void 0
        }, set: function (e, t) {
            return e.style.cssText = t + ""
        }
    });
    var Et = /^(?:input|select|textarea|button|object)$/i, $t = /^(?:a|area)$/i;
    oe.fn.extend({
        prop: function (e, t) {
            return Ae(this, oe.prop, e, t, arguments.length > 1)
        }, removeProp: function (e) {
            return e = oe.propFix[e] || e, this.each(function () {
                try {
                    this[e] = void 0, delete this[e]
                } catch (t) {
                }
            })
        }
    }), oe.extend({
        propFix: {"for": "htmlFor", "class": "className"}, prop: function (e, t, n) {
            var i, o, r, s = e.nodeType;
            return e && 3 !== s && 8 !== s && 2 !== s ? (r = 1 !== s || !oe.isXMLDoc(e), r && (t = oe.propFix[t] || t, o = oe.propHooks[t]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(e, n, t)) ? i : e[t] = n : o && "get" in o && null !== (i = o.get(e, t)) ? i : e[t]) : void 0
        }, propHooks: {
            tabIndex: {
                get: function (e) {
                    var t = oe.find.attr(e, "tabindex");
                    return t ? parseInt(t, 10) : Et.test(e.nodeName) || $t.test(e.nodeName) && e.href ? 0 : -1
                }
            }
        }
    }), ne.hrefNormalized || oe.each(["href", "src"], function (e, t) {
        oe.propHooks[t] = {
            get: function (e) {
                return e.getAttribute(t, 4)
            }
        }
    }), ne.optSelected || (oe.propHooks.selected = {
        get: function (e) {
            var t = e.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }
    }), oe.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        oe.propFix[this.toLowerCase()] = this
    }), ne.enctype || (oe.propFix.enctype = "encoding");
    var At = /[\t\r\n\f]/g;
    oe.fn.extend({
        addClass: function (e) {
            var t, n, i, o, r, s, a = 0, l = this.length, c = "string" == typeof e && e;
            if (oe.isFunction(e)) return this.each(function (t) {
                oe(this).addClass(e.call(this, t, this.className))
            });
            if (c) for (t = (e || "").match(xe) || []; l > a; a++) if (n = this[a], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(At, " ") : " ")) {
                for (r = 0; o = t[r++];) i.indexOf(" " + o + " ") < 0 && (i += o + " ");
                s = oe.trim(i), n.className !== s && (n.className = s)
            }
            return this
        }, removeClass: function (e) {
            var t, n, i, o, r, s, a = 0, l = this.length, c = 0 === arguments.length || "string" == typeof e && e;
            if (oe.isFunction(e)) return this.each(function (t) {
                oe(this).removeClass(e.call(this, t, this.className))
            });
            if (c) for (t = (e || "").match(xe) || []; l > a; a++) if (n = this[a], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(At, " ") : "")) {
                for (r = 0; o = t[r++];) for (; i.indexOf(" " + o + " ") >= 0;) i = i.replace(" " + o + " ", " ");
                s = e ? oe.trim(i) : "", n.className !== s && (n.className = s)
            }
            return this
        }, toggleClass: function (e, t) {
            var n = typeof e;
            return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : this.each(oe.isFunction(e) ? function (n) {
                oe(this).toggleClass(e.call(this, n, this.className, t), t)
            } : function () {
                if ("string" === n) for (var t, i = 0, o = oe(this), r = e.match(xe) || []; t = r[i++];) o.hasClass(t) ? o.removeClass(t) : o.addClass(t); else (n === ke || "boolean" === n) && (this.className && oe._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : oe._data(this, "__className__") || "")
            })
        }, hasClass: function (e) {
            for (var t = " " + e + " ", n = 0, i = this.length; i > n; n++) if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(At, " ").indexOf(t) >= 0) return !0;
            return !1
        }
    }), oe.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (e, t) {
        oe.fn[t] = function (e, n) {
            return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }
    }), oe.fn.extend({
        hover: function (e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }, bind: function (e, t, n) {
            return this.on(e, null, t, n)
        }, unbind: function (e, t) {
            return this.off(e, null, t)
        }, delegate: function (e, t, n, i) {
            return this.on(t, e, n, i)
        }, undelegate: function (e, t, n) {
            return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
        }
    });
    var Lt = oe.now(), Mt = /\?/,
        Ot = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    oe.parseJSON = function (t) {
        if (e.JSON && e.JSON.parse) return e.JSON.parse(t + "");
        var n, i = null, o = oe.trim(t + "");
        return o && !oe.trim(o.replace(Ot, function (e, t, o, r) {
            return n && t && (i = 0), 0 === i ? e : (n = o || t, i += !r - !o, "")
        })) ? Function("return " + o)() : oe.error("Invalid JSON: " + t)
    }, oe.parseXML = function (t) {
        var n, i;
        if (!t || "string" != typeof t) return null;
        try {
            e.DOMParser ? (i = new DOMParser, n = i.parseFromString(t, "text/xml")) : (n = new ActiveXObject("Microsoft.XMLDOM"), n.async = "false", n.loadXML(t))
        } catch (o) {
            n = void 0
        }
        return n && n.documentElement && !n.getElementsByTagName("parsererror").length || oe.error("Invalid XML: " + t), n
    };
    var jt, Nt, Ht = /#.*$/, Dt = /([?&])_=[^&]*/, It = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Pt = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, zt = /^(?:GET|HEAD)$/, Wt = /^\/\//,
        qt = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, Ft = {}, Bt = {}, Rt = "*/".concat("*");
    try {
        Nt = location.href
    } catch (Ut) {
        Nt = he.createElement("a"), Nt.href = "", Nt = Nt.href
    }
    jt = qt.exec(Nt.toLowerCase()) || [], oe.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Nt,
            type: "GET",
            isLocal: Pt.test(jt[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Rt,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {xml: /xml/, html: /html/, json: /json/},
            responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
            converters: {"* text": String, "text html": !0, "text json": oe.parseJSON, "text xml": oe.parseXML},
            flatOptions: {url: !0, context: !0}
        },
        ajaxSetup: function (e, t) {
            return t ? q(q(e, oe.ajaxSettings), t) : q(oe.ajaxSettings, e)
        },
        ajaxPrefilter: z(Ft),
        ajaxTransport: z(Bt),
        ajax: function (e, t) {
            function n(e, t, n, i) {
                var o, d, v, y, b, _ = t;
                2 !== x && (x = 2, a && clearTimeout(a), c = void 0, s = i || "", w.readyState = e > 0 ? 4 : 0, o = e >= 200 && 300 > e || 304 === e, n && (y = F(u, w, n)), y = B(u, y, w, o), o ? (u.ifModified && (b = w.getResponseHeader("Last-Modified"), b && (oe.lastModified[r] = b), b = w.getResponseHeader("etag"), b && (oe.etag[r] = b)), 204 === e || "HEAD" === u.type ? _ = "nocontent" : 304 === e ? _ = "notmodified" : (_ = y.state, d = y.data, v = y.error, o = !v)) : (v = _, (e || !_) && (_ = "error", 0 > e && (e = 0))), w.status = e, w.statusText = (t || _) + "", o ? h.resolveWith(p, [d, _, w]) : h.rejectWith(p, [w, _, v]), w.statusCode(m), m = void 0, l && f.trigger(o ? "ajaxSuccess" : "ajaxError", [w, u, o ? d : v]), g.fireWith(p, [w, _]), l && (f.trigger("ajaxComplete", [w, u]), --oe.active || oe.event.trigger("ajaxStop")))
            }

            "object" == typeof e && (t = e, e = void 0), t = t || {};
            var i, o, r, s, a, l, c, d, u = oe.ajaxSetup({}, t), p = u.context || u,
                f = u.context && (p.nodeType || p.jquery) ? oe(p) : oe.event, h = oe.Deferred(), g = oe.Callbacks("once memory"),
                m = u.statusCode || {}, v = {}, y = {}, x = 0, b = "canceled", w = {
                    readyState: 0, getResponseHeader: function (e) {
                        var t;
                        if (2 === x) {
                            if (!d) for (d = {}; t = It.exec(s);) d[t[1].toLowerCase()] = t[2];
                            t = d[e.toLowerCase()]
                        }
                        return null == t ? null : t
                    }, getAllResponseHeaders: function () {
                        return 2 === x ? s : null
                    }, setRequestHeader: function (e, t) {
                        var n = e.toLowerCase();
                        return x || (e = y[n] = y[n] || e, v[e] = t), this
                    }, overrideMimeType: function (e) {
                        return x || (u.mimeType = e), this
                    }, statusCode: function (e) {
                        var t;
                        if (e) if (2 > x) for (t in e) m[t] = [m[t], e[t]]; else w.always(e[w.status]);
                        return this
                    }, abort: function (e) {
                        var t = e || b;
                        return c && c.abort(t), n(0, t), this
                    }
                };
            if (h.promise(w).complete = g.add, w.success = w.done, w.error = w.fail, u.url = ((e || u.url || Nt) + "").replace(Ht, "").replace(Wt, jt[1] + "//"), u.type = t.method || t.type || u.method || u.type, u.dataTypes = oe.trim(u.dataType || "*").toLowerCase().match(xe) || [""], null == u.crossDomain && (i = qt.exec(u.url.toLowerCase()), u.crossDomain = !(!i || i[1] === jt[1] && i[2] === jt[2] && (i[3] || ("http:" === i[1] ? "80" : "443")) === (jt[3] || ("http:" === jt[1] ? "80" : "443")))), u.data && u.processData && "string" != typeof u.data && (u.data = oe.param(u.data, u.traditional)), W(Ft, u, t, w), 2 === x) return w;
            l = u.global, l && 0 === oe.active++ && oe.event.trigger("ajaxStart"), u.type = u.type.toUpperCase(), u.hasContent = !zt.test(u.type), r = u.url, u.hasContent || (u.data && (r = u.url += (Mt.test(r) ? "&" : "?") + u.data, delete u.data), u.cache === !1 && (u.url = Dt.test(r) ? r.replace(Dt, "$1_=" + Lt++) : r + (Mt.test(r) ? "&" : "?") + "_=" + Lt++)), u.ifModified && (oe.lastModified[r] && w.setRequestHeader("If-Modified-Since", oe.lastModified[r]), oe.etag[r] && w.setRequestHeader("If-None-Match", oe.etag[r])), (u.data && u.hasContent && u.contentType !== !1 || t.contentType) && w.setRequestHeader("Content-Type", u.contentType), w.setRequestHeader("Accept", u.dataTypes[0] && u.accepts[u.dataTypes[0]] ? u.accepts[u.dataTypes[0]] + ("*" !== u.dataTypes[0] ? ", " + Rt + "; q=0.01" : "") : u.accepts["*"]);
            for (o in u.headers) w.setRequestHeader(o, u.headers[o]);
            if (u.beforeSend && (u.beforeSend.call(p, w, u) === !1 || 2 === x)) return w.abort();
            b = "abort";
            for (o in{success: 1, error: 1, complete: 1}) w[o](u[o]);
            if (c = W(Bt, u, t, w)) {
                w.readyState = 1, l && f.trigger("ajaxSend", [w, u]), u.async && u.timeout > 0 && (a = setTimeout(function () {
                    w.abort("timeout")
                }, u.timeout));
                try {
                    x = 1, c.send(v, n)
                } catch (_) {
                    if (!(2 > x)) throw _;
                    n(-1, _)
                }
            } else n(-1, "No Transport");
            return w
        },
        getJSON: function (e, t, n) {
            return oe.get(e, t, n, "json")
        },
        getScript: function (e, t) {
            return oe.get(e, void 0, t, "script")
        }
    }), oe.each(["get", "post"], function (e, t) {
        oe[t] = function (e, n, i, o) {
            return oe.isFunction(n) && (o = o || i, i = n, n = void 0), oe.ajax({url: e, type: t, dataType: o, data: n, success: i})
        }
    }), oe.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
        oe.fn[t] = function (e) {
            return this.on(t, e)
        }
    }), oe._evalUrl = function (e) {
        return oe.ajax({url: e, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0})
    }, oe.fn.extend({
        wrapAll: function (e) {
            if (oe.isFunction(e)) return this.each(function (t) {
                oe(this).wrapAll(e.call(this, t))
            });
            if (this[0]) {
                var t = oe(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
                    for (var e = this; e.firstChild && 1 === e.firstChild.nodeType;) e = e.firstChild;
                    return e
                }).append(this)
            }
            return this
        }, wrapInner: function (e) {
            return this.each(oe.isFunction(e) ? function (t) {
                oe(this).wrapInner(e.call(this, t))
            } : function () {
                var t = oe(this), n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        }, wrap: function (e) {
            var t = oe.isFunction(e);
            return this.each(function (n) {
                oe(this).wrapAll(t ? e.call(this, n) : e)
            })
        }, unwrap: function () {
            return this.parent().each(function () {
                oe.nodeName(this, "body") || oe(this).replaceWith(this.childNodes);
            }).end()
        }
    }), oe.expr.filters.hidden = function (e) {
        return e.offsetWidth <= 0 && e.offsetHeight <= 0 || !ne.reliableHiddenOffsets() && "none" === (e.style && e.style.display || oe.css(e, "display"))
    }, oe.expr.filters.visible = function (e) {
        return !oe.expr.filters.hidden(e)
    };
    var Xt = /%20/g, Yt = /\[\]$/, Vt = /\r?\n/g, Qt = /^(?:submit|button|image|reset|file)$/i, Zt = /^(?:input|select|textarea|keygen)/i;
    oe.param = function (e, t) {
        var n, i = [], o = function (e, t) {
            t = oe.isFunction(t) ? t() : null == t ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
        };
        if (void 0 === t && (t = oe.ajaxSettings && oe.ajaxSettings.traditional), oe.isArray(e) || e.jquery && !oe.isPlainObject(e)) oe.each(e, function () {
            o(this.name, this.value)
        }); else for (n in e) R(n, e[n], t, o);
        return i.join("&").replace(Xt, "+")
    }, oe.fn.extend({
        serialize: function () {
            return oe.param(this.serializeArray())
        }, serializeArray: function () {
            return this.map(function () {
                var e = oe.prop(this, "elements");
                return e ? oe.makeArray(e) : this
            }).filter(function () {
                var e = this.type;
                return this.name && !oe(this).is(":disabled") && Zt.test(this.nodeName) && !Qt.test(e) && (this.checked || !Le.test(e))
            }).map(function (e, t) {
                var n = oe(this).val();
                return null == n ? null : oe.isArray(n) ? oe.map(n, function (e) {
                    return {name: t.name, value: e.replace(Vt, "\r\n")}
                }) : {name: t.name, value: n.replace(Vt, "\r\n")}
            }).get()
        }
    }), oe.ajaxSettings.xhr = void 0 !== e.ActiveXObject ? function () {
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && U() || X()
    } : U;
    var Kt = 0, Gt = {}, Jt = oe.ajaxSettings.xhr();
    e.ActiveXObject && oe(e).on("unload", function () {
        for (var e in Gt) Gt[e](void 0, !0)
    }), ne.cors = !!Jt && "withCredentials" in Jt, Jt = ne.ajax = !!Jt, Jt && oe.ajaxTransport(function (e) {
        if (!e.crossDomain || ne.cors) {
            var t;
            return {
                send: function (n, i) {
                    var o, r = e.xhr(), s = ++Kt;
                    if (r.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields) for (o in e.xhrFields) r[o] = e.xhrFields[o];
                    e.mimeType && r.overrideMimeType && r.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                    for (o in n) void 0 !== n[o] && r.setRequestHeader(o, n[o] + "");
                    r.send(e.hasContent && e.data || null), t = function (n, o) {
                        var a, l, c;
                        if (t && (o || 4 === r.readyState)) if (delete Gt[s], t = void 0, r.onreadystatechange = oe.noop, o) 4 !== r.readyState && r.abort(); else {
                            c = {}, a = r.status, "string" == typeof r.responseText && (c.text = r.responseText);
                            try {
                                l = r.statusText
                            } catch (d) {
                                l = ""
                            }
                            a || !e.isLocal || e.crossDomain ? 1223 === a && (a = 204) : a = c.text ? 200 : 404
                        }
                        c && i(a, l, c, r.getAllResponseHeaders())
                    }, e.async ? 4 === r.readyState ? setTimeout(t) : r.onreadystatechange = Gt[s] = t : t()
                }, abort: function () {
                    t && t(void 0, !0)
                }
            }
        }
    }), oe.ajaxSetup({
        accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
        contents: {script: /(?:java|ecma)script/},
        converters: {
            "text script": function (e) {
                return oe.globalEval(e), e
            }
        }
    }), oe.ajaxPrefilter("script", function (e) {
        void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
    }), oe.ajaxTransport("script", function (e) {
        if (e.crossDomain) {
            var t, n = he.head || oe("head")[0] || he.documentElement;
            return {
                send: function (i, o) {
                    t = he.createElement("script"), t.async = !0, e.scriptCharset && (t.charset = e.scriptCharset), t.src = e.url, t.onload = t.onreadystatechange = function (e, n) {
                        (n || !t.readyState || /loaded|complete/.test(t.readyState)) && (t.onload = t.onreadystatechange = null, t.parentNode && t.parentNode.removeChild(t), t = null, n || o(200, "success"))
                    }, n.insertBefore(t, n.firstChild)
                }, abort: function () {
                    t && t.onload(void 0, !0)
                }
            }
        }
    });
    var en = [], tn = /(=)\?(?=&|$)|\?\?/;
    oe.ajaxSetup({
        jsonp: "callback", jsonpCallback: function () {
            var e = en.pop() || oe.expando + "_" + Lt++;
            return this[e] = !0, e
        }
    }), oe.ajaxPrefilter("json jsonp", function (t, n, i) {
        var o, r, s,
            a = t.jsonp !== !1 && (tn.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && tn.test(t.data) && "data");
        return a || "jsonp" === t.dataTypes[0] ? (o = t.jsonpCallback = oe.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, a ? t[a] = t[a].replace(tn, "$1" + o) : t.jsonp !== !1 && (t.url += (Mt.test(t.url) ? "&" : "?") + t.jsonp + "=" + o), t.converters["script json"] = function () {
            return s || oe.error(o + " was not called"), s[0]
        }, t.dataTypes[0] = "json", r = e[o], e[o] = function () {
            s = arguments
        }, i.always(function () {
            e[o] = r, t[o] && (t.jsonpCallback = n.jsonpCallback, en.push(o)), s && oe.isFunction(r) && r(s[0]), s = r = void 0
        }), "script") : void 0
    }), oe.parseHTML = function (e, t, n) {
        if (!e || "string" != typeof e) return null;
        "boolean" == typeof t && (n = t, t = !1), t = t || he;
        var i = ue.exec(e), o = !n && [];
        return i ? [t.createElement(i[1])] : (i = oe.buildFragment([e], t, o), o && o.length && oe(o).remove(), oe.merge([], i.childNodes))
    };
    var nn = oe.fn.load;
    oe.fn.load = function (e, t, n) {
        if ("string" != typeof e && nn) return nn.apply(this, arguments);
        var i, o, r, s = this, a = e.indexOf(" ");
        return a >= 0 && (i = oe.trim(e.slice(a, e.length)), e = e.slice(0, a)), oe.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (r = "POST"), s.length > 0 && oe.ajax({
            url: e,
            type: r,
            dataType: "html",
            data: t
        }).done(function (e) {
            o = arguments, s.html(i ? oe("<div>").append(oe.parseHTML(e)).find(i) : e)
        }).complete(n && function (e, t) {
            s.each(n, o || [e.responseText, t, e])
        }), this
    }, oe.expr.filters.animated = function (e) {
        return oe.grep(oe.timers, function (t) {
            return e === t.elem
        }).length
    };
    var on = e.document.documentElement;
    oe.offset = {
        setOffset: function (e, t, n) {
            var i, o, r, s, a, l, c, d = oe.css(e, "position"), u = oe(e), p = {};
            "static" === d && (e.style.position = "relative"), a = u.offset(), r = oe.css(e, "top"), l = oe.css(e, "left"), c = ("absolute" === d || "fixed" === d) && oe.inArray("auto", [r, l]) > -1, c ? (i = u.position(), s = i.top, o = i.left) : (s = parseFloat(r) || 0, o = parseFloat(l) || 0), oe.isFunction(t) && (t = t.call(e, n, a)), null != t.top && (p.top = t.top - a.top + s), null != t.left && (p.left = t.left - a.left + o), "using" in t ? t.using.call(e, p) : u.css(p)
        }
    }, oe.fn.extend({
        offset: function (e) {
            if (arguments.length) return void 0 === e ? this : this.each(function (t) {
                oe.offset.setOffset(this, e, t)
            });
            var t, n, i = {top: 0, left: 0}, o = this[0], r = o && o.ownerDocument;
            return r ? (t = r.documentElement, oe.contains(t, o) ? (typeof o.getBoundingClientRect !== ke && (i = o.getBoundingClientRect()), n = Y(r), {
                top: i.top + (n.pageYOffset || t.scrollTop) - (t.clientTop || 0),
                left: i.left + (n.pageXOffset || t.scrollLeft) - (t.clientLeft || 0)
            }) : i) : void 0
        }, position: function () {
            if (this[0]) {
                var e, t, n = {top: 0, left: 0}, i = this[0];
                return "fixed" === oe.css(i, "position") ? t = i.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), oe.nodeName(e[0], "html") || (n = e.offset()), n.top += oe.css(e[0], "borderTopWidth", !0), n.left += oe.css(e[0], "borderLeftWidth", !0)), {
                    top: t.top - n.top - oe.css(i, "marginTop", !0),
                    left: t.left - n.left - oe.css(i, "marginLeft", !0)
                }
            }
        }, offsetParent: function () {
            return this.map(function () {
                for (var e = this.offsetParent || on; e && !oe.nodeName(e, "html") && "static" === oe.css(e, "position");) e = e.offsetParent;
                return e || on
            })
        }
    }), oe.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (e, t) {
        var n = /Y/.test(t);
        oe.fn[e] = function (i) {
            return Ae(this, function (e, i, o) {
                var r = Y(e);
                return void 0 === o ? r ? t in r ? r[t] : r.document.documentElement[i] : e[i] : void (r ? r.scrollTo(n ? oe(r).scrollLeft() : o, n ? o : oe(r).scrollTop()) : e[i] = o)
            }, e, i, arguments.length, null)
        }
    }), oe.each(["top", "left"], function (e, t) {
        oe.cssHooks[t] = C(ne.pixelPosition, function (e, n) {
            return n ? (n = tt(e, t), it.test(n) ? oe(e).position()[t] + "px" : n) : void 0
        })
    }), oe.each({Height: "height", Width: "width"}, function (e, t) {
        oe.each({padding: "inner" + e, content: t, "": "outer" + e}, function (n, i) {
            oe.fn[i] = function (i, o) {
                var r = arguments.length && (n || "boolean" != typeof i), s = n || (i === !0 || o === !0 ? "margin" : "border");
                return Ae(this, function (t, n, i) {
                    var o;
                    return oe.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (o = t.documentElement, Math.max(t.body["scroll" + e], o["scroll" + e], t.body["offset" + e], o["offset" + e], o["client" + e])) : void 0 === i ? oe.css(t, n, s) : oe.style(t, n, i, s)
                }, t, r ? i : void 0, r, null)
            }
        })
    }), oe.fn.size = function () {
        return this.length
    }, oe.fn.andSelf = oe.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
        return oe
    });
    var rn = e.jQuery, sn = e.$;
    return oe.noConflict = function (t) {
        return e.$ === oe && (e.$ = sn), t && e.jQuery === oe && (e.jQuery = rn), oe
    }, typeof t === ke && (e.jQuery = e.$ = oe), oe
}), !function (e) {
    if ("object" == typeof exports && "undefined" != typeof module) module.exports = e(); else if ("function" == typeof define && define.amd) define([], e); else {
        var t;
        t = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, t.Clipboard = e()
    }
}(function () {
    return function e(t, n, i) {
        function o(s, a) {
            if (!n[s]) {
                if (!t[s]) {
                    var l = "function" == typeof require && require;
                    if (!a && l) return l(s, !0);
                    if (r) return r(s, !0);
                    var c = new Error("Cannot find module '" + s + "'");
                    throw c.code = "MODULE_NOT_FOUND", c
                }
                var d = n[s] = {exports: {}};
                t[s][0].call(d.exports, function (e) {
                    var n = t[s][1][e];
                    return o(n ? n : e)
                }, d, d.exports, e, t, n, i)
            }
            return n[s].exports
        }

        for (var r = "function" == typeof require && require, s = 0; s < i.length; s++) o(i[s]);
        return o
    }({
        1: [function (e, t) {
            var n = e("matches-selector");
            t.exports = function (e, t, i) {
                for (var o = i ? e : e.parentNode; o && o !== document;) {
                    if (n(o, t)) return o;
                    o = o.parentNode
                }
            }
        }, {"matches-selector": 2}], 2: [function (e, t) {
            function n(e, t) {
                if (o) return o.call(e, t);
                for (var n = e.parentNode.querySelectorAll(t), i = 0; i < n.length; ++i) if (n[i] == e) return !0;
                return !1
            }

            var i = Element.prototype,
                o = i.matchesSelector || i.webkitMatchesSelector || i.mozMatchesSelector || i.msMatchesSelector || i.oMatchesSelector;
            t.exports = n
        }, {}], 3: [function (e, t) {
            function n(e, t, n) {
                var o = i.apply(this, arguments);
                return e.addEventListener(n, o), {
                    destroy: function () {
                        e.removeEventListener(n, o)
                    }
                }
            }

            function i(e, t, n, i) {
                return function (n) {
                    n.delegateTarget = o(n.target, t, !0), n.delegateTarget && i.call(e, n)
                }
            }

            var o = e("closest");
            t.exports = n
        }, {closest: 1}], 4: [function (e, t, n) {
            n.node = function (e) {
                return void 0 !== e && e instanceof HTMLElement && 1 === e.nodeType
            }, n.nodeList = function (e) {
                var t = Object.prototype.toString.call(e);
                return void 0 !== e && ("[object NodeList]" === t || "[object HTMLCollection]" === t) && "length" in e && (0 === e.length || n.node(e[0]))
            }, n.string = function (e) {
                return "string" == typeof e || e instanceof String
            }, n["function"] = function (e) {
                var t = Object.prototype.toString.call(e);
                return "[object Function]" === t
            }
        }, {}], 5: [function (e, t) {
            function n(e, t, n) {
                if (!e && !t && !n) throw new Error("Missing required arguments");
                if (!s.string(t)) throw new TypeError("Second argument must be a String");
                if (!s["function"](n)) throw new TypeError("Third argument must be a Function");
                if (s.node(e)) return i(e, t, n);
                if (s.nodeList(e)) return o(e, t, n);
                if (s.string(e)) return r(e, t, n);
                throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList")
            }

            function i(e, t, n) {
                return e.addEventListener(t, n), {
                    destroy: function () {
                        e.removeEventListener(t, n)
                    }
                }
            }

            function o(e, t, n) {
                return Array.prototype.forEach.call(e, function (e) {
                    e.addEventListener(t, n)
                }), {
                    destroy: function () {
                        Array.prototype.forEach.call(e, function (e) {
                            e.removeEventListener(t, n)
                        })
                    }
                }
            }

            function r(e, t, n) {
                return a(document.body, e, t, n)
            }

            var s = e("./is"), a = e("delegate");
            t.exports = n
        }, {"./is": 4, delegate: 3}], 6: [function (e, t) {
            function n(e) {
                var t;
                if ("INPUT" === e.nodeName || "TEXTAREA" === e.nodeName) e.focus(), e.setSelectionRange(0, e.value.length), t = e.value; else {
                    e.hasAttribute("contenteditable") && e.focus();
                    var n = window.getSelection(), i = document.createRange();
                    i.selectNodeContents(e), n.removeAllRanges(), n.addRange(i), t = n.toString()
                }
                return t
            }

            t.exports = n
        }, {}], 7: [function (e, t) {
            function n() {
            }

            n.prototype = {
                on: function (e, t, n) {
                    var i = this.e || (this.e = {});
                    return (i[e] || (i[e] = [])).push({fn: t, ctx: n}), this
                }, once: function (e, t, n) {
                    function i() {
                        o.off(e, i), t.apply(n, arguments)
                    }

                    var o = this;
                    return i._ = t, this.on(e, i, n)
                }, emit: function (e) {
                    var t = [].slice.call(arguments, 1), n = ((this.e || (this.e = {}))[e] || []).slice(), i = 0, o = n.length;
                    for (i; o > i; i++) n[i].fn.apply(n[i].ctx, t);
                    return this
                }, off: function (e, t) {
                    var n = this.e || (this.e = {}), i = n[e], o = [];
                    if (i && t) for (var r = 0, s = i.length; s > r; r++) i[r].fn !== t && i[r].fn._ !== t && o.push(i[r]);
                    return o.length ? n[e] = o : delete n[e], this
                }
            }, t.exports = n
        }, {}], 8: [function (e, t, n) {
            "use strict";

            function i(e) {
                return e && e.__esModule ? e : {"default": e}
            }

            function o(e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }

            n.__esModule = !0;
            var r = function () {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var i = t[n];
                        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                    }
                }

                return function (t, n, i) {
                    return n && e(t.prototype, n), i && e(t, i), t
                }
            }(), s = e("select"), a = i(s), l = function () {
                function e(t) {
                    o(this, e), this.resolveOptions(t), this.initSelection()
                }

                return e.prototype.resolveOptions = function () {
                    var e = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0];
                    this.action = e.action, this.emitter = e.emitter, this.target = e.target, this.text = e.text, this.trigger = e.trigger, this.selectedText = ""
                }, e.prototype.initSelection = function () {
                    if (this.text && this.target) throw new Error('Multiple attributes declared, use either "target" or "text"');
                    if (this.text) this.selectFake(); else {
                        if (!this.target) throw new Error('Missing required attributes, use either "target" or "text"');
                        this.selectTarget()
                    }
                }, e.prototype.selectFake = function () {
                    var e = this;
                    this.removeFake(), this.fakeHandler = document.body.addEventListener("click", function () {
                        return e.removeFake()
                    }), this.fakeElem = document.createElement("textarea"), this.fakeElem.style.position = "absolute", this.fakeElem.style.left = "-9999px", this.fakeElem.style.top = (window.pageYOffset || document.documentElement.scrollTop) + "px", this.fakeElem.setAttribute("readonly", ""), this.fakeElem.value = this.text, document.body.appendChild(this.fakeElem), this.selectedText = a["default"](this.fakeElem), this.copyText()
                }, e.prototype.removeFake = function () {
                    this.fakeHandler && (document.body.removeEventListener("click"), this.fakeHandler = null), this.fakeElem && (document.body.removeChild(this.fakeElem), this.fakeElem = null)
                }, e.prototype.selectTarget = function () {
                    this.selectedText = a["default"](this.target), this.copyText()
                }, e.prototype.copyText = function () {
                    var e = void 0;
                    try {
                        e = document.execCommand(this.action)
                    } catch (t) {
                        e = !1
                    }
                    this.handleResult(e)
                }, e.prototype.handleResult = function (e) {
                    e ? this.emitter.emit("success", {
                        action: this.action,
                        text: this.selectedText,
                        trigger: this.trigger,
                        clearSelection: this.clearSelection.bind(this)
                    }) : this.emitter.emit("error", {
                        action: this.action,
                        trigger: this.trigger,
                        clearSelection: this.clearSelection.bind(this)
                    })
                }, e.prototype.clearSelection = function () {
                    this.target && this.target.blur(), window.getSelection().removeAllRanges()
                }, e.prototype.destroy = function () {
                    this.removeFake()
                }, r(e, [{
                    key: "action", set: function () {
                        var e = arguments.length <= 0 || void 0 === arguments[0] ? "copy" : arguments[0];
                        if (this._action = e, "copy" !== this._action && "cut" !== this._action) throw new Error('Invalid "action" value, use either "copy" or "cut"')
                    }, get: function () {
                        return this._action
                    }
                }, {
                    key: "target", set: function (e) {
                        if (void 0 !== e) {
                            if (!e || "object" != typeof e || 1 !== e.nodeType) throw new Error('Invalid "target" value, use a valid Element');
                            this._target = e
                        }
                    }, get: function () {
                        return this._target
                    }
                }]), e
            }();
            n["default"] = l, t.exports = n["default"]
        }, {select: 6}], 9: [function (e, t, n) {
            "use strict";

            function i(e) {
                return e && e.__esModule ? e : {"default": e}
            }

            function o(e, t) {
                if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
            }

            function r(e, t) {
                if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
                e.prototype = Object.create(t && t.prototype, {
                    constructor: {
                        value: e,
                        enumerable: !1,
                        writable: !0,
                        configurable: !0
                    }
                }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
            }

            function s(e, t) {
                var n = "data-clipboard-" + e;
                return t.hasAttribute(n) ? t.getAttribute(n) : void 0
            }

            n.__esModule = !0;
            var a = e("./clipboard-action"), l = i(a), c = e("tiny-emitter"), d = i(c), u = e("good-listener"), p = i(u), f = function (e) {
                function t(n, i) {
                    o(this, t), e.call(this), this.resolveOptions(i), this.listenClick(n)
                }

                return r(t, e), t.prototype.resolveOptions = function () {
                    var e = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0];
                    this.action = "function" == typeof e.action ? e.action : this.defaultAction, this.target = "function" == typeof e.target ? e.target : this.defaultTarget, this.text = "function" == typeof e.text ? e.text : this.defaultText
                }, t.prototype.listenClick = function (e) {
                    var t = this;
                    this.listener = p["default"](e, "click", function (e) {
                        return t.onClick(e)
                    })
                }, t.prototype.onClick = function (e) {
                    var t = e.delegateTarget || e.currentTarget;
                    this.clipboardAction && (this.clipboardAction = null), this.clipboardAction = new l["default"]({
                        action: this.action(t),
                        target: this.target(t),
                        text: this.text(t),
                        trigger: t,
                        emitter: this
                    })
                }, t.prototype.defaultAction = function (e) {
                    return s("action", e)
                }, t.prototype.defaultTarget = function (e) {
                    var t = s("target", e);
                    return t ? document.querySelector(t) : void 0
                }, t.prototype.defaultText = function (e) {
                    return s("text", e)
                }, t.prototype.destroy = function () {
                    this.listener.destroy(), this.clipboardAction && (this.clipboardAction.destroy(), this.clipboardAction = null)
                }, t
            }(d["default"]);
            n["default"] = f, t.exports = n["default"]
        }, {"./clipboard-action": 8, "good-listener": 5, "tiny-emitter": 7}]
    }, {}, [9])(9)
}), !function (e, t) {
    "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", t) : "object" == typeof module && module.exports ? module.exports = t() : e.EvEmitter = t()
}("undefined" != typeof window ? window : this, function () {
    function e() {
    }

    var t = e.prototype;
    return t.on = function (e, t) {
        if (e && t) {
            var n = this._events = this._events || {}, i = n[e] = n[e] || [];
            return -1 == i.indexOf(t) && i.push(t), this
        }
    }, t.once = function (e, t) {
        if (e && t) {
            this.on(e, t);
            var n = this._onceEvents = this._onceEvents || {}, i = n[e] = n[e] || {};
            return i[t] = !0, this
        }
    }, t.off = function (e, t) {
        var n = this._events && this._events[e];
        if (n && n.length) {
            var i = n.indexOf(t);
            return -1 != i && n.splice(i, 1), this
        }
    }, t.emitEvent = function (e, t) {
        var n = this._events && this._events[e];
        if (n && n.length) {
            var i = 0, o = n[i];
            t = t || [];
            for (var r = this._onceEvents && this._onceEvents[e]; o;) {
                var s = r && r[o];
                s && (this.off(e, o), delete r[o]), o.apply(this, t), i += s ? 0 : 1, o = n[i]
            }
            return this
        }
    }, e
}), function (e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["ev-emitter/ev-emitter"], function (n) {
        return t(e, n)
    }) : "object" == typeof module && module.exports ? module.exports = t(e, require("ev-emitter")) : e.imagesLoaded = t(e, e.EvEmitter)
}(window, function (e, t) {
    function n(e, t) {
        for (var n in t) e[n] = t[n];
        return e
    }

    function i(e) {
        var t = [];
        if (Array.isArray(e)) t = e; else if ("number" == typeof e.length) for (var n = 0; n < e.length; n++) t.push(e[n]); else t.push(e);
        return t
    }

    function o(e, t, r) {
        return this instanceof o ? ("string" == typeof e && (e = document.querySelectorAll(e)), this.elements = i(e), this.options = n({}, this.options), "function" == typeof t ? r = t : n(this.options, t), r && this.on("always", r), this.getImages(), a && (this.jqDeferred = new a.Deferred), void setTimeout(function () {
            this.check()
        }.bind(this))) : new o(e, t, r)
    }

    function r(e) {
        this.img = e
    }

    function s(e, t) {
        this.url = e, this.element = t, this.img = new Image
    }

    var a = e.jQuery, l = e.console;
    o.prototype = Object.create(t.prototype), o.prototype.options = {}, o.prototype.getImages = function () {
        this.images = [], this.elements.forEach(this.addElementImages, this)
    }, o.prototype.addElementImages = function (e) {
        "IMG" == e.nodeName && this.addImage(e), this.options.background === !0 && this.addElementBackgroundImages(e);
        var t = e.nodeType;
        if (t && c[t]) {
            for (var n = e.querySelectorAll("img"), i = 0; i < n.length; i++) {
                var o = n[i];
                this.addImage(o)
            }
            if ("string" == typeof this.options.background) {
                var r = e.querySelectorAll(this.options.background);
                for (i = 0; i < r.length; i++) {
                    var s = r[i];
                    this.addElementBackgroundImages(s)
                }
            }
        }
    };
    var c = {1: !0, 9: !0, 11: !0};
    return o.prototype.addElementBackgroundImages = function (e) {
        var t = getComputedStyle(e);
        if (t) for (var n = /url\((['"])?(.*?)\1\)/gi, i = n.exec(t.backgroundImage); null !== i;) {
            var o = i && i[2];
            o && this.addBackground(o, e), i = n.exec(t.backgroundImage)
        }
    }, o.prototype.addImage = function (e) {
        var t = new r(e);
        this.images.push(t)
    }, o.prototype.addBackground = function (e, t) {
        var n = new s(e, t);
        this.images.push(n)
    }, o.prototype.check = function () {
        function e(e, n, i) {
            setTimeout(function () {
                t.progress(e, n, i)
            })
        }

        var t = this;
        return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function (t) {
            t.once("progress", e), t.check()
        }) : void this.complete()
    }, o.prototype.progress = function (e, t, n) {
        this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded, this.emitEvent("progress", [this, e, t]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, e), this.progressedCount == this.images.length && this.complete(), this.options.debug && l && l.log("progress: " + n, e, t)
    }, o.prototype.complete = function () {
        var e = this.hasAnyBroken ? "fail" : "done";
        if (this.isComplete = !0, this.emitEvent(e, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
            var t = this.hasAnyBroken ? "reject" : "resolve";
            this.jqDeferred[t](this)
        }
    }, r.prototype = Object.create(t.prototype), r.prototype.check = function () {
        var e = this.getIsImageComplete();
        return e ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void (this.proxyImage.src = this.img.src))
    }, r.prototype.getIsImageComplete = function () {
        return this.img.complete && void 0 !== this.img.naturalWidth
    }, r.prototype.confirm = function (e, t) {
        this.isLoaded = e, this.emitEvent("progress", [this, this.img, t])
    }, r.prototype.handleEvent = function (e) {
        var t = "on" + e.type;
        this[t] && this[t](e)
    }, r.prototype.onload = function () {
        this.confirm(!0, "onload"), this.unbindEvents()
    }, r.prototype.onerror = function () {
        this.confirm(!1, "onerror"), this.unbindEvents()
    }, r.prototype.unbindEvents = function () {
        this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
    }, s.prototype = Object.create(r.prototype), s.prototype.check = function () {
        this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url;
        var e = this.getIsImageComplete();
        e && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents())
    }, s.prototype.unbindEvents = function () {
        this.img.removeEventListener("load", this), this.img.removeEventListener("error", this)
    }, s.prototype.confirm = function (e, t) {
        this.isLoaded = e, this.emitEvent("progress", [this, this.element, t])
    }, o.makeJQueryPlugin = function (t) {
        t = t || e.jQuery, t && (a = t, a.fn.imagesLoaded = function (e, t) {
            var n = new o(this, e, t);
            return n.jqDeferred.promise(a(this))
        })
    }, o.makeJQueryPlugin(), o
}), function (e, t, n, i) {
    var o = n("html"), r = n(e), s = n(t), a = n.fancybox = function () {
        a.open.apply(this, arguments)
    }, l = navigator.userAgent.match(/msie/i), c = null, d = t.createTouch !== i, u = function (e) {
        return e && e.hasOwnProperty && e instanceof n
    }, p = function (e) {
        return e && "string" === n.type(e)
    }, f = function (e) {
        return p(e) && 0 < e.indexOf("%")
    }, h = function (e, t) {
        var n = parseInt(e, 10) || 0;
        return t && f(e) && (n *= a.getViewport()[t] / 100), Math.ceil(n)
    }, g = function (e, t) {
        return h(e, t) + "px"
    };
    n.extend(a, {
        version: "2.1.5",
        defaults: {
            padding: 15,
            margin: 20,
            width: 800,
            height: 600,
            minWidth: 100,
            minHeight: 100,
            maxWidth: 9999,
            maxHeight: 9999,
            pixelRatio: 1,
            autoSize: !0,
            autoHeight: !1,
            autoWidth: !1,
            autoResize: !0,
            autoCenter: !d,
            fitToView: !0,
            aspectRatio: !1,
            topRatio: .5,
            leftRatio: .5,
            scrolling: "auto",
            wrapCSS: "",
            arrows: !0,
            closeBtn: !0,
            closeClick: !1,
            nextClick: !1,
            mouseWheel: !0,
            autoPlay: !1,
            playSpeed: 3e3,
            preload: 3,
            modal: !1,
            loop: !0,
            ajax: {dataType: "html", headers: {"X-fancyBox": !0}},
            iframe: {scrolling: "auto", preload: !0},
            swf: {wmode: "transparent", allowfullscreen: "true", allowscriptaccess: "always"},
            keys: {
                next: {13: "left", 34: "up", 39: "left", 40: "up"},
                prev: {8: "right", 33: "down", 37: "right", 38: "down"},
                close: [27],
                play: [32],
                toggle: [70]
            },
            direction: {next: "left", prev: "right"},
            scrollOutside: !0,
            index: 0,
            type: null,
            href: null,
            content: null,
            title: null,
            tpl: {
                wrap: '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
                image: '<img class="fancybox-image" src="{href}" alt="" />',
                iframe: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (l ? ' allowtransparency="true"' : "") + "></iframe>",
                error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
                closeBtn: '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',
                next: '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
                prev: '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
            },
            openEffect: "fade",
            openSpeed: 250,
            openEasing: "swing",
            openOpacity: !0,
            openMethod: "zoomIn",
            closeEffect: "fade",
            closeSpeed: 250,
            closeEasing: "swing",
            closeOpacity: !0,
            closeMethod: "zoomOut",
            nextEffect: "elastic",
            nextSpeed: 250,
            nextEasing: "swing",
            nextMethod: "changeIn",
            prevEffect: "elastic",
            prevSpeed: 250,
            prevEasing: "swing",
            prevMethod: "changeOut",
            helpers: {overlay: !0, title: !0},
            onCancel: n.noop,
            beforeLoad: n.noop,
            afterLoad: n.noop,
            beforeShow: n.noop,
            afterShow: n.noop,
            beforeChange: n.noop,
            beforeClose: n.noop,
            afterClose: n.noop
        },
        group: {},
        opts: {},
        previous: null,
        coming: null,
        current: null,
        isActive: !1,
        isOpen: !1,
        isOpened: !1,
        wrap: null,
        skin: null,
        outer: null,
        inner: null,
        player: {timer: null, isActive: !1},
        ajaxLoad: null,
        imgPreload: null,
        transitions: {},
        helpers: {},
        open: function (e, t) {
            return e && (n.isPlainObject(t) || (t = {}), !1 !== a.close(!0)) ? (n.isArray(e) || (e = u(e) ? n(e).get() : [e]), n.each(e, function (o, r) {
                var s, l, c, d, f, h = {};
                "object" === n.type(r) && (r.nodeType && (r = n(r)), u(r) ? (h = {
                    href: r.data("fancybox-href") || r.attr("href"),
                    title: n("<div/>").text(r.data("fancybox-title") || r.attr("title")).html(),
                    isDom: !0,
                    element: r
                }, n.metadata && n.extend(!0, h, r.metadata())) : h = r), s = t.href || h.href || (p(r) ? r : null), l = t.title !== i ? t.title : h.title || "", d = (c = t.content || h.content) ? "html" : t.type || h.type, !d && h.isDom && (d = r.data("fancybox-type"), d || (d = (d = r.prop("class").match(/fancybox\.(\w+)/)) ? d[1] : null)), p(s) && (d || (a.isImage(s) ? d = "image" : a.isSWF(s) ? d = "swf" : "#" === s.charAt(0) ? d = "inline" : p(r) && (d = "html", c = r)), "ajax" === d && (f = s.split(/\s+/, 2), s = f.shift(), f = f.shift())), c || ("inline" === d ? s ? c = n(p(s) ? s.replace(/.*(?=#[^\s]+$)/, "") : s) : h.isDom && (c = r) : "html" === d ? c = s : d || s || !h.isDom || (d = "inline", c = r)), n.extend(h, {
                    href: s,
                    type: d,
                    content: c,
                    title: l,
                    selector: f
                }), e[o] = h
            }), a.opts = n.extend(!0, {}, a.defaults, t), t.keys !== i && (a.opts.keys = t.keys ? n.extend({}, a.defaults.keys, t.keys) : !1), a.group = e, a._start(a.opts.index)) : void 0
        },
        cancel: function () {
            var e = a.coming;
            e && !1 === a.trigger("onCancel") || (a.hideLoading(), e && (a.ajaxLoad && a.ajaxLoad.abort(), a.ajaxLoad = null, a.imgPreload && (a.imgPreload.onload = a.imgPreload.onerror = null), e.wrap && e.wrap.stop(!0, !0).trigger("onReset").remove(), a.coming = null, a.current || a._afterZoomOut(e)))
        },
        close: function (e) {
            a.cancel(), !1 !== a.trigger("beforeClose") && (a.unbindEvents(), a.isActive && (a.isOpen && !0 !== e ? (a.isOpen = a.isOpened = !1, a.isClosing = !0, n(".fancybox-item, .fancybox-nav").remove(), a.wrap.stop(!0, !0).removeClass("fancybox-opened"), a.transitions[a.current.closeMethod]()) : (n(".fancybox-wrap").stop(!0).trigger("onReset").remove(), a._afterZoomOut())))
        },
        play: function (e) {
            var t = function () {
                clearTimeout(a.player.timer)
            }, n = function () {
                t(), a.current && a.player.isActive && (a.player.timer = setTimeout(a.next, a.current.playSpeed))
            }, i = function () {
                t(), s.unbind(".player"), a.player.isActive = !1, a.trigger("onPlayEnd")
            };
            !0 === e || !a.player.isActive && !1 !== e ? a.current && (a.current.loop || a.current.index < a.group.length - 1) && (a.player.isActive = !0, s.bind({
                "onCancel.player beforeClose.player": i,
                "onUpdate.player": n,
                "beforeLoad.player": t
            }), n(), a.trigger("onPlayStart")) : i()
        },
        next: function (e) {
            var t = a.current;
            t && (p(e) || (e = t.direction.next), a.jumpto(t.index + 1, e, "next"))
        },
        prev: function (e) {
            var t = a.current;
            t && (p(e) || (e = t.direction.prev), a.jumpto(t.index - 1, e, "prev"))
        },
        jumpto: function (e, t, n) {
            var o = a.current;
            o && (e = h(e), a.direction = t || o.direction[e >= o.index ? "next" : "prev"], a.router = n || "jumpto", o.loop && (0 > e && (e = o.group.length + e % o.group.length), e %= o.group.length), o.group[e] !== i && (a.cancel(), a._start(e)))
        },
        reposition: function (e, t) {
            var i, o = a.current, r = o ? o.wrap : null;
            r && (i = a._getPosition(t), e && "scroll" === e.type ? (delete i.position, r.stop(!0, !0).animate(i, 200)) : (r.css(i), o.pos = n.extend({}, o.dim, i)))
        },
        update: function (e) {
            var t = e && e.originalEvent && e.originalEvent.type, n = !t || "orientationchange" === t;
            n && (clearTimeout(c), c = null), a.isOpen && !c && (c = setTimeout(function () {
                var i = a.current;
                i && !a.isClosing && (a.wrap.removeClass("fancybox-tmp"), (n || "load" === t || "resize" === t && i.autoResize) && a._setDimension(), "scroll" === t && i.canShrink || a.reposition(e), a.trigger("onUpdate"), c = null)
            }, n && !d ? 0 : 300))
        },
        toggle: function (e) {
            a.isOpen && (a.current.fitToView = "boolean" === n.type(e) ? e : !a.current.fitToView, d && (a.wrap.removeAttr("style").addClass("fancybox-tmp"), a.trigger("onUpdate")), a.update())
        },
        hideLoading: function () {
            s.unbind(".loading"), n("#fancybox-loading").remove()
        },
        showLoading: function () {
            var e, t;
            a.hideLoading(), e = n('<div id="fancybox-loading"><div></div></div>').click(a.cancel).appendTo("body"), s.bind("keydown.loading", function (e) {
                27 === (e.which || e.keyCode) && (e.preventDefault(), a.cancel())
            }), a.defaults.fixed || (t = a.getViewport(), e.css({
                position: "absolute",
                top: .5 * t.h + t.y,
                left: .5 * t.w + t.x
            })), a.trigger("onLoading")
        },
        getViewport: function () {
            var t = a.current && a.current.locked || !1, n = {x: r.scrollLeft(), y: r.scrollTop()};
            return t && t.length ? (n.w = t[0].clientWidth, n.h = t[0].clientHeight) : (n.w = d && e.innerWidth ? e.innerWidth : r.width(), n.h = d && e.innerHeight ? e.innerHeight : r.height()), n
        },
        unbindEvents: function () {
            a.wrap && u(a.wrap) && a.wrap.unbind(".fb"), s.unbind(".fb"), r.unbind(".fb")
        },
        bindEvents: function () {
            var e, t = a.current;
            t && (r.bind("orientationchange.fb" + (d ? "" : " resize.fb") + (t.autoCenter && !t.locked ? " scroll.fb" : ""), a.update), (e = t.keys) && s.bind("keydown.fb", function (o) {
                var r = o.which || o.keyCode, s = o.target || o.srcElement;
                return 27 === r && a.coming ? !1 : void (o.ctrlKey || o.altKey || o.shiftKey || o.metaKey || s && (s.type || n(s).is("[contenteditable]")) || n.each(e, function (e, s) {
                    return 1 < t.group.length && s[r] !== i ? (a[e](s[r]), o.preventDefault(), !1) : -1 < n.inArray(r, s) ? (a[e](), o.preventDefault(), !1) : void 0
                }))
            }), n.fn.mousewheel && t.mouseWheel && a.wrap.bind("mousewheel.fb", function (e, i, o, r) {
                for (var s = n(e.target || null), l = !1; s.length && !(l || s.is(".fancybox-skin") || s.is(".fancybox-wrap"));) l = s[0] && !(s[0].style.overflow && "hidden" === s[0].style.overflow) && (s[0].clientWidth && s[0].scrollWidth > s[0].clientWidth || s[0].clientHeight && s[0].scrollHeight > s[0].clientHeight), s = n(s).parent();
                0 !== i && !l && 1 < a.group.length && !t.canShrink && (r > 0 || o > 0 ? a.prev(r > 0 ? "down" : "left") : (0 > r || 0 > o) && a.next(0 > r ? "up" : "right"), e.preventDefault())
            }))
        },
        trigger: function (e, t) {
            var i, o = t || a.coming || a.current;
            if (o) {
                if (n.isFunction(o[e]) && (i = o[e].apply(o, Array.prototype.slice.call(arguments, 1))), !1 === i) return !1;
                o.helpers && n.each(o.helpers, function (t, i) {
                    i && a.helpers[t] && n.isFunction(a.helpers[t][e]) && a.helpers[t][e](n.extend(!0, {}, a.helpers[t].defaults, i), o)
                })
            }
            s.trigger(e)
        },
        isImage: function (e) {
            return p(e) && e.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i)
        },
        isSWF: function (e) {
            return p(e) && e.match(/\.(swf)((\?|#).*)?$/i)
        },
        _start: function (e) {
            var t, i, o = {};
            if (e = h(e), t = a.group[e] || null, !t) return !1;
            if (o = n.extend(!0, {}, a.opts, t), t = o.margin, i = o.padding, "number" === n.type(t) && (o.margin = [t, t, t, t]), "number" === n.type(i) && (o.padding = [i, i, i, i]), o.modal && n.extend(!0, o, {
                closeBtn: !1,
                closeClick: !1,
                nextClick: !1,
                arrows: !1,
                mouseWheel: !1,
                keys: null,
                helpers: {overlay: {closeClick: !1}}
            }), o.autoSize && (o.autoWidth = o.autoHeight = !0), "auto" === o.width && (o.autoWidth = !0), "auto" === o.height && (o.autoHeight = !0), o.group = a.group, o.index = e, a.coming = o, !1 === a.trigger("beforeLoad")) a.coming = null; else {
                if (i = o.type, t = o.href, !i) return a.coming = null, a.current && a.router && "jumpto" !== a.router ? (a.current.index = e, a[a.router](a.direction)) : !1;
                if (a.isActive = !0, "image" !== i && "swf" !== i || (o.autoHeight = o.autoWidth = !1, o.scrolling = "visible"), "image" === i && (o.aspectRatio = !0), "iframe" === i && d && (o.scrolling = "scroll"), o.wrap = n(o.tpl.wrap).addClass("fancybox-" + (d ? "mobile" : "desktop") + " fancybox-type-" + i + " fancybox-tmp " + o.wrapCSS).appendTo(o.parent || "body"), n.extend(o, {
                    skin: n(".fancybox-skin", o.wrap),
                    outer: n(".fancybox-outer", o.wrap),
                    inner: n(".fancybox-inner", o.wrap)
                }), n.each(["Top", "Right", "Bottom", "Left"], function (e, t) {
                    o.skin.css("padding" + t, g(o.padding[e]))
                }), a.trigger("onReady"), "inline" === i || "html" === i) {
                    if (!o.content || !o.content.length) return a._error("content")
                } else if (!t) return a._error("href");
                "image" === i ? a._loadImage() : "ajax" === i ? a._loadAjax() : "iframe" === i ? a._loadIframe() : a._afterLoad()
            }
        },
        _error: function (e) {
            n.extend(a.coming, {
                type: "html", autoWidth: !0,
                autoHeight: !0, minWidth: 0, minHeight: 0, scrolling: "no", hasError: e, content: a.coming.tpl.error
            }), a._afterLoad()
        },
        _loadImage: function () {
            var e = a.imgPreload = new Image;
            e.onload = function () {
                this.onload = this.onerror = null, a.coming.width = this.width / a.opts.pixelRatio, a.coming.height = this.height / a.opts.pixelRatio, a._afterLoad()
            }, e.onerror = function () {
                this.onload = this.onerror = null, a._error("image")
            }, e.src = a.coming.href, !0 !== e.complete && a.showLoading()
        },
        _loadAjax: function () {
            var e = a.coming;
            a.showLoading(), a.ajaxLoad = n.ajax(n.extend({}, e.ajax, {
                url: e.href, error: function (e, t) {
                    a.coming && "abort" !== t ? a._error("ajax", e) : a.hideLoading()
                }, success: function (t, n) {
                    "success" === n && (e.content = t, a._afterLoad())
                }
            }))
        },
        _loadIframe: function () {
            var e = a.coming,
                t = n(e.tpl.iframe.replace(/\{rnd\}/g, (new Date).getTime())).attr("scrolling", d ? "auto" : e.iframe.scrolling).attr("src", e.href);
            n(e.wrap).bind("onReset", function () {
                try {
                    n(this).find("iframe").hide().attr("src", "//about:blank").end().empty()
                } catch (e) {
                }
            }), e.iframe.preload && (a.showLoading(), t.one("load", function () {
                n(this).data("ready", 1), d || n(this).bind("load.fb", a.update), n(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show(), a._afterLoad()
            })), e.content = t.appendTo(e.inner), e.iframe.preload || a._afterLoad()
        },
        _preloadImages: function () {
            var e, t, n = a.group, i = a.current, o = n.length, r = i.preload ? Math.min(i.preload, o - 1) : 0;
            for (t = 1; r >= t; t += 1) e = n[(i.index + t) % o], "image" === e.type && e.href && ((new Image).src = e.href)
        },
        _afterLoad: function () {
            var e, t, i, o, r, s = a.coming, l = a.current;
            if (a.hideLoading(), s && !1 !== a.isActive) if (!1 === a.trigger("afterLoad", s, l)) s.wrap.stop(!0).trigger("onReset").remove(), a.coming = null; else {
                switch (l && (a.trigger("beforeChange", l), l.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove()), a.unbindEvents(), e = s.content, t = s.type, i = s.scrolling, n.extend(a, {
                    wrap: s.wrap,
                    skin: s.skin,
                    outer: s.outer,
                    inner: s.inner,
                    current: s,
                    previous: l
                }), o = s.href, t) {
                    case"inline":
                    case"ajax":
                    case"html":
                        s.selector ? e = n("<div>").html(e).find(s.selector) : u(e) && (e.data("fancybox-placeholder") || e.data("fancybox-placeholder", n('<div class="fancybox-placeholder"></div>').insertAfter(e).hide()), e = e.show().detach(), s.wrap.bind("onReset", function () {
                            n(this).find(e).length && e.hide().replaceAll(e.data("fancybox-placeholder")).data("fancybox-placeholder", !1)
                        }));
                        break;
                    case"image":
                        e = s.tpl.image.replace(/\{href\}/g, o);
                        break;
                    case"swf":
                        e = '<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="' + o + '"></param>', r = "", n.each(s.swf, function (t, n) {
                            e += '<param name="' + t + '" value="' + n + '"></param>', r += " " + t + '="' + n + '"'
                        }), e += '<embed src="' + o + '" type="application/x-shockwave-flash" width="100%" height="100%"' + r + "></embed></object>"
                }
                u(e) && e.parent().is(s.inner) || s.inner.append(e), a.trigger("beforeShow"), s.inner.css("overflow", "yes" === i ? "scroll" : "no" === i ? "hidden" : i), a._setDimension(), a.reposition(), a.isOpen = !1, a.coming = null, a.bindEvents(), a.isOpened ? l.prevMethod && a.transitions[l.prevMethod]() : n(".fancybox-wrap").not(s.wrap).stop(!0).trigger("onReset").remove(), a.transitions[a.isOpened ? s.nextMethod : s.openMethod](), a._preloadImages()
            }
        },
        _setDimension: function () {
            var e, t, i, o, r, s, l, c, d, u = a.getViewport(), p = 0, m = !1, v = !1, m = a.wrap, y = a.skin, x = a.inner, b = a.current,
                v = b.width, w = b.height, _ = b.minWidth, k = b.minHeight, T = b.maxWidth, S = b.maxHeight, C = b.scrolling,
                E = b.scrollOutside ? b.scrollbarWidth : 0, $ = b.margin, A = h($[1] + $[3]), L = h($[0] + $[2]);
            if (m.add(y).add(x).width("auto").height("auto").removeClass("fancybox-tmp"), $ = h(y.outerWidth(!0) - y.width()), e = h(y.outerHeight(!0) - y.height()), t = A + $, i = L + e, o = f(v) ? (u.w - t) * h(v) / 100 : v, r = f(w) ? (u.h - i) * h(w) / 100 : w, "iframe" === b.type) {
                if (d = b.content, b.autoHeight && 1 === d.data("ready")) try {
                    d[0].contentWindow.document.location && (x.width(o).height(9999), s = d.contents().find("body"), E && s.css("overflow-x", "hidden"), r = s.outerHeight(!0))
                } catch (M) {
                }
            } else (b.autoWidth || b.autoHeight) && (x.addClass("fancybox-tmp"), b.autoWidth || x.width(o), b.autoHeight || x.height(r), b.autoWidth && (o = x.width()), b.autoHeight && (r = x.height()), x.removeClass("fancybox-tmp"));
            if (v = h(o), w = h(r), c = o / r, _ = h(f(_) ? h(_, "w") - t : _), T = h(f(T) ? h(T, "w") - t : T), k = h(f(k) ? h(k, "h") - i : k), S = h(f(S) ? h(S, "h") - i : S), s = T, l = S, b.fitToView && (T = Math.min(u.w - t, T), S = Math.min(u.h - i, S)), t = u.w - A, L = u.h - L, b.aspectRatio ? (v > T && (v = T, w = h(v / c)), w > S && (w = S, v = h(w * c)), _ > v && (v = _, w = h(v / c)), k > w && (w = k, v = h(w * c))) : (v = Math.max(_, Math.min(v, T)), b.autoHeight && "iframe" !== b.type && (x.width(v), w = x.height()), w = Math.max(k, Math.min(w, S))), b.fitToView) if (x.width(v).height(w), m.width(v + $), u = m.width(), A = m.height(), b.aspectRatio) for (; (u > t || A > L) && v > _ && w > k && !(19 < p++);) w = Math.max(k, Math.min(S, w - 10)), v = h(w * c), _ > v && (v = _, w = h(v / c)), v > T && (v = T, w = h(v / c)), x.width(v).height(w), m.width(v + $), u = m.width(), A = m.height(); else v = Math.max(_, Math.min(v, v - (u - t))), w = Math.max(k, Math.min(w, w - (A - L)));
            E && "auto" === C && r > w && t > v + $ + E && (v += E), x.width(v).height(w), m.width(v + $), u = m.width(), A = m.height(), m = (u > t || A > L) && v > _ && w > k, v = b.aspectRatio ? s > v && l > w && o > v && r > w : (s > v || l > w) && (o > v || r > w), n.extend(b, {
                dim: {
                    width: g(u),
                    height: g(A)
                },
                origWidth: o,
                origHeight: r,
                canShrink: m,
                canExpand: v,
                wPadding: $,
                hPadding: e,
                wrapSpace: A - y.outerHeight(!0),
                skinSpace: y.height() - w
            }), !d && b.autoHeight && w > k && S > w && !v && x.height("auto")
        },
        _getPosition: function (e) {
            var t = a.current, n = a.getViewport(), i = t.margin, o = a.wrap.width() + i[1] + i[3], r = a.wrap.height() + i[0] + i[2],
                i = {position: "absolute", top: i[0], left: i[3]};
            return t.autoCenter && t.fixed && !e && r <= n.h && o <= n.w ? i.position = "fixed" : t.locked || (i.top += n.y, i.left += n.x), i.top = g(Math.max(i.top, i.top + (n.h - r) * t.topRatio)), i.left = g(Math.max(i.left, i.left + (n.w - o) * t.leftRatio)), i
        },
        _afterZoomIn: function () {
            var e = a.current;
            e && (a.isOpen = a.isOpened = !0, a.wrap.css("overflow", "visible").addClass("fancybox-opened"), a.update(), (e.closeClick || e.nextClick && 1 < a.group.length) && a.inner.css("cursor", "pointer").bind("click.fb", function (t) {
                n(t.target).is("a") || n(t.target).parent().is("a") || (t.preventDefault(), a[e.closeClick ? "close" : "next"]())
            }), e.closeBtn && n(e.tpl.closeBtn).appendTo(a.skin).bind("click.fb", function (e) {
                e.preventDefault(), a.close()
            }), e.arrows && 1 < a.group.length && ((e.loop || 0 < e.index) && n(e.tpl.prev).appendTo(a.outer).bind("click.fb", a.prev), (e.loop || e.index < a.group.length - 1) && n(e.tpl.next).appendTo(a.outer).bind("click.fb", a.next)), a.trigger("afterShow"), e.loop || e.index !== e.group.length - 1 ? a.opts.autoPlay && !a.player.isActive && (a.opts.autoPlay = !1, a.play(!0)) : a.play(!1))
        },
        _afterZoomOut: function (e) {
            e = e || a.current, n(".fancybox-wrap").trigger("onReset").remove(), n.extend(a, {
                group: {},
                opts: {},
                router: !1,
                current: null,
                isActive: !1,
                isOpened: !1,
                isOpen: !1,
                isClosing: !1,
                wrap: null,
                skin: null,
                outer: null,
                inner: null
            }), a.trigger("afterClose", e)
        }
    }), a.transitions = {
        getOrigPosition: function () {
            var e = a.current, t = e.element, n = e.orig, i = {}, o = 50, r = 50, s = e.hPadding, l = e.wPadding, c = a.getViewport();
            return !n && e.isDom && t.is(":visible") && (n = t.find("img:first"), n.length || (n = t)), u(n) ? (i = n.offset(), n.is("img") && (o = n.outerWidth(), r = n.outerHeight())) : (i.top = c.y + (c.h - r) * e.topRatio, i.left = c.x + (c.w - o) * e.leftRatio), ("fixed" === a.wrap.css("position") || e.locked) && (i.top -= c.y, i.left -= c.x), i = {
                top: g(i.top - s * e.topRatio),
                left: g(i.left - l * e.leftRatio),
                width: g(o + l),
                height: g(r + s)
            }
        }, step: function (e, t) {
            var n, i, o = t.prop;
            i = a.current;
            var r = i.wrapSpace, s = i.skinSpace;
            "width" !== o && "height" !== o || (n = t.end === t.start ? 1 : (e - t.start) / (t.end - t.start), a.isClosing && (n = 1 - n), i = "width" === o ? i.wPadding : i.hPadding, i = e - i, a.skin[o](h("width" === o ? i : i - r * n)), a.inner[o](h("width" === o ? i : i - r * n - s * n)))
        }, zoomIn: function () {
            var e = a.current, t = e.pos, i = e.openEffect, o = "elastic" === i, r = n.extend({opacity: 1}, t);
            delete r.position, o ? (t = this.getOrigPosition(), e.openOpacity && (t.opacity = .1)) : "fade" === i && (t.opacity = .1), a.wrap.css(t).animate(r, {
                duration: "none" === i ? 0 : e.openSpeed,
                easing: e.openEasing,
                step: o ? this.step : null,
                complete: a._afterZoomIn
            })
        }, zoomOut: function () {
            var e = a.current, t = e.closeEffect, n = "elastic" === t, i = {opacity: .1};
            n && (i = this.getOrigPosition(), e.closeOpacity && (i.opacity = .1)), a.wrap.animate(i, {
                duration: "none" === t ? 0 : e.closeSpeed,
                easing: e.closeEasing,
                step: n ? this.step : null,
                complete: a._afterZoomOut
            })
        }, changeIn: function () {
            var e, t = a.current, n = t.nextEffect, i = t.pos, o = {opacity: 1}, r = a.direction;
            i.opacity = .1, "elastic" === n && (e = "down" === r || "up" === r ? "top" : "left", "down" === r || "right" === r ? (i[e] = g(h(i[e]) - 200), o[e] = "+=200px") : (i[e] = g(h(i[e]) + 200), o[e] = "-=200px")), "none" === n ? a._afterZoomIn() : a.wrap.css(i).animate(o, {
                duration: t.nextSpeed,
                easing: t.nextEasing,
                complete: a._afterZoomIn
            })
        }, changeOut: function () {
            var e = a.previous, t = e.prevEffect, i = {opacity: .1}, o = a.direction;
            "elastic" === t && (i["down" === o || "up" === o ? "top" : "left"] = ("up" === o || "left" === o ? "-" : "+") + "=200px"), e.wrap.animate(i, {
                duration: "none" === t ? 0 : e.prevSpeed,
                easing: e.prevEasing,
                complete: function () {
                    n(this).trigger("onReset").remove()
                }
            })
        }
    }, a.helpers.overlay = {
        defaults: {closeClick: !0, speedOut: 200, showEarly: !0, css: {}, locked: !d, fixed: !0},
        overlay: null,
        fixed: !1,
        el: n("html"),
        create: function (e) {
            var t;
            e = n.extend({}, this.defaults, e), this.overlay && this.close(), t = a.coming ? a.coming.parent : e.parent, this.overlay = n('<div class="fancybox-overlay"></div>').appendTo(t && t.lenth ? t : "body"), this.fixed = !1, e.fixed && a.defaults.fixed && (this.overlay.addClass("fancybox-overlay-fixed"), this.fixed = !0)
        },
        open: function (e) {
            var t = this;
            e = n.extend({}, this.defaults, e), this.overlay ? this.overlay.unbind(".overlay").width("auto").height("auto") : this.create(e), this.fixed || (r.bind("resize.overlay", n.proxy(this.update, this)), this.update()), e.closeClick && this.overlay.bind("click.overlay", function (e) {
                return n(e.target).hasClass("fancybox-overlay") ? (a.isActive ? a.close() : t.close(), !1) : void 0
            }), this.overlay.css(e.css).show()
        },
        close: function () {
            r.unbind("resize.overlay"), this.el.hasClass("fancybox-lock") && (n(".fancybox-margin").removeClass("fancybox-margin"), this.el.removeClass("fancybox-lock"), r.scrollTop(this.scrollV).scrollLeft(this.scrollH)), n(".fancybox-overlay").remove().hide(), n.extend(this, {
                overlay: null,
                fixed: !1
            })
        },
        update: function () {
            var e, n = "100%";
            this.overlay.width(n).height("100%"), l ? (e = Math.max(t.documentElement.offsetWidth, t.body.offsetWidth), s.width() > e && (n = s.width())) : s.width() > r.width() && (n = s.width()), this.overlay.width(n).height(s.height())
        },
        onReady: function (e, t) {
            var i = this.overlay;
            n(".fancybox-overlay").stop(!0, !0), i || this.create(e), e.locked && this.fixed && t.fixed && (t.locked = this.overlay.append(t.wrap), t.fixed = !1), !0 === e.showEarly && this.beforeShow.apply(this, arguments)
        },
        beforeShow: function (e, t) {
            t.locked && !this.el.hasClass("fancybox-lock") && (!1 !== this.fixPosition && n("*").filter(function () {
                return "fixed" === n(this).css("position") && !n(this).hasClass("fancybox-overlay") && !n(this).hasClass("fancybox-wrap")
            }).addClass("fancybox-margin"), this.el.addClass("fancybox-margin"), this.scrollV = r.scrollTop(), this.scrollH = r.scrollLeft(), this.el.addClass("fancybox-lock"), r.scrollTop(this.scrollV).scrollLeft(this.scrollH)), this.open(e)
        },
        onUpdate: function () {
            this.fixed || this.update()
        },
        afterClose: function (e) {
            this.overlay && !a.coming && this.overlay.fadeOut(e.speedOut, n.proxy(this.close, this))
        }
    }, a.helpers.title = {
        defaults: {type: "float", position: "bottom"}, beforeShow: function (e) {
            var t = a.current, i = t.title, o = e.type;
            if (n.isFunction(i) && (i = i.call(t.element, t)), p(i) && "" !== n.trim(i)) {
                switch (t = n('<div class="fancybox-title fancybox-title-' + o + '-wrap">' + i + "</div>"), o) {
                    case"inside":
                        o = a.skin;
                        break;
                    case"outside":
                        o = a.wrap;
                        break;
                    case"over":
                        o = a.inner;
                        break;
                    default:
                        o = a.skin, t.appendTo("body"), l && t.width(t.width()), t.wrapInner('<span class="child"></span>'), a.current.margin[2] += Math.abs(h(t.css("margin-bottom")))
                }
                t["top" === e.position ? "prependTo" : "appendTo"](o)
            }
        }
    }, n.fn.fancybox = function (e) {
        var t, i = n(this), o = this.selector || "", r = function (r) {
            var s, l, c = n(this).blur(), d = t;
            r.ctrlKey || r.altKey || r.shiftKey || r.metaKey || c.is(".fancybox-wrap") || (s = e.groupAttr || "data-fancybox-group", l = c.attr(s), l || (s = "rel", l = c.get(0)[s]), l && "" !== l && "nofollow" !== l && (c = o.length ? n(o) : i, c = c.filter("[" + s + '="' + l + '"]'), d = c.index(this)), e.index = d, !1 !== a.open(c, e) && r.preventDefault())
        };
        return e = e || {}, t = e.index || 0, o && !1 !== e.live ? s.undelegate(o, "click.fb-start").delegate(o + ":not('.fancybox-item, .fancybox-nav')", "click.fb-start", r) : i.unbind("click.fb-start").bind("click.fb-start", r), this.filter("[data-fancybox-start=1]").trigger("click"), this
    }, s.ready(function () {
        var t, r;
        n.scrollbarWidth === i && (n.scrollbarWidth = function () {
            var e = n('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"), t = e.children(),
                t = t.innerWidth() - t.height(99).innerWidth();
            return e.remove(), t
        }), n.support.fixedPosition === i && (n.support.fixedPosition = function () {
            var e = n('<div style="position:fixed;top:20px;"></div>').appendTo("body"), t = 20 === e[0].offsetTop || 15 === e[0].offsetTop;
            return e.remove(), t
        }()), n.extend(a.defaults, {
            scrollbarWidth: n.scrollbarWidth(),
            fixed: n.support.fixedPosition,
            parent: n("body")
        }), t = n(e).width(), o.addClass("fancybox-lock-test"), r = n(e).width(), o.removeClass("fancybox-lock-test"), n("<style type='text/css'>.fancybox-margin{margin-right:" + (r - t) + "px;}</style>").appendTo("head")
    })
}(window, document, jQuery), !function (e, t) {
    "use strict";
    e.MixItUp = function () {
        var t = this;
        t._execAction("_constructor", 0), e.extend(t, {
            selectors: {target: ".mix", filter: ".filter", sort: ".sort"},
            animation: {
                enable: !0,
                effects: "fade scale",
                duration: 600,
                easing: "ease",
                perspectiveDistance: "3000",
                perspectiveOrigin: "50% 50%",
                queue: !0,
                queueLimit: 1,
                animateChangeLayout: !1,
                animateResizeContainer: !0,
                animateResizeTargets: !1,
                staggerSequence: !1,
                reverseOut: !1
            },
            callbacks: {onMixLoad: !1, onMixStart: !1, onMixBusy: !1, onMixEnd: !1, onMixFail: !1, _user: !1},
            controls: {enable: !0, live: !1, toggleFilterButtons: !1, toggleLogic: "or", activeClass: "active"},
            layout: {display: "inline-block", containerClass: "", containerClassFail: "fail"},
            load: {filter: "all", sort: !1},
            _$body: null,
            _$container: null,
            _$targets: null,
            _$parent: null,
            _$sortButtons: null,
            _$filterButtons: null,
            _suckMode: !1,
            _mixing: !1,
            _sorting: !1,
            _clicking: !1,
            _loading: !0,
            _changingLayout: !1,
            _changingClass: !1,
            _changingDisplay: !1,
            _origOrder: [],
            _startOrder: [],
            _newOrder: [],
            _activeFilter: null,
            _toggleArray: [],
            _toggleString: "",
            _activeSort: "default:asc",
            _newSort: null,
            _startHeight: null,
            _newHeight: null,
            _incPadding: !0,
            _newDisplay: null,
            _newClass: null,
            _targetsBound: 0,
            _targetsDone: 0,
            _queue: [],
            _$show: e(),
            _$hide: e()
        }), t._execAction("_constructor", 1)
    }, e.MixItUp.prototype = {
        constructor: e.MixItUp,
        _instances: {},
        _handled: {_filter: {}, _sort: {}},
        _bound: {_filter: {}, _sort: {}},
        _actions: {},
        _filters: {},
        extend: function (t) {
            for (var n in t) e.MixItUp.prototype[n] = t[n]
        },
        addAction: function (t, n, i, o) {
            e.MixItUp.prototype._addHook("_actions", t, n, i, o)
        },
        addFilter: function (t, n, i, o) {
            e.MixItUp.prototype._addHook("_filters", t, n, i, o)
        },
        _addHook: function (t, n, i, o, r) {
            var s = e.MixItUp.prototype[t], a = {};
            r = 1 === r || "post" === r ? "post" : "pre", a[n] = {}, a[n][r] = {}, a[n][r][i] = o, e.extend(!0, s, a)
        },
        _init: function (t, n) {
            var i = this;
            if (i._execAction("_init", 0, arguments), n && e.extend(!0, i, n), i._$body = e("body"), i._domNode = t, i._$container = e(t), i._$container.addClass(i.layout.containerClass), i._id = t.id, i._platformDetect(), i._brake = i._getPrefixedCSS("transition", "none"), i._refresh(!0), i._$parent = i._$targets.parent().length ? i._$targets.parent() : i._$container, i.load.sort && (i._newSort = i._parseSort(i.load.sort), i._newSortString = i.load.sort, i._activeSort = i.load.sort, i._sort(), i._printSort()), i._activeFilter = "all" === i.load.filter ? i.selectors.target : "none" === i.load.filter ? "" : i.load.filter, i.controls.enable && i._bindHandlers(), i.controls.toggleFilterButtons) {
                i._buildToggleArray();
                for (var o = 0; o < i._toggleArray.length; o++) i._updateControls({filter: i._toggleArray[o], sort: i._activeSort}, !0)
            } else i.controls.enable && i._updateControls({filter: i._activeFilter, sort: i._activeSort});
            i._filter(), i._init = !0, i._$container.data("mixItUp", i), i._execAction("_init", 1, arguments), i._buildState(), i._$targets.css(i._brake), i._goMix(i.animation.enable)
        },
        _platformDetect: function () {
            var e = this, n = ["Webkit", "Moz", "O", "ms"], i = ["webkit", "moz"],
                o = window.navigator.appVersion.match(/Chrome\/(\d+)\./) || !1, r = "undefined" != typeof InstallTrigger, s = function (e) {
                    for (var t = 0; t < n.length; t++) if (n[t] + "Transition" in e.style) return {
                        prefix: "-" + n[t].toLowerCase() + "-",
                        vendor: n[t]
                    };
                    return "transition" in e.style ? "" : !1
                }, a = s(e._domNode);
            e._execAction("_platformDetect", 0), e._chrome = o ? parseInt(o[1], 10) : !1, e._ff = r ? parseInt(window.navigator.userAgent.match(/rv:([^)]+)\)/)[1]) : !1, e._prefix = a.prefix, e._vendor = a.vendor, e._suckMode = !window.atob || !e._prefix, e._suckMode && (e.animation.enable = !1), e._ff && e._ff <= 4 && (e.animation.enable = !1);
            for (var l = 0; l < i.length && !window.requestAnimationFrame; l++) window.requestAnimationFrame = window[i[l] + "RequestAnimationFrame"];
            "function" != typeof Object.getPrototypeOf && ("object" == typeof "test".__proto__ ? Object.getPrototypeOf = function (e) {
                return e.__proto__
            } : Object.getPrototypeOf = function (e) {
                return e.constructor.prototype
            }), e._domNode.nextElementSibling === t && Object.defineProperty(Element.prototype, "nextElementSibling", {
                get: function () {
                    for (var e = this.nextSibling; e;) {
                        if (1 === e.nodeType) return e;
                        e = e.nextSibling
                    }
                    return null
                }
            }), e._execAction("_platformDetect", 1)
        },
        _refresh: function (e, n) {
            var i = this;
            i._execAction("_refresh", 0, arguments), i._$targets = i._$container.find(i.selectors.target);
            for (var o = 0; o < i._$targets.length; o++) {
                var r = i._$targets[o];
                if (r.dataset === t || n) {
                    r.dataset = {};
                    for (var s = 0; s < r.attributes.length; s++) {
                        var a = r.attributes[s], l = a.name, c = a.value;
                        if (l.indexOf("data-") > -1) {
                            var d = i._helpers._camelCase(l.substring(5, l.length));
                            r.dataset[d] = c
                        }
                    }
                }
                r.mixParent === t && (r.mixParent = i._id)
            }
            if (i._$targets.length && e || !i._origOrder.length && i._$targets.length) {
                i._origOrder = [];
                for (var o = 0; o < i._$targets.length; o++) {
                    var r = i._$targets[o];
                    i._origOrder.push(r)
                }
            }
            i._execAction("_refresh", 1, arguments)
        },
        _bindHandlers: function () {
            var n = this, i = e.MixItUp.prototype._bound._filter, o = e.MixItUp.prototype._bound._sort;
            n._execAction("_bindHandlers", 0), n.controls.live ? n._$body.on("click.mixItUp." + n._id, n.selectors.sort, function () {
                n._processClick(e(this), "sort")
            }).on("click.mixItUp." + n._id, n.selectors.filter, function () {
                n._processClick(e(this), "filter")
            }) : (n._$sortButtons = e(n.selectors.sort), n._$filterButtons = e(n.selectors.filter), n._$sortButtons.on("click.mixItUp." + n._id, function () {
                n._processClick(e(this), "sort")
            }), n._$filterButtons.on("click.mixItUp." + n._id, function () {
                n._processClick(e(this), "filter")
            })), i[n.selectors.filter] = i[n.selectors.filter] === t ? 1 : i[n.selectors.filter] + 1, o[n.selectors.sort] = o[n.selectors.sort] === t ? 1 : o[n.selectors.sort] + 1, n._execAction("_bindHandlers", 1)
        },
        _processClick: function (n, i) {
            var o = this, r = function (n, i, r) {
                var s = e.MixItUp.prototype;
                s._handled["_" + i][o.selectors[i]] = s._handled["_" + i][o.selectors[i]] === t ? 1 : s._handled["_" + i][o.selectors[i]] + 1, s._handled["_" + i][o.selectors[i]] === s._bound["_" + i][o.selectors[i]] && (n[(r ? "remove" : "add") + "Class"](o.controls.activeClass), delete s._handled["_" + i][o.selectors[i]])
            };
            if (o._execAction("_processClick", 0, arguments), !o._mixing || o.animation.queue && o._queue.length < o.animation.queueLimit) {
                if (o._clicking = !0, "sort" === i) {
                    var s = n.attr("data-sort");
                    (!n.hasClass(o.controls.activeClass) || s.indexOf("random") > -1) && (e(o.selectors.sort).removeClass(o.controls.activeClass), r(n, i), o.sort(s))
                }
                if ("filter" === i) {
                    var a, l = n.attr("data-filter"), c = "or" === o.controls.toggleLogic ? "," : "";
                    o.controls.toggleFilterButtons ? (o._buildToggleArray(), n.hasClass(o.controls.activeClass) ? (r(n, i, !0), a = o._toggleArray.indexOf(l), o._toggleArray.splice(a, 1)) : (r(n, i), o._toggleArray.push(l)), o._toggleArray = e.grep(o._toggleArray, function (e) {
                        return e
                    }), o._toggleString = o._toggleArray.join(c), o.filter(o._toggleString)) : n.hasClass(o.controls.activeClass) || (e(o.selectors.filter).removeClass(o.controls.activeClass), r(n, i), o.filter(l))
                }
                o._execAction("_processClick", 1, arguments)
            } else "function" == typeof o.callbacks.onMixBusy && o.callbacks.onMixBusy.call(o._domNode, o._state, o), o._execAction("_processClickBusy", 1, arguments)
        },
        _buildToggleArray: function () {
            var e = this, t = e._activeFilter.replace(/\s/g, "");
            if (e._execAction("_buildToggleArray", 0, arguments), "or" === e.controls.toggleLogic) e._toggleArray = t.split(","); else {
                e._toggleArray = t.split("."), !e._toggleArray[0] && e._toggleArray.shift();
                for (var n, i = 0; n = e._toggleArray[i]; i++) e._toggleArray[i] = "." + n
            }
            e._execAction("_buildToggleArray", 1, arguments)
        },
        _updateControls: function (n, i) {
            var o = this, r = {filter: n.filter, sort: n.sort}, s = function (e, t) {
                try {
                    i && "filter" === a && "none" !== r.filter && "" !== r.filter ? e.filter(t).addClass(o.controls.activeClass) : e.removeClass(o.controls.activeClass).filter(t).addClass(o.controls.activeClass)
                } catch (n) {
                }
            }, a = "filter", l = null;
            o._execAction("_updateControls", 0, arguments), n.filter === t && (r.filter = o._activeFilter), n.sort === t && (r.sort = o._activeSort), r.filter === o.selectors.target && (r.filter = "all");
            for (var c = 0; 2 > c; c++) l = o.controls.live ? e(o.selectors[a]) : o["_$" + a + "Buttons"], l && s(l, "[data-" + a + '="' + r[a] + '"]'), a = "sort";
            o._execAction("_updateControls", 1, arguments)
        },
        _filter: function () {
            var t = this;
            t._execAction("_filter", 0);
            for (var n = 0; n < t._$targets.length; n++) {
                var i = e(t._$targets[n]);
                i.is(t._activeFilter) ? t._$show = t._$show.add(i) : t._$hide = t._$hide.add(i)
            }
            t._execAction("_filter", 1)
        },
        _sort: function () {
            var e = this, t = function (e) {
                for (var t = e.slice(), n = t.length, i = n; i--;) {
                    var o = parseInt(Math.random() * n), r = t[i];
                    t[i] = t[o], t[o] = r
                }
                return t
            };
            e._execAction("_sort", 0), e._startOrder = [];
            for (var n = 0; n < e._$targets.length; n++) {
                var i = e._$targets[n];
                e._startOrder.push(i)
            }
            switch (e._newSort[0].sortBy) {
                case"default":
                    e._newOrder = e._origOrder;
                    break;
                case"random":
                    e._newOrder = t(e._startOrder);
                    break;
                case"custom":
                    e._newOrder = e._newSort[0].order;
                    break;
                default:
                    e._newOrder = e._startOrder.concat().sort(function (t, n) {
                        return e._compare(t, n)
                    })
            }
            e._execAction("_sort", 1)
        },
        _compare: function (e, t, n) {
            n = n ? n : 0;
            var i = this, o = i._newSort[n].order, r = function (e) {
                return e.dataset[i._newSort[n].sortBy] || 0
            }, s = isNaN(1 * r(e)) ? r(e).toLowerCase() : 1 * r(e), a = isNaN(1 * r(t)) ? r(t).toLowerCase() : 1 * r(t);
            return a > s ? "asc" === o ? -1 : 1 : s > a ? "asc" === o ? 1 : -1 : s === a && i._newSort.length > n + 1 ? i._compare(e, t, n + 1) : 0
        },
        _printSort: function (e) {
            var t = this, n = e ? t._startOrder : t._newOrder, i = t._$parent[0].querySelectorAll(t.selectors.target),
                o = i.length ? i[i.length - 1].nextElementSibling : null, r = document.createDocumentFragment();
            t._execAction("_printSort", 0, arguments);
            for (var s = 0; s < i.length; s++) {
                var a = i[s], l = a.nextSibling;
                "absolute" !== a.style.position && (l && "#text" === l.nodeName && t._$parent[0].removeChild(l), t._$parent[0].removeChild(a))
            }
            for (var s = 0; s < n.length; s++) {
                var c = n[s];
                if ("default" !== t._newSort[0].sortBy || "desc" !== t._newSort[0].order || e) r.appendChild(c), r.appendChild(document.createTextNode(" ")); else {
                    var d = r.firstChild;
                    r.insertBefore(c, d), r.insertBefore(document.createTextNode(" "), c)
                }
            }
            o ? t._$parent[0].insertBefore(r, o) : t._$parent[0].appendChild(r), t._execAction("_printSort", 1, arguments)
        },
        _parseSort: function (e) {
            for (var t = this, n = "string" == typeof e ? e.split(" ") : [e], i = [], o = 0; o < n.length; o++) {
                var r = "string" == typeof e ? n[o].split(":") : ["custom", n[o]],
                    s = {sortBy: t._helpers._camelCase(r[0]), order: r[1] || "asc"};
                if (i.push(s), "default" === s.sortBy || "random" === s.sortBy) break
            }
            return t._execFilter("_parseSort", i, arguments)
        },
        _parseEffects: function () {
            var e = this, t = {opacity: "", transformIn: "", transformOut: "", filter: ""}, n = function (t, n) {
                if (e.animation.effects.indexOf(t) > -1) {
                    if (n) {
                        var i = e.animation.effects.indexOf(t + "(");
                        if (i > -1) {
                            var o = e.animation.effects.substring(i), r = /\(([^)]+)\)/.exec(o), s = r[1];
                            return {val: s}
                        }
                    }
                    return !0
                }
                return !1
            }, i = function (e, t) {
                return t ? "-" === e.charAt(0) ? e.substr(1, e.length) : "-" + e : e
            }, o = function (e, o) {
                for (var r = [["scale", ".01"], ["translateX", "20px"], ["translateY", "20px"], ["translateZ", "20px"], ["rotateX", "90deg"], ["rotateY", "90deg"], ["rotateZ", "180deg"]], s = 0; s < r.length; s++) {
                    var a = r[s][0], l = r[s][1], c = o && "scale" !== a;
                    t[e] += n(a) ? a + "(" + i(n(a, !0).val || l, c) + ") " : ""
                }
            };
            return t.opacity = n("fade") ? n("fade", !0).val || "0" : "1", o("transformIn"), e.animation.reverseOut ? o("transformOut", !0) : t.transformOut = t.transformIn, t.transition = {}, t.transition = e._getPrefixedCSS("transition", "all " + e.animation.duration + "ms " + e.animation.easing + ", opacity " + e.animation.duration + "ms linear"), e.animation.stagger = !!n("stagger"), e.animation.staggerDuration = parseInt(n("stagger") && n("stagger", !0).val ? n("stagger", !0).val : 100), e._execFilter("_parseEffects", t)
        },
        _buildState: function (e) {
            var t = this, n = {};
            return t._execAction("_buildState", 0), n = {
                activeFilter: "" === t._activeFilter ? "none" : t._activeFilter,
                activeSort: e && t._newSortString ? t._newSortString : t._activeSort,
                fail: !t._$show.length && "" !== t._activeFilter,
                $targets: t._$targets,
                $show: t._$show,
                $hide: t._$hide,
                totalTargets: t._$targets.length,
                totalShow: t._$show.length,
                totalHide: t._$hide.length,
                display: e && t._newDisplay ? t._newDisplay : t.layout.display
            }, e ? t._execFilter("_buildState", n) : (t._state = n, void t._execAction("_buildState", 1))
        },
        _goMix: function (e) {
            var t = this, n = function () {
                t._chrome && 31 === t._chrome && r(t._$parent[0]), t._setInter(), i()
            }, i = function () {
                var e = window.pageYOffset, n = window.pageXOffset;
                document.documentElement.scrollHeight, t._getInterMixData(), t._setFinal(), t._getFinalMixData(), window.pageYOffset !== e && window.scrollTo(n, e), t._prepTargets(), window.requestAnimationFrame ? requestAnimationFrame(o) : setTimeout(function () {
                    o()
                }, 20)
            }, o = function () {
                t._animateTargets(), 0 === t._targetsBound && t._cleanUp()
            }, r = function (e) {
                var t = e.parentElement, n = document.createElement("div"), i = document.createDocumentFragment();
                t.insertBefore(n, e), i.appendChild(e), t.replaceChild(e, n)
            }, s = t._buildState(!0);
            t._execAction("_goMix", 0, arguments), !t.animation.duration && (e = !1), t._mixing = !0, t._$container.removeClass(t.layout.containerClassFail), "function" == typeof t.callbacks.onMixStart && t.callbacks.onMixStart.call(t._domNode, t._state, s, t), t._$container.trigger("mixStart", [t._state, s, t]), t._getOrigMixData(), e && !t._suckMode ? window.requestAnimationFrame ? requestAnimationFrame(n) : n() : t._cleanUp(), t._execAction("_goMix", 1, arguments)
        },
        _getTargetData: function (e, t) {
            var n, i = this;
            e.dataset[t + "PosX"] = e.offsetLeft, e.dataset[t + "PosY"] = e.offsetTop, i.animation.animateResizeTargets && (n = i._suckMode ? {
                marginBottom: "",
                marginRight: ""
            } : window.getComputedStyle(e), e.dataset[t + "MarginBottom"] = parseInt(n.marginBottom), e.dataset[t + "MarginRight"] = parseInt(n.marginRight), e.dataset[t + "Width"] = e.offsetWidth, e.dataset[t + "Height"] = e.offsetHeight)
        },
        _getOrigMixData: function () {
            var e = this, t = e._suckMode ? {boxSizing: ""} : window.getComputedStyle(e._$parent[0]),
                n = t.boxSizing || t[e._vendor + "BoxSizing"];
            e._incPadding = "border-box" === n, e._execAction("_getOrigMixData", 0), !e._suckMode && (e.effects = e._parseEffects()), e._$toHide = e._$hide.filter(":visible"), e._$toShow = e._$show.filter(":hidden"), e._$pre = e._$targets.filter(":visible"), e._startHeight = e._incPadding ? e._$parent.outerHeight() : e._$parent.height();
            for (var i = 0; i < e._$pre.length; i++) {
                var o = e._$pre[i];
                e._getTargetData(o, "orig")
            }
            e._execAction("_getOrigMixData", 1)
        },
        _setInter: function () {
            var e = this;
            e._execAction("_setInter", 0), e._changingLayout && e.animation.animateChangeLayout ? (e._$toShow.css("display", e._newDisplay), e._changingClass && e._$container.removeClass(e.layout.containerClass).addClass(e._newClass)) : e._$toShow.css("display", e.layout.display), e._execAction("_setInter", 1)
        },
        _getInterMixData: function () {
            var e = this;
            e._execAction("_getInterMixData", 0);
            for (var t = 0; t < e._$toShow.length; t++) {
                var n = e._$toShow[t];
                e._getTargetData(n, "inter")
            }
            for (var t = 0; t < e._$pre.length; t++) {
                var n = e._$pre[t];
                e._getTargetData(n, "inter")
            }
            e._execAction("_getInterMixData", 1)
        },
        _setFinal: function () {
            var e = this;
            e._execAction("_setFinal", 0), e._sorting && e._printSort(), e._$toHide.removeStyle("display"), e._changingLayout && e.animation.animateChangeLayout && e._$pre.css("display", e._newDisplay), e._execAction("_setFinal", 1)
        },
        _getFinalMixData: function () {
            var e = this;
            e._execAction("_getFinalMixData", 0);
            for (var t = 0; t < e._$toShow.length; t++) {
                var n = e._$toShow[t];
                e._getTargetData(n, "final")
            }
            for (var t = 0; t < e._$pre.length; t++) {
                var n = e._$pre[t];
                e._getTargetData(n, "final")
            }
            e._newHeight = e._incPadding ? e._$parent.outerHeight() : e._$parent.height(), e._sorting && e._printSort(!0), e._$toShow.removeStyle("display"), e._$pre.css("display", e.layout.display), e._changingClass && e.animation.animateChangeLayout && e._$container.removeClass(e._newClass).addClass(e.layout.containerClass), e._execAction("_getFinalMixData", 1)
        },
        _prepTargets: function () {
            var t = this, n = {
                _in: t._getPrefixedCSS("transform", t.effects.transformIn),
                _out: t._getPrefixedCSS("transform", t.effects.transformOut)
            };
            t._execAction("_prepTargets", 0), t.animation.animateResizeContainer && t._$parent.css("height", t._startHeight + "px");
            for (var i = 0; i < t._$toShow.length; i++) {
                var o = t._$toShow[i], r = e(o);
                o.style.opacity = t.effects.opacity, o.style.display = t._changingLayout && t.animation.animateChangeLayout ? t._newDisplay : t.layout.display, r.css(n._in), t.animation.animateResizeTargets && (o.style.width = o.dataset.finalWidth + "px", o.style.height = o.dataset.finalHeight + "px", o.style.marginRight = -(o.dataset.finalWidth - o.dataset.interWidth) + 1 * o.dataset.finalMarginRight + "px", o.style.marginBottom = -(o.dataset.finalHeight - o.dataset.interHeight) + 1 * o.dataset.finalMarginBottom + "px")
            }
            for (var i = 0; i < t._$pre.length; i++) {
                var o = t._$pre[i], r = e(o),
                    s = {x: o.dataset.origPosX - o.dataset.interPosX, y: o.dataset.origPosY - o.dataset.interPosY},
                    n = t._getPrefixedCSS("transform", "translate(" + s.x + "px," + s.y + "px)");
                r.css(n), t.animation.animateResizeTargets && (o.style.width = o.dataset.origWidth + "px", o.style.height = o.dataset.origHeight + "px", o.dataset.origWidth - o.dataset.finalWidth && (o.style.marginRight = -(o.dataset.origWidth - o.dataset.interWidth) + 1 * o.dataset.origMarginRight + "px"), o.dataset.origHeight - o.dataset.finalHeight && (o.style.marginBottom = -(o.dataset.origHeight - o.dataset.interHeight) + 1 * o.dataset.origMarginBottom + "px"))
            }
            t._execAction("_prepTargets", 1)
        },
        _animateTargets: function () {
            var t = this;
            t._execAction("_animateTargets", 0), t._targetsDone = 0, t._targetsBound = 0, t._$parent.css(t._getPrefixedCSS("perspective", t.animation.perspectiveDistance + "px")).css(t._getPrefixedCSS("perspective-origin", t.animation.perspectiveOrigin)), t.animation.animateResizeContainer && t._$parent.css(t._getPrefixedCSS("transition", "height " + t.animation.duration + "ms ease")).css("height", t._newHeight + "px");
            for (var n = 0; n < t._$toShow.length; n++) {
                var i = t._$toShow[n], o = e(i),
                    r = {x: i.dataset.finalPosX - i.dataset.interPosX, y: i.dataset.finalPosY - i.dataset.interPosY}, s = t._getDelay(n),
                    a = {};
                i.style.opacity = "";
                for (var l = 0; 2 > l; l++) {
                    var c = 0 === l ? c = t._prefix : "";
                    t._ff && t._ff <= 20 && (a[c + "transition-property"] = "all", a[c + "transition-timing-function"] = t.animation.easing + "ms", a[c + "transition-duration"] = t.animation.duration + "ms"), a[c + "transition-delay"] = s + "ms", a[c + "transform"] = "translate(" + r.x + "px," + r.y + "px)"
                }
                (t.effects.transform || t.effects.opacity) && t._bindTargetDone(o), t._ff && t._ff <= 20 ? o.css(a) : o.css(t.effects.transition).css(a)
            }
            for (var n = 0; n < t._$pre.length; n++) {
                var i = t._$pre[n], o = e(i),
                    r = {x: i.dataset.finalPosX - i.dataset.interPosX, y: i.dataset.finalPosY - i.dataset.interPosY}, s = t._getDelay(n);
                (i.dataset.finalPosX !== i.dataset.origPosX || i.dataset.finalPosY !== i.dataset.origPosY) && t._bindTargetDone(o), o.css(t._getPrefixedCSS("transition", "all " + t.animation.duration + "ms " + t.animation.easing + " " + s + "ms")), o.css(t._getPrefixedCSS("transform", "translate(" + r.x + "px," + r.y + "px)")), t.animation.animateResizeTargets && (i.dataset.origWidth - i.dataset.finalWidth && 1 * i.dataset.finalWidth && (i.style.width = i.dataset.finalWidth + "px", i.style.marginRight = -(i.dataset.finalWidth - i.dataset.interWidth) + 1 * i.dataset.finalMarginRight + "px"), i.dataset.origHeight - i.dataset.finalHeight && 1 * i.dataset.finalHeight && (i.style.height = i.dataset.finalHeight + "px", i.style.marginBottom = -(i.dataset.finalHeight - i.dataset.interHeight) + 1 * i.dataset.finalMarginBottom + "px"))
            }
            t._changingClass && t._$container.removeClass(t.layout.containerClass).addClass(t._newClass);
            for (var n = 0; n < t._$toHide.length; n++) {
                for (var i = t._$toHide[n], o = e(i), s = t._getDelay(n), d = {}, l = 0; 2 > l; l++) {
                    var c = 0 === l ? c = t._prefix : "";
                    d[c + "transition-delay"] = s + "ms", d[c + "transform"] = t.effects.transformOut, d.opacity = t.effects.opacity
                }
                o.css(t.effects.transition).css(d), (t.effects.transform || t.effects.opacity) && t._bindTargetDone(o)
            }
            t._execAction("_animateTargets", 1)
        },
        _bindTargetDone: function (t) {
            var n = this, i = t[0];
            n._execAction("_bindTargetDone", 0, arguments), i.dataset.bound || (i.dataset.bound = !0, n._targetsBound++, t.on("webkitTransitionEnd.mixItUp transitionend.mixItUp", function (o) {
                (o.originalEvent.propertyName.indexOf("transform") > -1 || o.originalEvent.propertyName.indexOf("opacity") > -1) && e(o.originalEvent.target).is(n.selectors.target) && (t.off(".mixItUp"), i.dataset.bound = "", n._targetDone())
            })), n._execAction("_bindTargetDone", 1, arguments)
        },
        _targetDone: function () {
            var e = this;
            e._execAction("_targetDone", 0), e._targetsDone++, e._targetsDone === e._targetsBound && e._cleanUp(), e._execAction("_targetDone", 1)
        },
        _cleanUp: function () {
            var t = this,
                n = t.animation.animateResizeTargets ? "transform opacity width height margin-bottom margin-right" : "transform opacity",
                i = function () {
                    t._$targets.removeStyle("transition", t._prefix)
                };
            t._execAction("_cleanUp", 0), t._changingLayout ? t._$show.css("display", t._newDisplay) : t._$show.css("display", t.layout.display), t._$targets.css(t._brake), t._$targets.removeStyle(n, t._prefix).removeAttr("data-inter-pos-x data-inter-pos-y data-final-pos-x data-final-pos-y data-orig-pos-x data-orig-pos-y data-orig-height data-orig-width data-final-height data-final-width data-inter-width data-inter-height data-orig-margin-right data-orig-margin-bottom data-inter-margin-right data-inter-margin-bottom data-final-margin-right data-final-margin-bottom"), t._$hide.removeStyle("display"), t._$parent.removeStyle("height transition perspective-distance perspective perspective-origin-x perspective-origin-y perspective-origin perspectiveOrigin", t._prefix), t._sorting && (t._printSort(), t._activeSort = t._newSortString, t._sorting = !1), t._changingLayout && (t._changingDisplay && (t.layout.display = t._newDisplay, t._changingDisplay = !1), t._changingClass && (t._$parent.removeClass(t.layout.containerClass).addClass(t._newClass), t.layout.containerClass = t._newClass, t._changingClass = !1), t._changingLayout = !1), t._refresh(), t._buildState(), t._state.fail && t._$container.addClass(t.layout.containerClassFail), t._$show = e(), t._$hide = e(), window.requestAnimationFrame && requestAnimationFrame(i), t._mixing = !1, "function" == typeof t.callbacks._user && t.callbacks._user.call(t._domNode, t._state, t), "function" == typeof t.callbacks.onMixEnd && t.callbacks.onMixEnd.call(t._domNode, t._state, t), t._$container.trigger("mixEnd", [t._state, t]), t._state.fail && ("function" == typeof t.callbacks.onMixFail && t.callbacks.onMixFail.call(t._domNode, t._state, t), t._$container.trigger("mixFail", [t._state, t])), t._loading && ("function" == typeof t.callbacks.onMixLoad && t.callbacks.onMixLoad.call(t._domNode, t._state, t), t._$container.trigger("mixLoad", [t._state, t])), t._queue.length && (t._execAction("_queue", 0), t.multiMix(t._queue[0][0], t._queue[0][1], t._queue[0][2]), t._queue.splice(0, 1)), t._execAction("_cleanUp", 1), t._loading = !1
        },
        _getPrefixedCSS: function (e, t, n) {
            var i = this, o = {}, r = "", s = -1;
            for (s = 0; 2 > s; s++) r = 0 === s ? i._prefix : "", n ? o[r + e] = r + t : o[r + e] = t;
            return i._execFilter("_getPrefixedCSS", o, arguments)
        },
        _getDelay: function (e) {
            var t = this,
                n = "function" == typeof t.animation.staggerSequence ? t.animation.staggerSequence.call(t._domNode, e, t._state) : e,
                i = t.animation.stagger ? n * t.animation.staggerDuration : 0;
            return t._execFilter("_getDelay", i, arguments)
        },
        _parseMultiMixArgs: function (e) {
            for (var t = this, n = {command: null, animate: t.animation.enable, callback: null}, i = 0; i < e.length; i++) {
                var o = e[i];
                null !== o && ("object" == typeof o || "string" == typeof o ? n.command = o : "boolean" == typeof o ? n.animate = o : "function" == typeof o && (n.callback = o))
            }
            return t._execFilter("_parseMultiMixArgs", n, arguments)
        },
        _parseInsertArgs: function (t) {
            for (var n = this, i = {
                index: 0,
                $object: e(),
                multiMix: {filter: n._state.activeFilter},
                callback: null
            }, o = 0; o < t.length; o++) {
                var r = t[o];
                "number" == typeof r ? i.index = r : "object" == typeof r && r instanceof e ? i.$object = r : "object" == typeof r && n._helpers._isElement(r) ? i.$object = e(r) : "object" == typeof r && null !== r ? i.multiMix = r : "boolean" != typeof r || r ? "function" == typeof r && (i.callback = r) : i.multiMix = !1
            }
            return n._execFilter("_parseInsertArgs", i, arguments)
        },
        _execAction: function (e, t, n) {
            var i = this, o = t ? "post" : "pre";
            if (!i._actions.isEmptyObject && i._actions.hasOwnProperty(e)) for (var r in i._actions[e][o]) i._actions[e][o][r].call(i, n)
        },
        _execFilter: function (e, t, n) {
            var i = this;
            if (i._filters.isEmptyObject || !i._filters.hasOwnProperty(e)) return t;
            for (var o in i._filters[e]) return i._filters[e][o].call(i, n)
        },
        _helpers: {
            _camelCase: function (e) {
                return e.replace(/-([a-z])/g, function (e) {
                    return e[1].toUpperCase()
                })
            }, _isElement: function (e) {
                return window.HTMLElement ? e instanceof HTMLElement : null !== e && 1 === e.nodeType && "string" === e.nodeName
            }
        },
        isMixing: function () {
            var e = this;
            return e._execFilter("isMixing", e._mixing)
        },
        filter: function () {
            var e = this, t = e._parseMultiMixArgs(arguments);
            e._clicking && (e._toggleString = ""), e.multiMix({filter: t.command}, t.animate, t.callback)
        },
        sort: function () {
            var e = this, t = e._parseMultiMixArgs(arguments);
            e.multiMix({sort: t.command}, t.animate, t.callback)
        },
        changeLayout: function () {
            var e = this, t = e._parseMultiMixArgs(arguments);
            e.multiMix({changeLayout: t.command}, t.animate, t.callback)
        },
        multiMix: function () {
            var e = this, n = e._parseMultiMixArgs(arguments);
            if (e._execAction("multiMix", 0, arguments), e._mixing) e.animation.queue && e._queue.length < e.animation.queueLimit ? (e._queue.push(arguments), e.controls.enable && !e._clicking && e._updateControls(n.command), e._execAction("multiMixQueue", 1, arguments)) : ("function" == typeof e.callbacks.onMixBusy && e.callbacks.onMixBusy.call(e._domNode, e._state, e), e._$container.trigger("mixBusy", [e._state, e]), e._execAction("multiMixBusy", 1, arguments)); else {
                e.controls.enable && !e._clicking && (e.controls.toggleFilterButtons && e._buildToggleArray(), e._updateControls(n.command, e.controls.toggleFilterButtons)), e._queue.length < 2 && (e._clicking = !1), delete e.callbacks._user, n.callback && (e.callbacks._user = n.callback);
                var i = n.command.sort, o = n.command.filter, r = n.command.changeLayout;
                e._refresh(), i && (e._newSort = e._parseSort(i), e._newSortString = i, e._sorting = !0, e._sort()), o !== t && (o = "all" === o ? e.selectors.target : o, e._activeFilter = o), e._filter(), r && (e._newDisplay = "string" == typeof r ? r : r.display || e.layout.display, e._newClass = r.containerClass || "", (e._newDisplay !== e.layout.display || e._newClass !== e.layout.containerClass) && (e._changingLayout = !0, e._changingClass = e._newClass !== e.layout.containerClass, e._changingDisplay = e._newDisplay !== e.layout.display)), e._$targets.css(e._brake), e._goMix(n.animate ^ e.animation.enable ? n.animate : e.animation.enable), e._execAction("multiMix", 1, arguments)
            }
        },
        insert: function () {
            var e = this, t = e._parseInsertArgs(arguments), n = "function" == typeof t.callback ? t.callback : null,
                i = document.createDocumentFragment(), o = function () {
                    return e._refresh(), e._$targets.length ? t.index < e._$targets.length || !e._$targets.length ? e._$targets[t.index] : e._$targets[e._$targets.length - 1].nextElementSibling : e._$parent[0].children[0]
                }();
            if (e._execAction("insert", 0, arguments), t.$object) {
                for (var r = 0; r < t.$object.length; r++) {
                    var s = t.$object[r];
                    i.appendChild(s), i.appendChild(document.createTextNode(" "))
                }
                e._$parent[0].insertBefore(i, o)
            }
            e._execAction("insert", 1, arguments), "object" == typeof t.multiMix && e.multiMix(t.multiMix, n)
        },
        prepend: function () {
            var e = this, t = e._parseInsertArgs(arguments);
            e.insert(0, t.$object, t.multiMix, t.callback)
        },
        append: function () {
            var e = this, t = e._parseInsertArgs(arguments);
            e.insert(e._state.totalTargets, t.$object, t.multiMix, t.callback)
        },
        getOption: function (e) {
            var n = this, i = function (e, n) {
                for (var i = n.split("."), o = i.pop(), r = i.length, s = 1, a = i[0] || n; (e = e[a]) && r > s;) a = i[s], s++;
                return e !== t ? e[o] !== t ? e[o] : e : void 0
            };
            return e ? n._execFilter("getOption", i(n, e), arguments) : n
        },
        setOptions: function (t) {
            var n = this;
            n._execAction("setOptions", 0, arguments), "object" == typeof t && e.extend(!0, n, t), n._execAction("setOptions", 1, arguments)
        },
        getState: function () {
            var e = this;
            return e._execFilter("getState", e._state, e)
        },
        forceRefresh: function () {
            var e = this;
            e._refresh(!1, !0)
        },
        destroy: function (t) {
            var n = this, i = e.MixItUp.prototype._bound._filter, o = e.MixItUp.prototype._bound._sort;
            n._execAction("destroy", 0, arguments), n._$body.add(e(n.selectors.sort)).add(e(n.selectors.filter)).off(".mixItUp");
            for (var r = 0; r < n._$targets.length; r++) {
                var s = n._$targets[r];
                t && (s.style.display = ""), delete s.mixParent
            }
            n._execAction("destroy", 1, arguments), i[n.selectors.filter] && i[n.selectors.filter] > 1 ? i[n.selectors.filter]-- : 1 === i[n.selectors.filter] && delete i[n.selectors.filter], o[n.selectors.sort] && o[n.selectors.sort] > 1 ? o[n.selectors.sort]-- : 1 === o[n.selectors.sort] && delete o[n.selectors.sort], delete e.MixItUp.prototype._instances[n._id]
        }
    }, e.fn.mixItUp = function () {
        var n, i = arguments, o = [], r = function (t, n) {
            var i = new e.MixItUp, o = function () {
                return ("00000" + (16777216 * Math.random() << 0).toString(16)).substr(-6).toUpperCase()
            };
            i._execAction("_instantiate", 0, arguments), t.id = t.id ? t.id : "MixItUp" + o(), i._instances[t.id] || (i._instances[t.id] = i, i._init(t, n)), i._execAction("_instantiate", 1, arguments)
        };
        return n = this.each(function () {
            if (i && "string" == typeof i[0]) {
                var n = e.MixItUp.prototype._instances[this.id];
                if ("isLoaded" === i[0]) o.push(!!n); else {
                    var s = n[i[0]](i[1], i[2], i[3]);
                    s !== t && o.push(s)
                }
            } else r(this, i[0])
        }), o.length ? o.length > 1 ? o : o[0] : n
    }, e.fn.removeStyle = function (n, i) {
        return i = i ? i : "", this.each(function () {
            for (var o = this, r = n.split(" "), s = 0; s < r.length; s++) for (var a = 0; 4 > a; a++) {
                switch (a) {
                    case 0:
                        var l = r[s];
                        break;
                    case 1:
                        var l = e.MixItUp.prototype._helpers._camelCase(l);
                        break;
                    case 2:
                        var l = i + r[s];
                        break;
                    case 3:
                        var l = e.MixItUp.prototype._helpers._camelCase(i + r[s])
                }
                if (o.style[l] !== t && "unknown" != typeof o.style[l] && o.style[l].length > 0 && (o.style[l] = ""), !i && 1 === a) break
            }
            o.attributes && o.attributes.style && o.attributes.style !== t && "" === o.attributes.style.value && o.attributes.removeNamedItem("style")
        })
    }
}(jQuery), !function (e, t, n) {
    function i(e) {
        var t = {}, i = /^jQuery\d+$/;
        return n.each(e.attributes, function (e, n) {
            n.specified && !i.test(n.name) && (t[n.name] = n.value)
        }), t
    }

    function o(e, t) {
        var i = this, o = n(i);
        if (i.value == o.attr("placeholder") && o.hasClass("placeholder")) if (o.data("placeholder-password")) {
            if (o = o.hide().next().show().attr("id", o.removeAttr("id").data("placeholder-id")), e === !0) return o[0].value = t;
            o.focus()
        } else i.value = "", o.removeClass("placeholder"), i == s() && i.select()
    }

    function r() {
        var e, t = this, r = n(t), s = this.id;
        if ("" == t.value) {
            if ("password" == t.type) {
                if (!r.data("placeholder-textinput")) {
                    try {
                        e = r.clone().attr({type: "text"})
                    } catch (a) {
                        e = n("<input>").attr(n.extend(i(this), {type: "text"}))
                    }
                    e.removeAttr("name").data({
                        "placeholder-password": r,
                        "placeholder-id": s
                    }).bind("focus.placeholder", o), r.data({"placeholder-textinput": e, "placeholder-id": s}).before(e)
                }
                r = r.removeAttr("id").hide().prev().attr("id", s).show()
            }
            r.addClass("placeholder"), r[0].value = r.attr("placeholder")
        } else r.removeClass("placeholder")
    }

    function s() {
        try {
            return t.activeElement
        } catch (e) {
        }
    }

    var a, l, c = "[object OperaMini]" == Object.prototype.toString.call(e.operamini), d = "placeholder" in t.createElement("input") && !c,
        u = "placeholder" in t.createElement("textarea") && !c, p = n.fn, f = n.valHooks, h = n.propHooks;
    d && u ? (l = p.placeholder = function () {
        return this
    }, l.input = l.textarea = !0) : (l = p.placeholder = function () {
        var e = this;
        return e.filter((d ? "textarea" : ":input") + "[placeholder]").not(".placeholder").bind({
            "focus.placeholder": o,
            "blur.placeholder": r
        }).data("placeholder-enabled", !0).trigger("blur.placeholder"), e
    }, l.input = d, l.textarea = u, a = {
        get: function (e) {
            var t = n(e), i = t.data("placeholder-password");
            return i ? i[0].value : t.data("placeholder-enabled") && t.hasClass("placeholder") ? "" : e.value
        }, set: function (e, t) {
            var i = n(e), a = i.data("placeholder-password");
            return a ? a[0].value = t : i.data("placeholder-enabled") ? ("" == t ? (e.value = t, e != s() && r.call(e)) : i.hasClass("placeholder") ? o.call(e, !0, t) || (e.value = t) : e.value = t, i) : e.value = t
        }
    }, d || (f.input = a, h.value = a), u || (f.textarea = a, h.value = a), n(function () {
        n(t).delegate("form", "submit.placeholder", function () {
            var e = n(".placeholder", this).each(o);
            setTimeout(function () {
                e.each(r)
            }, 10)
        })
    }), n(e).bind("beforeunload.placeholder", function () {
        n(".placeholder").each(function () {
            this.value = ""
        })
    }))
}(this, document, jQuery), !function (e, t) {
    "function" == typeof define && define.amd ? define("jquery-bridget/jquery-bridget", ["jquery"], function (n) {
        return t(e, n)
    }) : "object" == typeof module && module.exports ? module.exports = t(e, require("jquery")) : e.jQueryBridget = t(e, e.jQuery)
}(window, function (e, t) {
    "use strict";

    function n(n, r, a) {
        function l(e, t, i) {
            var o, r = "$()." + n + '("' + t + '")';
            return e.each(function (e, l) {
                var c = a.data(l, n);
                if (!c) return void s(n + " not initialized. Cannot call methods, i.e. " + r);
                var d = c[t];
                if (!d || "_" == t.charAt(0)) return void s(r + " is not a valid method");
                var u = d.apply(c, i);
                o = void 0 === o ? u : o
            }), void 0 !== o ? o : e
        }

        function c(e, t) {
            e.each(function (e, i) {
                var o = a.data(i, n);
                o ? (o.option(t), o._init()) : (o = new r(i, t), a.data(i, n, o))
            })
        }

        a = a || t || e.jQuery, a && (r.prototype.option || (r.prototype.option = function (e) {
            a.isPlainObject(e) && (this.options = a.extend(!0, this.options, e))
        }), a.fn[n] = function (e) {
            if ("string" == typeof e) {
                var t = o.call(arguments, 1);
                return l(this, e, t)
            }
            return c(this, e), this
        }, i(a))
    }

    function i(e) {
        !e || e && e.bridget || (e.bridget = n)
    }

    var o = Array.prototype.slice, r = e.console, s = "undefined" == typeof r ? function () {
    } : function (e) {
        r.error(e)
    };
    return i(t || e.jQuery), n
}), function (e, t) {
    "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", t) : "object" == typeof module && module.exports ? module.exports = t() : e.EvEmitter = t()
}("undefined" != typeof window ? window : this, function () {
    function e() {
    }

    var t = e.prototype;
    return t.on = function (e, t) {
        if (e && t) {
            var n = this._events = this._events || {}, i = n[e] = n[e] || [];
            return -1 == i.indexOf(t) && i.push(t), this
        }
    }, t.once = function (e, t) {
        if (e && t) {
            this.on(e, t);
            var n = this._onceEvents = this._onceEvents || {}, i = n[e] = n[e] || {};
            return i[t] = !0, this
        }
    }, t.off = function (e, t) {
        var n = this._events && this._events[e];
        if (n && n.length) {
            var i = n.indexOf(t);
            return -1 != i && n.splice(i, 1), this
        }
    }, t.emitEvent = function (e, t) {
        var n = this._events && this._events[e];
        if (n && n.length) {
            var i = 0, o = n[i];
            t = t || [];
            for (var r = this._onceEvents && this._onceEvents[e]; o;) {
                var s = r && r[o];
                s && (this.off(e, o), delete r[o]), o.apply(this, t), i += s ? 0 : 1, o = n[i]
            }
            return this
        }
    }, e
}), function (e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define("get-size/get-size", [], function () {
        return t()
    }) : "object" == typeof module && module.exports ? module.exports = t() : e.getSize = t()
}(window, function () {
    "use strict";

    function e(e) {
        var t = parseFloat(e), n = -1 == e.indexOf("%") && !isNaN(t);
        return n && t
    }

    function t() {
    }

    function n() {
        for (var e = {width: 0, height: 0, innerWidth: 0, innerHeight: 0, outerWidth: 0, outerHeight: 0}, t = 0; c > t; t++) {
            var n = l[t];
            e[n] = 0
        }
        return e
    }

    function i(e) {
        var t = getComputedStyle(e);
        return t || a("Style returned " + t + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), t
    }

    function o() {
        if (!d) {
            d = !0;
            var t = document.createElement("div");
            t.style.width = "200px", t.style.padding = "1px 2px 3px 4px", t.style.borderStyle = "solid", t.style.borderWidth = "1px 2px 3px 4px", t.style.boxSizing = "border-box";
            var n = document.body || document.documentElement;
            n.appendChild(t);
            var o = i(t);
            r.isBoxSizeOuter = s = 200 == e(o.width), n.removeChild(t)
        }
    }

    function r(t) {
        if (o(), "string" == typeof t && (t = document.querySelector(t)), t && "object" == typeof t && t.nodeType) {
            var r = i(t);
            if ("none" == r.display) return n();
            var a = {};
            a.width = t.offsetWidth, a.height = t.offsetHeight;
            for (var d = a.isBorderBox = "border-box" == r.boxSizing, u = 0; c > u; u++) {
                var p = l[u], f = r[p], h = parseFloat(f);
                a[p] = isNaN(h) ? 0 : h
            }
            var g = a.paddingLeft + a.paddingRight, m = a.paddingTop + a.paddingBottom, v = a.marginLeft + a.marginRight,
                y = a.marginTop + a.marginBottom, x = a.borderLeftWidth + a.borderRightWidth, b = a.borderTopWidth + a.borderBottomWidth,
                w = d && s, _ = e(r.width);
            _ !== !1 && (a.width = _ + (w ? 0 : g + x));
            var k = e(r.height);
            return k !== !1 && (a.height = k + (w ? 0 : m + b)), a.innerWidth = a.width - (g + x), a.innerHeight = a.height - (m + b), a.outerWidth = a.width + v, a.outerHeight = a.height + y, a
        }
    }

    var s, a = "undefined" == typeof console ? t : function (e) {
            console.error(e)
        },
        l = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"],
        c = l.length, d = !1;
    return r
}), function (e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define("desandro-matches-selector/matches-selector", t) : "object" == typeof module && module.exports ? module.exports = t() : e.matchesSelector = t()
}(window, function () {
    "use strict";
    var e = function () {
        var e = Element.prototype;
        if (e.matches) return "matches";
        if (e.matchesSelector) return "matchesSelector";
        for (var t = ["webkit", "moz", "ms", "o"], n = 0; n < t.length; n++) {
            var i = t[n], o = i + "MatchesSelector";
            if (e[o]) return o
        }
    }();
    return function (t, n) {
        return t[e](n)
    }
}), function (e, t) {
    "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["desandro-matches-selector/matches-selector"], function (n) {
        return t(e, n)
    }) : "object" == typeof module && module.exports ? module.exports = t(e, require("desandro-matches-selector")) : e.fizzyUIUtils = t(e, e.matchesSelector)
}(window, function (e, t) {
    var n = {};
    n.extend = function (e, t) {
        for (var n in t) e[n] = t[n];
        return e
    }, n.modulo = function (e, t) {
        return (e % t + t) % t
    }, n.makeArray = function (e) {
        var t = [];
        if (Array.isArray(e)) t = e; else if (e && "number" == typeof e.length) for (var n = 0; n < e.length; n++) t.push(e[n]); else t.push(e);
        return t
    }, n.removeFrom = function (e, t) {
        var n = e.indexOf(t);
        -1 != n && e.splice(n, 1)
    }, n.getParent = function (e, n) {
        for (; e != document.body;) if (e = e.parentNode, t(e, n)) return e
    }, n.getQueryElement = function (e) {
        return "string" == typeof e ? document.querySelector(e) : e
    }, n.handleEvent = function (e) {
        var t = "on" + e.type;
        this[t] && this[t](e)
    }, n.filterFindElements = function (e, i) {
        e = n.makeArray(e);
        var o = [];
        return e.forEach(function (e) {
            if (e instanceof HTMLElement) {
                if (!i) return void o.push(e);
                t(e, i) && o.push(e);
                for (var n = e.querySelectorAll(i), r = 0; r < n.length; r++) o.push(n[r])
            }
        }), o
    }, n.debounceMethod = function (e, t, n) {
        var i = e.prototype[t], o = t + "Timeout";
        e.prototype[t] = function () {
            var e = this[o];
            e && clearTimeout(e);
            var t = arguments, r = this;
            this[o] = setTimeout(function () {
                i.apply(r, t), delete r[o]
            }, n || 100)
        }
    }, n.docReady = function (e) {
        var t = document.readyState;
        "complete" == t || "interactive" == t ? e() : document.addEventListener("DOMContentLoaded", e)
    }, n.toDashed = function (e) {
        return e.replace(/(.)([A-Z])/g, function (e, t, n) {
            return t + "-" + n
        }).toLowerCase()
    };
    var i = e.console;
    return n.htmlInit = function (t, o) {
        n.docReady(function () {
            var r = n.toDashed(o), s = "data-" + r, a = document.querySelectorAll("[" + s + "]"), l = document.querySelectorAll(".js-" + r),
                c = n.makeArray(a).concat(n.makeArray(l)), d = s + "-options", u = e.jQuery;
            c.forEach(function (e) {
                var n, r = e.getAttribute(s) || e.getAttribute(d);
                try {
                    n = r && JSON.parse(r)
                } catch (a) {
                    return void (i && i.error("Error parsing " + s + " on " + e.className + ": " + a))
                }
                var l = new t(e, n);
                u && u.data(e, o, l)
            })
        })
    }, n
}), function (e, t) {
    "function" == typeof define && define.amd ? define("outlayer/item", ["ev-emitter/ev-emitter", "get-size/get-size"], t) : "object" == typeof module && module.exports ? module.exports = t(require("ev-emitter"), require("get-size")) : (e.Outlayer = {}, e.Outlayer.Item = t(e.EvEmitter, e.getSize))
}(window, function (e, t) {
    "use strict";

    function n(e) {
        for (var t in e) return !1;
        return t = null, !0
    }

    function i(e, t) {
        e && (this.element = e, this.layout = t, this.position = {x: 0, y: 0}, this._create())
    }

    function o(e) {
        return e.replace(/([A-Z])/g, function (e) {
            return "-" + e.toLowerCase()
        })
    }

    var r = document.documentElement.style, s = "string" == typeof r.transition ? "transition" : "WebkitTransition",
        a = "string" == typeof r.transform ? "transform" : "WebkitTransform",
        l = {WebkitTransition: "webkitTransitionEnd", transition: "transitionend"}[s], c = {
            transform: a,
            transition: s,
            transitionDuration: s + "Duration",
            transitionProperty: s + "Property",
            transitionDelay: s + "Delay"
        }, d = i.prototype = Object.create(e.prototype);
    d.constructor = i, d._create = function () {
        this._transn = {ingProperties: {}, clean: {}, onEnd: {}}, this.css({position: "absolute"})
    }, d.handleEvent = function (e) {
        var t = "on" + e.type;
        this[t] && this[t](e)
    }, d.getSize = function () {
        this.size = t(this.element)
    }, d.css = function (e) {
        var t = this.element.style;
        for (var n in e) {
            var i = c[n] || n;
            t[i] = e[n]
        }
    }, d.getPosition = function () {
        var e = getComputedStyle(this.element), t = this.layout._getOption("originLeft"), n = this.layout._getOption("originTop"),
            i = e[t ? "left" : "right"], o = e[n ? "top" : "bottom"], r = this.layout.size,
            s = -1 != i.indexOf("%") ? parseFloat(i) / 100 * r.width : parseInt(i, 10),
            a = -1 != o.indexOf("%") ? parseFloat(o) / 100 * r.height : parseInt(o, 10);
        s = isNaN(s) ? 0 : s, a = isNaN(a) ? 0 : a, s -= t ? r.paddingLeft : r.paddingRight, a -= n ? r.paddingTop : r.paddingBottom, this.position.x = s, this.position.y = a
    }, d.layoutPosition = function () {
        var e = this.layout.size, t = {}, n = this.layout._getOption("originLeft"), i = this.layout._getOption("originTop"),
            o = n ? "paddingLeft" : "paddingRight", r = n ? "left" : "right", s = n ? "right" : "left", a = this.position.x + e[o];
        t[r] = this.getXValue(a), t[s] = "";
        var l = i ? "paddingTop" : "paddingBottom", c = i ? "top" : "bottom", d = i ? "bottom" : "top", u = this.position.y + e[l];
        t[c] = this.getYValue(u), t[d] = "", this.css(t), this.emitEvent("layout", [this])
    }, d.getXValue = function (e) {
        var t = this.layout._getOption("horizontal");
        return this.layout.options.percentPosition && !t ? e / this.layout.size.width * 100 + "%" : e + "px"
    }, d.getYValue = function (e) {
        var t = this.layout._getOption("horizontal");
        return this.layout.options.percentPosition && t ? e / this.layout.size.height * 100 + "%" : e + "px"
    }, d._transitionTo = function (e, t) {
        this.getPosition();
        var n = this.position.x, i = this.position.y, o = parseInt(e, 10), r = parseInt(t, 10),
            s = o === this.position.x && r === this.position.y;
        if (this.setPosition(e, t), s && !this.isTransitioning) return void this.layoutPosition();
        var a = e - n, l = t - i, c = {};
        c.transform = this.getTranslate(a, l), this.transition({to: c, onTransitionEnd: {transform: this.layoutPosition}, isCleaning: !0})
    }, d.getTranslate = function (e, t) {
        var n = this.layout._getOption("originLeft"), i = this.layout._getOption("originTop");
        return e = n ? e : -e, t = i ? t : -t, "translate3d(" + e + "px, " + t + "px, 0)"
    }, d.goTo = function (e, t) {
        this.setPosition(e, t), this.layoutPosition()
    }, d.moveTo = d._transitionTo, d.setPosition = function (e, t) {
        this.position.x = parseInt(e, 10), this.position.y = parseInt(t, 10)
    }, d._nonTransition = function (e) {
        this.css(e.to), e.isCleaning && this._removeStyles(e.to);
        for (var t in e.onTransitionEnd) e.onTransitionEnd[t].call(this)
    }, d.transition = function (e) {
        if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(e);
        var t = this._transn;
        for (var n in e.onTransitionEnd) t.onEnd[n] = e.onTransitionEnd[n];
        for (n in e.to) t.ingProperties[n] = !0, e.isCleaning && (t.clean[n] = !0);
        if (e.from) {
            this.css(e.from);
            var i = this.element.offsetHeight;
            i = null
        }
        this.enableTransition(e.to), this.css(e.to), this.isTransitioning = !0
    };
    var u = "opacity," + o(a);
    d.enableTransition = function () {
        if (!this.isTransitioning) {
            var e = this.layout.options.transitionDuration;
            e = "number" == typeof e ? e + "ms" : e, this.css({
                transitionProperty: u,
                transitionDuration: e,
                transitionDelay: this.staggerDelay || 0
            }), this.element.addEventListener(l, this, !1)
        }
    }, d.onwebkitTransitionEnd = function (e) {
        this.ontransitionend(e)
    }, d.onotransitionend = function (e) {
        this.ontransitionend(e)
    };
    var p = {"-webkit-transform": "transform"};
    d.ontransitionend = function (e) {
        if (e.target === this.element) {
            var t = this._transn, i = p[e.propertyName] || e.propertyName;
            if (delete t.ingProperties[i], n(t.ingProperties) && this.disableTransition(), i in t.clean && (this.element.style[e.propertyName] = "", delete t.clean[i]), i in t.onEnd) {
                var o = t.onEnd[i];
                o.call(this), delete t.onEnd[i]
            }
            this.emitEvent("transitionEnd", [this])
        }
    }, d.disableTransition = function () {
        this.removeTransitionStyles(), this.element.removeEventListener(l, this, !1), this.isTransitioning = !1
    }, d._removeStyles = function (e) {
        var t = {};
        for (var n in e) t[n] = "";
        this.css(t)
    };
    var f = {transitionProperty: "", transitionDuration: "", transitionDelay: ""};
    return d.removeTransitionStyles = function () {
        this.css(f)
    }, d.stagger = function (e) {
        e = isNaN(e) ? 0 : e, this.staggerDelay = e + "ms"
    }, d.removeElem = function () {
        this.element.parentNode.removeChild(this.element), this.css({display: ""}), this.emitEvent("remove", [this])
    }, d.remove = function () {
        return s && parseFloat(this.layout.options.transitionDuration) ? (this.once("transitionEnd", function () {
            this.removeElem()
        }), void this.hide()) : void this.removeElem()
    }, d.reveal = function () {
        delete this.isHidden, this.css({display: ""});
        var e = this.layout.options, t = {}, n = this.getHideRevealTransitionEndProperty("visibleStyle");
        t[n] = this.onRevealTransitionEnd, this.transition({from: e.hiddenStyle, to: e.visibleStyle, isCleaning: !0, onTransitionEnd: t})
    }, d.onRevealTransitionEnd = function () {
        this.isHidden || this.emitEvent("reveal")
    }, d.getHideRevealTransitionEndProperty = function (e) {
        var t = this.layout.options[e];
        if (t.opacity) return "opacity";
        for (var n in t) return n
    }, d.hide = function () {
        this.isHidden = !0, this.css({display: ""});
        var e = this.layout.options, t = {}, n = this.getHideRevealTransitionEndProperty("hiddenStyle");
        t[n] = this.onHideTransitionEnd, this.transition({from: e.visibleStyle, to: e.hiddenStyle, isCleaning: !0, onTransitionEnd: t})
    }, d.onHideTransitionEnd = function () {
        this.isHidden && (this.css({display: "none"}), this.emitEvent("hide"))
    }, d.destroy = function () {
        this.css({position: "", left: "", right: "", top: "", bottom: "", transition: "", transform: ""})
    }, i
}), function (e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define("outlayer/outlayer", ["ev-emitter/ev-emitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function (n, i, o, r) {
        return t(e, n, i, o, r)
    }) : "object" == typeof module && module.exports ? module.exports = t(e, require("ev-emitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : e.Outlayer = t(e, e.EvEmitter, e.getSize, e.fizzyUIUtils, e.Outlayer.Item)
}(window, function (e, t, n, i, o) {
    "use strict";

    function r(e, t) {
        var n = i.getQueryElement(e);
        if (!n) return void (l && l.error("Bad element for " + this.constructor.namespace + ": " + (n || e)));
        this.element = n, c && (this.$element = c(this.element)), this.options = i.extend({}, this.constructor.defaults), this.option(t);
        var o = ++u;
        this.element.outlayerGUID = o, p[o] = this, this._create();
        var r = this._getOption("initLayout");
        r && this.layout()
    }

    function s(e) {
        function t() {
            e.apply(this, arguments)
        }

        return t.prototype = Object.create(e.prototype), t.prototype.constructor = t, t
    }

    function a(e) {
        if ("number" == typeof e) return e;
        var t = e.match(/(^\d*\.?\d*)(\w*)/), n = t && t[1], i = t && t[2];
        if (!n.length) return 0;
        n = parseFloat(n);
        var o = h[i] || 1;
        return n * o
    }

    var l = e.console, c = e.jQuery, d = function () {
    }, u = 0, p = {};
    r.namespace = "outlayer", r.Item = o, r.defaults = {
        containerStyle: {position: "relative"},
        initLayout: !0,
        originLeft: !0,
        originTop: !0,
        resize: !0,
        resizeContainer: !0,
        transitionDuration: "0.4s",
        hiddenStyle: {opacity: 0, transform: "scale(0.001)"},
        visibleStyle: {opacity: 1, transform: "scale(1)"}
    };
    var f = r.prototype;
    i.extend(f, t.prototype), f.option = function (e) {
        i.extend(this.options, e)
    }, f._getOption = function (e) {
        var t = this.constructor.compatOptions[e];
        return t && void 0 !== this.options[t] ? this.options[t] : this.options[e]
    }, r.compatOptions = {
        initLayout: "isInitLayout",
        horizontal: "isHorizontal",
        layoutInstant: "isLayoutInstant",
        originLeft: "isOriginLeft",
        originTop: "isOriginTop",
        resize: "isResizeBound",
        resizeContainer: "isResizingContainer"
    }, f._create = function () {
        this.reloadItems(), this.stamps = [], this.stamp(this.options.stamp), i.extend(this.element.style, this.options.containerStyle);
        var e = this._getOption("resize");
        e && this.bindResize()
    }, f.reloadItems = function () {
        this.items = this._itemize(this.element.children)
    }, f._itemize = function (e) {
        for (var t = this._filterFindItemElements(e), n = this.constructor.Item, i = [], o = 0; o < t.length; o++) {
            var r = t[o], s = new n(r, this);
            i.push(s)
        }
        return i
    }, f._filterFindItemElements = function (e) {
        return i.filterFindElements(e, this.options.itemSelector)
    }, f.getItemElements = function () {
        return this.items.map(function (e) {
            return e.element
        })
    }, f.layout = function () {
        this._resetLayout(), this._manageStamps();
        var e = this._getOption("layoutInstant"), t = void 0 !== e ? e : !this._isLayoutInited;
        this.layoutItems(this.items, t), this._isLayoutInited = !0
    }, f._init = f.layout, f._resetLayout = function () {
        this.getSize()
    }, f.getSize = function () {
        this.size = n(this.element)
    }, f._getMeasurement = function (e, t) {
        var i, o = this.options[e];
        o ? ("string" == typeof o ? i = this.element.querySelector(o) : o instanceof HTMLElement && (i = o), this[e] = i ? n(i)[t] : o) : this[e] = 0
    }, f.layoutItems = function (e, t) {
        e = this._getItemsForLayout(e), this._layoutItems(e, t), this._postLayout()
    }, f._getItemsForLayout = function (e) {
        return e.filter(function (e) {
            return !e.isIgnored
        })
    }, f._layoutItems = function (e, t) {
        if (this._emitCompleteOnItems("layout", e), e && e.length) {
            var n = [];
            e.forEach(function (e) {
                var i = this._getItemLayoutPosition(e);
                i.item = e, i.isInstant = t || e.isLayoutInstant, n.push(i)
            }, this), this._processLayoutQueue(n)
        }
    }, f._getItemLayoutPosition = function () {
        return {x: 0, y: 0}
    }, f._processLayoutQueue = function (e) {
        this.updateStagger(), e.forEach(function (e, t) {
            this._positionItem(e.item, e.x, e.y, e.isInstant, t)
        }, this)
    }, f.updateStagger = function () {
        var e = this.options.stagger;
        return null === e || void 0 === e ? void (this.stagger = 0) : (this.stagger = a(e), this.stagger)
    }, f._positionItem = function (e, t, n, i, o) {
        i ? e.goTo(t, n) : (e.stagger(o * this.stagger), e.moveTo(t, n))
    }, f._postLayout = function () {
        this.resizeContainer()
    }, f.resizeContainer = function () {
        var e = this._getOption("resizeContainer");
        if (e) {
            var t = this._getContainerSize();
            t && (this._setContainerMeasure(t.width, !0), this._setContainerMeasure(t.height, !1))
        }
    }, f._getContainerSize = d, f._setContainerMeasure = function (e, t) {
        if (void 0 !== e) {
            var n = this.size;
            n.isBorderBox && (e += t ? n.paddingLeft + n.paddingRight + n.borderLeftWidth + n.borderRightWidth : n.paddingBottom + n.paddingTop + n.borderTopWidth + n.borderBottomWidth), e = Math.max(e, 0), this.element.style[t ? "width" : "height"] = e + "px"
        }
    }, f._emitCompleteOnItems = function (e, t) {
        function n() {
            o.dispatchEvent(e + "Complete", null, [t])
        }

        function i() {
            s++, s == r && n()
        }

        var o = this, r = t.length;
        if (!t || !r) return void n();
        var s = 0;
        t.forEach(function (t) {
            t.once(e, i)
        })
    }, f.dispatchEvent = function (e, t, n) {
        var i = t ? [t].concat(n) : n;
        if (this.emitEvent(e, i), c) if (this.$element = this.$element || c(this.element), t) {
            var o = c.Event(t);
            o.type = e, this.$element.trigger(o, n)
        } else this.$element.trigger(e, n)
    }, f.ignore = function (e) {
        var t = this.getItem(e);
        t && (t.isIgnored = !0)
    }, f.unignore = function (e) {
        var t = this.getItem(e);
        t && delete t.isIgnored
    }, f.stamp = function (e) {
        e = this._find(e), e && (this.stamps = this.stamps.concat(e), e.forEach(this.ignore, this))
    }, f.unstamp = function (e) {
        e = this._find(e), e && e.forEach(function (e) {
            i.removeFrom(this.stamps, e), this.unignore(e)
        }, this)
    }, f._find = function (e) {
        return e ? ("string" == typeof e && (e = this.element.querySelectorAll(e)), e = i.makeArray(e)) : void 0
    }, f._manageStamps = function () {
        this.stamps && this.stamps.length && (this._getBoundingRect(), this.stamps.forEach(this._manageStamp, this))
    }, f._getBoundingRect = function () {
        var e = this.element.getBoundingClientRect(), t = this.size;
        this._boundingRect = {
            left: e.left + t.paddingLeft + t.borderLeftWidth,
            top: e.top + t.paddingTop + t.borderTopWidth,
            right: e.right - (t.paddingRight + t.borderRightWidth),
            bottom: e.bottom - (t.paddingBottom + t.borderBottomWidth)
        }
    }, f._manageStamp = d, f._getElementOffset = function (e) {
        var t = e.getBoundingClientRect(), i = this._boundingRect, o = n(e), r = {
            left: t.left - i.left - o.marginLeft,
            top: t.top - i.top - o.marginTop,
            right: i.right - t.right - o.marginRight,
            bottom: i.bottom - t.bottom - o.marginBottom
        };
        return r
    }, f.handleEvent = i.handleEvent, f.bindResize = function () {
        e.addEventListener("resize", this), this.isResizeBound = !0
    }, f.unbindResize = function () {
        e.removeEventListener("resize", this), this.isResizeBound = !1
    }, f.onresize = function () {
        this.resize()
    }, i.debounceMethod(r, "onresize", 100), f.resize = function () {
        this.isResizeBound && this.needsResizeLayout() && this.layout()
    }, f.needsResizeLayout = function () {
        var e = n(this.element), t = this.size && e;
        return t && e.innerWidth !== this.size.innerWidth
    }, f.addItems = function (e) {
        var t = this._itemize(e);
        return t.length && (this.items = this.items.concat(t)), t
    }, f.appended = function (e) {
        var t = this.addItems(e);
        t.length && (this.layoutItems(t, !0), this.reveal(t))
    }, f.prepended = function (e) {
        var t = this._itemize(e);
        if (t.length) {
            var n = this.items.slice(0);
            this.items = t.concat(n), this._resetLayout(), this._manageStamps(), this.layoutItems(t, !0), this.reveal(t), this.layoutItems(n)
        }
    }, f.reveal = function (e) {
        if (this._emitCompleteOnItems("reveal", e), e && e.length) {
            var t = this.updateStagger();
            e.forEach(function (e, n) {
                e.stagger(n * t), e.reveal()
            })
        }
    }, f.hide = function (e) {
        if (this._emitCompleteOnItems("hide", e), e && e.length) {
            var t = this.updateStagger();
            e.forEach(function (e, n) {
                e.stagger(n * t), e.hide()
            })
        }
    }, f.revealItemElements = function (e) {
        var t = this.getItems(e);
        this.reveal(t)
    }, f.hideItemElements = function (e) {
        var t = this.getItems(e);
        this.hide(t)
    }, f.getItem = function (e) {
        for (var t = 0; t < this.items.length; t++) {
            var n = this.items[t];
            if (n.element == e) return n
        }
    }, f.getItems = function (e) {
        e = i.makeArray(e);
        var t = [];
        return e.forEach(function (e) {
            var n = this.getItem(e);
            n && t.push(n)
        }, this), t
    }, f.remove = function (e) {
        var t = this.getItems(e);
        this._emitCompleteOnItems("remove", t), t && t.length && t.forEach(function (e) {
            e.remove(),
                i.removeFrom(this.items, e)
        }, this)
    }, f.destroy = function () {
        var e = this.element.style;
        e.height = "", e.position = "", e.width = "", this.items.forEach(function (e) {
            e.destroy()
        }), this.unbindResize();
        var t = this.element.outlayerGUID;
        delete p[t], delete this.element.outlayerGUID, c && c.removeData(this.element, this.constructor.namespace)
    }, r.data = function (e) {
        e = i.getQueryElement(e);
        var t = e && e.outlayerGUID;
        return t && p[t]
    }, r.create = function (e, t) {
        var n = s(r);
        return n.defaults = i.extend({}, r.defaults), i.extend(n.defaults, t), n.compatOptions = i.extend({}, r.compatOptions), n.namespace = e, n.data = r.data, n.Item = s(o), i.htmlInit(n, e), c && c.bridget && c.bridget(e, n), n
    };
    var h = {ms: 1, s: 1e3};
    return r.Item = o, r
}), function (e, t) {
    "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size"], t) : "object" == typeof module && module.exports ? module.exports = t(require("outlayer"), require("get-size")) : e.Masonry = t(e.Outlayer, e.getSize)
}(window, function (e, t) {
    var n = e.create("masonry");
    return n.compatOptions.fitWidth = "isFitWidth", n.prototype._resetLayout = function () {
        this.getSize(), this._getMeasurement("columnWidth", "outerWidth"), this._getMeasurement("gutter", "outerWidth"), this.measureColumns(), this.colYs = [];
        for (var e = 0; e < this.cols; e++) this.colYs.push(0);
        this.maxY = 0
    }, n.prototype.measureColumns = function () {
        if (this.getContainerWidth(), !this.columnWidth) {
            var e = this.items[0], n = e && e.element;
            this.columnWidth = n && t(n).outerWidth || this.containerWidth
        }
        var i = this.columnWidth += this.gutter, o = this.containerWidth + this.gutter, r = o / i, s = i - o % i,
            a = s && 1 > s ? "round" : "floor";
        r = Math[a](r), this.cols = Math.max(r, 1)
    }, n.prototype.getContainerWidth = function () {
        var e = this._getOption("fitWidth"), n = e ? this.element.parentNode : this.element, i = t(n);
        this.containerWidth = i && i.innerWidth
    }, n.prototype._getItemLayoutPosition = function (e) {
        e.getSize();
        var t = e.size.outerWidth % this.columnWidth, n = t && 1 > t ? "round" : "ceil", i = Math[n](e.size.outerWidth / this.columnWidth);
        i = Math.min(i, this.cols);
        for (var o = this._getColGroup(i), r = Math.min.apply(Math, o), s = o.indexOf(r), a = {
            x: this.columnWidth * s,
            y: r
        }, l = r + e.size.outerHeight, c = this.cols + 1 - o.length, d = 0; c > d; d++) this.colYs[s + d] = l;
        return a
    }, n.prototype._getColGroup = function (e) {
        if (2 > e) return this.colYs;
        for (var t = [], n = this.cols + 1 - e, i = 0; n > i; i++) {
            var o = this.colYs.slice(i, i + e);
            t[i] = Math.max.apply(Math, o)
        }
        return t
    }, n.prototype._manageStamp = function (e) {
        var n = t(e), i = this._getElementOffset(e), o = this._getOption("originLeft"), r = o ? i.left : i.right, s = r + n.outerWidth,
            a = Math.floor(r / this.columnWidth);
        a = Math.max(0, a);
        var l = Math.floor(s / this.columnWidth);
        l -= s % this.columnWidth ? 0 : 1, l = Math.min(this.cols - 1, l);
        for (var c = this._getOption("originTop"), d = (c ? i.top : i.bottom) + n.outerHeight, u = a; l >= u; u++) this.colYs[u] = Math.max(d, this.colYs[u])
    }, n.prototype._getContainerSize = function () {
        this.maxY = Math.max.apply(Math, this.colYs);
        var e = {height: this.maxY};
        return this._getOption("fitWidth") && (e.width = this._getContainerFitWidth()), e
    }, n.prototype._getContainerFitWidth = function () {
        for (var e = 0, t = this.cols; --t && 0 === this.colYs[t];) e++;
        return (this.cols - e) * this.columnWidth - this.gutter
    }, n.prototype.needsResizeLayout = function () {
        var e = this.containerWidth;
        return this.getContainerWidth(), e != this.containerWidth
    }, n
}), window.Modernizr = function (e, t, n) {
    function i(e) {
        y.cssText = e
    }

    function o(e, t) {
        return typeof e === t
    }

    function r(e, t) {
        return !!~("" + e).indexOf(t)
    }

    function s(e, t) {
        for (var i in e) {
            var o = e[i];
            if (!r(o, "-") && y[o] !== n) return "pfx" == t ? o : !0
        }
        return !1
    }

    function a(e, t, i) {
        for (var r in e) {
            var s = t[e[r]];
            if (s !== n) return i === !1 ? e[r] : o(s, "function") ? s.bind(i || t) : s
        }
        return !1
    }

    function l(e, t, n) {
        var i = e.charAt(0).toUpperCase() + e.slice(1), r = (e + " " + b.join(i + " ") + i).split(" ");
        return o(t, "string") || o(t, "undefined") ? s(r, t) : (r = (e + " " + w.join(i + " ") + i).split(" "), a(r, t, n))
    }

    var c, d, u, p = "2.8.3", f = {}, h = !0, g = t.documentElement, m = "modernizr", v = t.createElement(m), y = v.style,
        x = ({}.toString, "Webkit Moz O ms"), b = x.split(" "), w = x.toLowerCase().split(" "), _ = {}, k = [], T = k.slice,
        S = {}.hasOwnProperty;
    u = o(S, "undefined") || o(S.call, "undefined") ? function (e, t) {
        return t in e && o(e.constructor.prototype[t], "undefined")
    } : function (e, t) {
        return S.call(e, t)
    }, Function.prototype.bind || (Function.prototype.bind = function (e) {
        var t = this;
        if ("function" != typeof t) throw new TypeError;
        var n = T.call(arguments, 1), i = function () {
            if (this instanceof i) {
                var o = function () {
                };
                o.prototype = t.prototype;
                var r = new o, s = t.apply(r, n.concat(T.call(arguments)));
                return Object(s) === s ? s : r
            }
            return t.apply(e, n.concat(T.call(arguments)))
        };
        return i
    }), _.cssanimations = function () {
        return l("animationName")
    };
    for (var C in _) u(_, C) && (d = C.toLowerCase(), f[d] = _[C](), k.push((f[d] ? "" : "no-") + d));
    return f.addTest = function (e, t) {
        if ("object" == typeof e) for (var i in e) u(e, i) && f.addTest(i, e[i]); else {
            if (e = e.toLowerCase(), f[e] !== n) return f;
            t = "function" == typeof t ? t() : t, "undefined" != typeof h && h && (g.className += " " + (t ? "" : "no-") + e), f[e] = t
        }
        return f
    }, i(""), v = c = null, function (e, t) {
        function n(e, t) {
            var n = e.createElement("p"), i = e.getElementsByTagName("head")[0] || e.documentElement;
            return n.innerHTML = "x<style>" + t + "</style>", i.insertBefore(n.lastChild, i.firstChild)
        }

        function i() {
            var e = y.elements;
            return "string" == typeof e ? e.split(" ") : e
        }

        function o(e) {
            var t = v[e[g]];
            return t || (t = {}, m++, e[g] = m, v[m] = t), t
        }

        function r(e, n, i) {
            if (n || (n = t), d) return n.createElement(e);
            i || (i = o(n));
            var r;
            return r = i.cache[e] ? i.cache[e].cloneNode() : h.test(e) ? (i.cache[e] = i.createElem(e)).cloneNode() : i.createElem(e), !r.canHaveChildren || f.test(e) || r.tagUrn ? r : i.frag.appendChild(r)
        }

        function s(e, n) {
            if (e || (e = t), d) return e.createDocumentFragment();
            n = n || o(e);
            for (var r = n.frag.cloneNode(), s = 0, a = i(), l = a.length; l > s; s++) r.createElement(a[s]);
            return r
        }

        function a(e, t) {
            t.cache || (t.cache = {}, t.createElem = e.createElement, t.createFrag = e.createDocumentFragment, t.frag = t.createFrag()), e.createElement = function (n) {
                return y.shivMethods ? r(n, e, t) : t.createElem(n)
            }, e.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + i().join().replace(/[\w\-]+/g, function (e) {
                return t.createElem(e), t.frag.createElement(e), 'c("' + e + '")'
            }) + ");return n}")(y, t.frag)
        }

        function l(e) {
            e || (e = t);
            var i = o(e);
            return y.shivCSS && !c && !i.hasCSS && (i.hasCSS = !!n(e, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), d || a(e, i), e
        }

        var c, d, u = "3.7.0", p = e.html5 || {}, f = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
            h = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
            g = "_html5shiv", m = 0, v = {};
        !function () {
            try {
                var e = t.createElement("a");
                e.innerHTML = "<xyz></xyz>", c = "hidden" in e, d = 1 == e.childNodes.length || function () {
                    t.createElement("a");
                    var e = t.createDocumentFragment();
                    return "undefined" == typeof e.cloneNode || "undefined" == typeof e.createDocumentFragment || "undefined" == typeof e.createElement
                }()
            } catch (n) {
                c = !0, d = !0
            }
        }();
        var y = {
            elements: p.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
            version: u,
            shivCSS: p.shivCSS !== !1,
            supportsUnknownElements: d,
            shivMethods: p.shivMethods !== !1,
            type: "default",
            shivDocument: l,
            createElement: r,
            createDocumentFragment: s
        };
        e.html5 = y, l(t)
    }(this, t), f._version = p, f._domPrefixes = w, f._cssomPrefixes = b, f.testProp = function (e) {
        return s([e])
    }, f.testAllProps = l, f.prefixed = function (e, t, n) {
        return t ? l(e, t, n) : l(e, "pfx")
    }, g.className = g.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (h ? " js " + k.join(" ") : ""), f
}(this, this.document), function (e, t, n) {
    function i(e) {
        return "[object Function]" == m.call(e)
    }

    function o(e) {
        return "string" == typeof e
    }

    function r() {
    }

    function s(e) {
        return !e || "loaded" == e || "complete" == e || "uninitialized" == e
    }

    function a() {
        var e = v.shift();
        y = 1, e ? e.t ? h(function () {
            ("c" == e.t ? p.injectCss : p.injectJs)(e.s, 0, e.a, e.x, e.e, 1)
        }, 0) : (e(), a()) : y = 0
    }

    function l(e, n, i, o, r, l, c) {
        function d(t) {
            if (!f && s(u.readyState) && (x.r = f = 1, !y && a(), u.onload = u.onreadystatechange = null, t)) {
                "img" != e && h(function () {
                    w.removeChild(u)
                }, 50);
                for (var i in C[n]) C[n].hasOwnProperty(i) && C[n][i].onload()
            }
        }

        var c = c || p.errorTimeout, u = t.createElement(e), f = 0, m = 0, x = {t: i, s: n, e: r, a: l, x: c};
        1 === C[n] && (m = 1, C[n] = []), "object" == e ? u.data = n : (u.src = n, u.type = e), u.width = u.height = "0", u.onerror = u.onload = u.onreadystatechange = function () {
            d.call(this, m)
        }, v.splice(o, 0, x), "img" != e && (m || 2 === C[n] ? (w.insertBefore(u, b ? null : g), h(d, c)) : C[n].push(u))
    }

    function c(e, t, n, i, r) {
        return y = 0, t = t || "j", o(e) ? l("c" == t ? k : _, e, t, this.i++, n, i, r) : (v.splice(this.i++, 0, e), 1 == v.length && a()), this
    }

    function d() {
        var e = p;
        return e.loader = {load: c, i: 0}, e
    }

    var u, p, f = t.documentElement, h = e.setTimeout, g = t.getElementsByTagName("script")[0], m = {}.toString, v = [], y = 0,
        x = "MozAppearance" in f.style, b = x && !!t.createRange().compareNode, w = b ? f : g.parentNode,
        f = e.opera && "[object Opera]" == m.call(e.opera), f = !!t.attachEvent && !f, _ = x ? "object" : f ? "script" : "img",
        k = f ? "script" : _, T = Array.isArray || function (e) {
            return "[object Array]" == m.call(e)
        }, S = [], C = {}, E = {
            timeout: function (e, t) {
                return t.length && (e.timeout = t[0]), e
            }
        };
    p = function (e) {
        function t(e) {
            var t, n, i, e = e.split("!"), o = S.length, r = e.pop(), s = e.length, r = {url: r, origUrl: r, prefixes: e};
            for (n = 0; s > n; n++) i = e[n].split("="), (t = E[i.shift()]) && (r = t(r, i));
            for (n = 0; o > n; n++) r = S[n](r);
            return r
        }

        function s(e, o, r, s, a) {
            var l = t(e), c = l.autoCallback;
            l.url.split(".").pop().split("?").shift(), l.bypass || (o && (o = i(o) ? o : o[e] || o[s] || o[e.split("/").pop().split("?")[0]]), l.instead ? l.instead(e, o, r, s, a) : (C[l.url] ? l.noexec = !0 : C[l.url] = 1, r.load(l.url, l.forceCSS || !l.forceJS && "css" == l.url.split(".").pop().split("?").shift() ? "c" : n, l.noexec, l.attrs, l.timeout), (i(o) || i(c)) && r.load(function () {
                d(), o && o(l.origUrl, a, s), c && c(l.origUrl, a, s), C[l.url] = 2
            })))
        }

        function a(e, t) {
            function n(e, n) {
                if (e) {
                    if (o(e)) n || (u = function () {
                        var e = [].slice.call(arguments);
                        p.apply(this, e), f()
                    }), s(e, u, t, 0, c); else if (Object(e) === e) for (l in a = function () {
                        var t, n = 0;
                        for (t in e) e.hasOwnProperty(t) && n++;
                        return n
                    }(), e) e.hasOwnProperty(l) && (!n && !--a && (i(u) ? u = function () {
                        var e = [].slice.call(arguments);
                        p.apply(this, e), f()
                    } : u[l] = function (e) {
                        return function () {
                            var t = [].slice.call(arguments);
                            e && e.apply(this, t), f()
                        }
                    }(p[l])), s(e[l], u, t, l, c))
                } else !n && f()
            }

            var a, l, c = !!e.test, d = e.load || e.both, u = e.callback || r, p = u, f = e.complete || r;
            n(c ? e.yep : e.nope, !!d), d && n(d)
        }

        var l, c, u = this.yepnope.loader;
        if (o(e)) s(e, 0, u, 0); else if (T(e)) for (l = 0; l < e.length; l++) c = e[l], o(c) ? s(c, 0, u, 0) : T(c) ? p(c) : Object(c) === c && a(c, u); else Object(e) === e && a(e, u)
    }, p.addPrefix = function (e, t) {
        E[e] = t
    }, p.addFilter = function (e) {
        S.push(e)
    }, p.errorTimeout = 1e4, null == t.readyState && t.addEventListener && (t.readyState = "loading", t.addEventListener("DOMContentLoaded", u = function () {
        t.removeEventListener("DOMContentLoaded", u, 0), t.readyState = "complete"
    }, 0)), e.yepnope = d(), e.yepnope.executeStack = a, e.yepnope.injectJs = function (e, n, i, o, l, c) {
        var d, u, f = t.createElement("script"), o = o || p.errorTimeout;
        f.src = e;
        for (u in i) f.setAttribute(u, i[u]);
        n = c ? a : n || r, f.onreadystatechange = f.onload = function () {
            !d && s(f.readyState) && (d = 1, n(), f.onload = f.onreadystatechange = null)
        }, h(function () {
            d || (d = 1, n(1))
        }, o), l ? f.onload() : g.parentNode.insertBefore(f, g)
    }, e.yepnope.injectCss = function (e, n, i, o, s, l) {
        var c, o = t.createElement("link"), n = l ? a : n || r;
        o.href = e, o.rel = "stylesheet", o.type = "text/css";
        for (c in i) o.setAttribute(c, i[c]);
        s || (g.parentNode.insertBefore(o, g), h(n, 0))
    }
}(this, document), Modernizr.load = function () {
    yepnope.apply(window, [].slice.call(arguments, 0))
}, !function (e) {
    "use strict";
    e.matchMedia = e.matchMedia || function (e) {
        var t, n = e.documentElement, i = n.firstElementChild || n.firstChild, o = e.createElement("body"), r = e.createElement("div");
        return r.id = "mq-test-1", r.style.cssText = "position:absolute;top:-100em", o.style.background = "none", o.appendChild(r), function (e) {
            return r.innerHTML = '&shy;<style media="' + e + '"> #mq-test-1 { width: 42px; }</style>', n.insertBefore(o, i), t = 42 === r.offsetWidth, n.removeChild(o), {
                matches: t,
                media: e
            }
        }
    }(e.document)
}(this), function (e) {
    "use strict";

    function t() {
        w(!0)
    }

    var n = {};
    e.respond = n, n.update = function () {
    };
    var i = [], o = function () {
        var t = !1;
        try {
            t = new e.XMLHttpRequest
        } catch (n) {
            t = new e.ActiveXObject("Microsoft.XMLHTTP")
        }
        return function () {
            return t
        }
    }(), r = function (e, t) {
        var n = o();
        n && (n.open("GET", e, !0), n.onreadystatechange = function () {
            4 !== n.readyState || 200 !== n.status && 304 !== n.status || t(n.responseText)
        }, 4 !== n.readyState && n.send(null))
    }, s = function (e) {
        return e.replace(n.regex.minmaxwh, "").match(n.regex.other)
    };
    if (n.ajax = r, n.queue = i, n.unsupportedmq = s, n.regex = {
        media: /@media[^\{]+\{([^\{\}]*\{[^\}\{]*\})+/gi,
        keyframes: /@(?:\-(?:o|moz|webkit)\-)?keyframes[^\{]+\{(?:[^\{\}]*\{[^\}\{]*\})+[^\}]*\}/gi,
        comments: /\/\*[^*]*\*+([^/][^*]*\*+)*\//gi,
        urls: /(url\()['"]?([^\/\)'"][^:\)'"]+)['"]?(\))/g,
        findStyles: /@media *([^\{]+)\{([\S\s]+?)$/,
        only: /(only\s+)?([a-zA-Z]+)\s?/,
        minw: /\(\s*min\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/,
        maxw: /\(\s*max\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/,
        minmaxwh: /\(\s*m(in|ax)\-(height|width)\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/gi,
        other: /\([^\)]*\)/g
    }, n.mediaQueriesSupported = e.matchMedia && null !== e.matchMedia("only all") && e.matchMedia("only all").matches, !n.mediaQueriesSupported) {
        var a, l, c, d = e.document, u = d.documentElement, p = [], f = [], h = [], g = {}, m = 30,
            v = d.getElementsByTagName("head")[0] || u, y = d.getElementsByTagName("base")[0], x = v.getElementsByTagName("link"),
            b = function () {
                var e, t = d.createElement("div"), n = d.body, i = u.style.fontSize, o = n && n.style.fontSize, r = !1;
                return t.style.cssText = "position:absolute;font-size:1em;width:1em", n || (n = r = d.createElement("body"), n.style.background = "none"), u.style.fontSize = "100%", n.style.fontSize = "100%", n.appendChild(t), r && u.insertBefore(n, u.firstChild), e = t.offsetWidth, r ? u.removeChild(n) : n.removeChild(t), u.style.fontSize = i, o && (n.style.fontSize = o), e = c = parseFloat(e)
            }, w = function (t) {
                var n = "clientWidth", i = u[n], o = "CSS1Compat" === d.compatMode && i || d.body[n] || i, r = {}, s = x[x.length - 1],
                    g = (new Date).getTime();
                if (t && a && m > g - a) return e.clearTimeout(l), void (l = e.setTimeout(w, m));
                a = g;
                for (var y in p) if (p.hasOwnProperty(y)) {
                    var _ = p[y], k = _.minw, T = _.maxw, S = null === k, C = null === T, E = "em";
                    k && (k = parseFloat(k) * (k.indexOf(E) > -1 ? c || b() : 1)), T && (T = parseFloat(T) * (T.indexOf(E) > -1 ? c || b() : 1)), _.hasquery && (S && C || !(S || o >= k) || !(C || T >= o)) || (r[_.media] || (r[_.media] = []), r[_.media].push(f[_.rules]))
                }
                for (var $ in h) h.hasOwnProperty($) && h[$] && h[$].parentNode === v && v.removeChild(h[$]);
                h.length = 0;
                for (var A in r) if (r.hasOwnProperty(A)) {
                    var L = d.createElement("style"), M = r[A].join("\n");
                    L.type = "text/css", L.media = A, v.insertBefore(L, s.nextSibling), L.styleSheet ? L.styleSheet.cssText = M : L.appendChild(d.createTextNode(M)), h.push(L)
                }
            }, _ = function (e, t, i) {
                var o = e.replace(n.regex.comments, "").replace(n.regex.keyframes, "").match(n.regex.media), r = o && o.length || 0;
                t = t.substring(0, t.lastIndexOf("/"));
                var a = function (e) {
                    return e.replace(n.regex.urls, "$1" + t + "$2$3")
                }, l = !r && i;
                t.length && (t += "/"), l && (r = 1);
                for (var c = 0; r > c; c++) {
                    var d, u, h, g;
                    l ? (d = i, f.push(a(e))) : (d = o[c].match(n.regex.findStyles) && RegExp.$1, f.push(RegExp.$2 && a(RegExp.$2))), h = d.split(","), g = h.length;
                    for (var m = 0; g > m; m++) u = h[m], s(u) || p.push({
                        media: u.split("(")[0].match(n.regex.only) && RegExp.$2 || "all",
                        rules: f.length - 1,
                        hasquery: u.indexOf("(") > -1,
                        minw: u.match(n.regex.minw) && parseFloat(RegExp.$1) + (RegExp.$2 || ""),
                        maxw: u.match(n.regex.maxw) && parseFloat(RegExp.$1) + (RegExp.$2 || "")
                    })
                }
                w()
            }, k = function () {
                if (i.length) {
                    var t = i.shift();
                    r(t.href, function (n) {
                        _(n, t.href, t.media), g[t.href] = !0, e.setTimeout(function () {
                            k()
                        }, 0)
                    })
                }
            }, T = function () {
                for (var t = 0; t < x.length; t++) {
                    var n = x[t], o = n.href, r = n.media, s = n.rel && "stylesheet" === n.rel.toLowerCase();
                    o && s && !g[o] && (n.styleSheet && n.styleSheet.rawCssText ? (_(n.styleSheet.rawCssText, o, r), g[o] = !0) : (!/^([a-zA-Z:]*\/\/)/.test(o) && !y || o.replace(RegExp.$1, "").split("/")[0] === e.location.host) && ("//" === o.substring(0, 2) && (o = e.location.protocol + o), i.push({
                        href: o,
                        media: r
                    })))
                }
                k()
            };
        T(), n.update = T, n.getEmValue = b, e.addEventListener ? e.addEventListener("resize", t, !1) : e.attachEvent && e.attachEvent("onresize", t)
    }
}(this), !function (e) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(function (e) {
    "use strict";
    var t = window.Slick || {};
    t = function () {
        function t(t, i) {
            var o, r = this;
            r.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: e(t),
                appendDots: e(t),
                arrows: !0,
                asNavFor: null,
                prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="" tabindex="0" role="button"></button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="" tabindex="0" role="button"></button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function (e, t) {
                    return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">' + (t + 1) + "</button>"
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: .35,
                fade: !1,
                focusOnSelect: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                useTransform: !1,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3
            }, r.initials = {
                animating: !1,
                dragging: !1,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: !1,
                slideOffset: 0,
                swipeLeft: null,
                $list: null,
                touchObject: {},
                transformsEnabled: !1,
                unslicked: !1
            }, e.extend(r, r.initials), r.activeBreakpoint = null, r.animType = null, r.animProp = null, r.breakpoints = [], r.breakpointSettings = [], r.cssTransitions = !1, r.hidden = "hidden", r.paused = !1, r.positionProp = null, r.respondTo = null, r.rowCount = 1, r.shouldClick = !0, r.$slider = e(t), r.$slidesCache = null, r.transformType = null, r.transitionType = null, r.visibilityChange = "visibilitychange", r.windowWidth = 0, r.windowTimer = null, o = e(t).data("slick") || {}, r.options = e.extend({}, r.defaults, o, i), r.currentSlide = r.options.initialSlide, r.originalSettings = r.options, "undefined" != typeof document.mozHidden ? (r.hidden = "mozHidden", r.visibilityChange = "mozvisibilitychange") : "undefined" != typeof document.webkitHidden && (r.hidden = "webkitHidden", r.visibilityChange = "webkitvisibilitychange"), r.autoPlay = e.proxy(r.autoPlay, r), r.autoPlayClear = e.proxy(r.autoPlayClear, r), r.changeSlide = e.proxy(r.changeSlide, r), r.clickHandler = e.proxy(r.clickHandler, r), r.selectHandler = e.proxy(r.selectHandler, r), r.setPosition = e.proxy(r.setPosition, r), r.swipeHandler = e.proxy(r.swipeHandler, r), r.dragHandler = e.proxy(r.dragHandler, r), r.keyHandler = e.proxy(r.keyHandler, r), r.autoPlayIterator = e.proxy(r.autoPlayIterator, r), r.instanceUid = n++, r.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, r.registerBreakpoints(), r.init(!0), r.checkResponsive(!0)
        }

        var n = 0;
        return t
    }(), t.prototype.addSlide = t.prototype.slickAdd = function (t, n, i) {
        var o = this;
        if ("boolean" == typeof n) i = n, n = null; else if (0 > n || n >= o.slideCount) return !1;
        o.unload(), "number" == typeof n ? 0 === n && 0 === o.$slides.length ? e(t).appendTo(o.$slideTrack) : i ? e(t).insertBefore(o.$slides.eq(n)) : e(t).insertAfter(o.$slides.eq(n)) : i === !0 ? e(t).prependTo(o.$slideTrack) : e(t).appendTo(o.$slideTrack), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slides.each(function (t, n) {
            e(n).attr("data-slick-index", t)
        }), o.$slidesCache = o.$slides, o.reinit()
    }, t.prototype.animateHeight = function () {
        var e = this;
        if (1 === e.options.slidesToShow && e.options.adaptiveHeight === !0 && e.options.vertical === !1) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.animate({height: t}, e.options.speed)
        }
    }, t.prototype.animateSlide = function (t, n) {
        var i = {}, o = this;
        o.animateHeight(), o.options.rtl === !0 && o.options.vertical === !1 && (t = -t), o.transformsEnabled === !1 ? o.options.vertical === !1 ? o.$slideTrack.animate({left: t}, o.options.speed, o.options.easing, n) : o.$slideTrack.animate({top: t}, o.options.speed, o.options.easing, n) : o.cssTransitions === !1 ? (o.options.rtl === !0 && (o.currentLeft = -o.currentLeft), e({animStart: o.currentLeft}).animate({animStart: t}, {
            duration: o.options.speed,
            easing: o.options.easing,
            step: function (e) {
                e = Math.ceil(e), o.options.vertical === !1 ? (i[o.animType] = "translate(" + e + "px, 0px)", o.$slideTrack.css(i)) : (i[o.animType] = "translate(0px," + e + "px)", o.$slideTrack.css(i))
            },
            complete: function () {
                n && n.call()
            }
        })) : (o.applyTransition(), t = Math.ceil(t), o.options.vertical === !1 ? i[o.animType] = "translate3d(" + t + "px, 0px, 0px)" : i[o.animType] = "translate3d(0px," + t + "px, 0px)", o.$slideTrack.css(i), n && setTimeout(function () {
            o.disableTransition(), n.call()
        }, o.options.speed))
    }, t.prototype.asNavFor = function (t) {
        var n = this, i = n.options.asNavFor;
        i && null !== i && (i = e(i).not(n.$slider)), null !== i && "object" == typeof i && i.each(function () {
            var n = e(this).slick("getSlick");
            n.unslicked || n.slideHandler(t, !0)
        })
    }, t.prototype.applyTransition = function (e) {
        var t = this, n = {};
        t.options.fade === !1 ? n[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : n[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase, t.options.fade === !1 ? t.$slideTrack.css(n) : t.$slides.eq(e).css(n)
    }, t.prototype.autoPlay = function () {
        var e = this;
        e.autoPlayTimer && clearInterval(e.autoPlayTimer), e.slideCount > e.options.slidesToShow && e.paused !== !0 && (e.autoPlayTimer = setInterval(e.autoPlayIterator, e.options.autoplaySpeed))
    }, t.prototype.autoPlayClear = function () {
        var e = this;
        e.autoPlayTimer && clearInterval(e.autoPlayTimer)
    }, t.prototype.autoPlayIterator = function () {
        var e = this;
        e.options.infinite === !1 ? 1 === e.direction ? (e.currentSlide + 1 === e.slideCount - 1 && (e.direction = 0), e.slideHandler(e.currentSlide + e.options.slidesToScroll)) : (e.currentSlide - 1 === 0 && (e.direction = 1), e.slideHandler(e.currentSlide - e.options.slidesToScroll)) : e.slideHandler(e.currentSlide + e.options.slidesToScroll)
    }, t.prototype.buildArrows = function () {
        var t = this;
        t.options.arrows === !0 && (t.$prevArrow = e(t.options.prevArrow).addClass("slick-arrow"), t.$nextArrow = e(t.options.nextArrow).addClass("slick-arrow"), t.slideCount > t.options.slidesToShow ? (t.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.prependTo(t.options.appendArrows), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.appendTo(t.options.appendArrows), t.options.infinite !== !0 && t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : t.$prevArrow.add(t.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }))
    }, t.prototype.buildDots = function () {
        var t, n, i = this;
        if (i.options.dots === !0 && i.slideCount > i.options.slidesToShow) {
            for (n = '<ul class="' + i.options.dotsClass + '">', t = 0; t <= i.getDotCount(); t += 1) n += "<li>" + i.options.customPaging.call(this, i, t) + "</li>";
            n += "</ul>", i.$dots = e(n).appendTo(i.options.appendDots), i.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
        }
    }, t.prototype.buildOut = function () {
        var t = this;
        t.$slides = t.$slider.children(t.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), t.slideCount = t.$slides.length, t.$slides.each(function (t, n) {
            e(n).attr("data-slick-index", t).data("originalStyling", e(n).attr("style") || "")
        }), t.$slider.addClass("slick-slider"), t.$slideTrack = 0 === t.slideCount ? e('<div class="slick-track"/>').appendTo(t.$slider) : t.$slides.wrapAll('<div class="slick-track"/>').parent(), t.$list = t.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(), t.$slideTrack.css("opacity", 0), (t.options.centerMode === !0 || t.options.swipeToSlide === !0) && (t.options.slidesToScroll = 1), e("img[data-lazy]", t.$slider).not("[src]").addClass("slick-loading"), t.setupInfinite(), t.buildArrows(), t.buildDots(), t.updateDots(), t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0), t.options.draggable === !0 && t.$list.addClass("draggable")
    }, t.prototype.buildRows = function () {
        var e, t, n, i, o, r, s, a = this;
        if (i = document.createDocumentFragment(), r = a.$slider.children(), a.options.rows > 1) {
            for (s = a.options.slidesPerRow * a.options.rows, o = Math.ceil(r.length / s), e = 0; o > e; e++) {
                var l = document.createElement("div");
                for (t = 0; t < a.options.rows; t++) {
                    var c = document.createElement("div");
                    for (n = 0; n < a.options.slidesPerRow; n++) {
                        var d = e * s + (t * a.options.slidesPerRow + n);
                        r.get(d) && c.appendChild(r.get(d))
                    }
                    l.appendChild(c)
                }
                i.appendChild(l)
            }
            a.$slider.html(i), a.$slider.children().children().children().css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block"
            })
        }
    }, t.prototype.checkResponsive = function (t, n) {
        var i, o, r, s = this, a = !1, l = s.$slider.width(), c = window.innerWidth || e(window).width();
        if ("window" === s.respondTo ? r = c : "slider" === s.respondTo ? r = l : "min" === s.respondTo && (r = Math.min(c, l)), s.options.responsive && s.options.responsive.length && null !== s.options.responsive) {
            o = null;
            for (i in s.breakpoints) s.breakpoints.hasOwnProperty(i) && (s.originalSettings.mobileFirst === !1 ? r < s.breakpoints[i] && (o = s.breakpoints[i]) : r > s.breakpoints[i] && (o = s.breakpoints[i]));
            null !== o ? null !== s.activeBreakpoint ? (o !== s.activeBreakpoint || n) && (s.activeBreakpoint = o, "unslick" === s.breakpointSettings[o] ? s.unslick(o) : (s.options = e.extend({}, s.originalSettings, s.breakpointSettings[o]), t === !0 && (s.currentSlide = s.options.initialSlide), s.refresh(t)), a = o) : (s.activeBreakpoint = o, "unslick" === s.breakpointSettings[o] ? s.unslick(o) : (s.options = e.extend({}, s.originalSettings, s.breakpointSettings[o]), t === !0 && (s.currentSlide = s.options.initialSlide), s.refresh(t)), a = o) : null !== s.activeBreakpoint && (s.activeBreakpoint = null, s.options = s.originalSettings, t === !0 && (s.currentSlide = s.options.initialSlide), s.refresh(t), a = o), t || a === !1 || s.$slider.trigger("breakpoint", [s, a])
        }
    }, t.prototype.changeSlide = function (t, n) {
        var i, o, r, s = this, a = e(t.target);
        switch (a.is("a") && t.preventDefault(), a.is("li") || (a = a.closest("li")), r = s.slideCount % s.options.slidesToScroll !== 0, i = r ? 0 : (s.slideCount - s.currentSlide) % s.options.slidesToScroll, t.data.message) {
            case"previous":
                o = 0 === i ? s.options.slidesToScroll : s.options.slidesToShow - i, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide - o, !1, n);
                break;
            case"next":
                o = 0 === i ? s.options.slidesToScroll : i, s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide + o, !1, n);
                break;
            case"index":
                var l = 0 === t.data.index ? 0 : t.data.index || a.index() * s.options.slidesToScroll;
                s.slideHandler(s.checkNavigable(l), !1, n), a.children().trigger("focus");
                break;
            default:
                return
        }
    }, t.prototype.checkNavigable = function (e) {
        var t, n, i = this;
        if (t = i.getNavigableIndexes(), n = 0, e > t[t.length - 1]) e = t[t.length - 1]; else for (var o in t) {
            if (e < t[o]) {
                e = n;
                break
            }
            n = t[o]
        }
        return e
    }, t.prototype.cleanUpEvents = function () {
        var t = this;
        t.options.dots && null !== t.$dots && (e("li", t.$dots).off("click.slick", t.changeSlide), t.options.pauseOnDotsHover === !0 && t.options.autoplay === !0 && e("li", t.$dots).off("mouseenter.slick", e.proxy(t.setPaused, t, !0)).off("mouseleave.slick", e.proxy(t.setPaused, t, !1))), t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && (t.$prevArrow && t.$prevArrow.off("click.slick", t.changeSlide), t.$nextArrow && t.$nextArrow.off("click.slick", t.changeSlide)), t.$list.off("touchstart.slick mousedown.slick", t.swipeHandler), t.$list.off("touchmove.slick mousemove.slick", t.swipeHandler), t.$list.off("touchend.slick mouseup.slick", t.swipeHandler), t.$list.off("touchcancel.slick mouseleave.slick", t.swipeHandler), t.$list.off("click.slick", t.clickHandler), e(document).off(t.visibilityChange, t.visibility), t.$list.off("mouseenter.slick", e.proxy(t.setPaused, t, !0)), t.$list.off("mouseleave.slick", e.proxy(t.setPaused, t, !1)), t.options.accessibility === !0 && t.$list.off("keydown.slick", t.keyHandler), t.options.focusOnSelect === !0 && e(t.$slideTrack).children().off("click.slick", t.selectHandler), e(window).off("orientationchange.slick.slick-" + t.instanceUid, t.orientationChange), e(window).off("resize.slick.slick-" + t.instanceUid, t.resize), e("[draggable!=true]", t.$slideTrack).off("dragstart", t.preventDefault), e(window).off("load.slick.slick-" + t.instanceUid, t.setPosition), e(document).off("ready.slick.slick-" + t.instanceUid, t.setPosition)
    }, t.prototype.cleanUpRows = function () {
        var e, t = this;
        t.options.rows > 1 && (e = t.$slides.children().children(), e.removeAttr("style"), t.$slider.html(e))
    }, t.prototype.clickHandler = function (e) {
        var t = this;
        t.shouldClick === !1 && (e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault())
    }, t.prototype.destroy = function (t) {
        var n = this;
        n.autoPlayClear(), n.touchObject = {}, n.cleanUpEvents(), e(".slick-cloned", n.$slider).detach(), n.$dots && n.$dots.remove(), n.$prevArrow && n.$prevArrow.length && (n.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), n.htmlExpr.test(n.options.prevArrow) && n.$prevArrow.remove()), n.$nextArrow && n.$nextArrow.length && (n.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), n.htmlExpr.test(n.options.nextArrow) && n.$nextArrow.remove()), n.$slides && (n.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
            e(this).attr("style", e(this).data("originalStyling"))
        }), n.$slideTrack.children(this.options.slide).detach(), n.$slideTrack.detach(), n.$list.detach(), n.$slider.append(n.$slides)), n.cleanUpRows(), n.$slider.removeClass("slick-slider"), n.$slider.removeClass("slick-initialized"), n.unslicked = !0, t || n.$slider.trigger("destroy", [n])
    }, t.prototype.disableTransition = function (e) {
        var t = this, n = {};
        n[t.transitionType] = "", t.options.fade === !1 ? t.$slideTrack.css(n) : t.$slides.eq(e).css(n)
    }, t.prototype.fadeSlide = function (e, t) {
        var n = this;
        n.cssTransitions === !1 ? (n.$slides.eq(e).css({zIndex: n.options.zIndex}), n.$slides.eq(e).animate({opacity: 1}, n.options.speed, n.options.easing, t)) : (n.applyTransition(e), n.$slides.eq(e).css({
            opacity: 1,
            zIndex: n.options.zIndex
        }), t && setTimeout(function () {
            n.disableTransition(e), t.call()
        }, n.options.speed))
    }, t.prototype.fadeSlideOut = function (e) {
        var t = this;
        t.cssTransitions === !1 ? t.$slides.eq(e).animate({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }, t.options.speed, t.options.easing) : (t.applyTransition(e), t.$slides.eq(e).css({opacity: 0, zIndex: t.options.zIndex - 2}))
    }, t.prototype.filterSlides = t.prototype.slickFilter = function (e) {
        var t = this;
        null !== e && (t.$slidesCache = t.$slides, t.unload(), t.$slideTrack.children(this.options.slide).detach(), t.$slidesCache.filter(e).appendTo(t.$slideTrack), t.reinit())
    }, t.prototype.getCurrent = t.prototype.slickCurrentSlide = function () {
        var e = this;
        return e.currentSlide
    }, t.prototype.getDotCount = function () {
        var e = this, t = 0, n = 0, i = 0;
        if (e.options.infinite === !0) for (; t < e.slideCount;) ++i, t = n + e.options.slidesToScroll, n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow; else if (e.options.centerMode === !0) i = e.slideCount; else for (; t < e.slideCount;) ++i, t = n + e.options.slidesToScroll, n += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
        return i - 1
    }, t.prototype.getLeft = function (e) {
        var t, n, i, o = this, r = 0;
        return o.slideOffset = 0, n = o.$slides.first().outerHeight(!0), o.options.infinite === !0 ? (o.slideCount > o.options.slidesToShow && (o.slideOffset = o.slideWidth * o.options.slidesToShow * -1, r = n * o.options.slidesToShow * -1), o.slideCount % o.options.slidesToScroll !== 0 && e + o.options.slidesToScroll > o.slideCount && o.slideCount > o.options.slidesToShow && (e > o.slideCount ? (o.slideOffset = (o.options.slidesToShow - (e - o.slideCount)) * o.slideWidth * -1, r = (o.options.slidesToShow - (e - o.slideCount)) * n * -1) : (o.slideOffset = o.slideCount % o.options.slidesToScroll * o.slideWidth * -1, r = o.slideCount % o.options.slidesToScroll * n * -1))) : e + o.options.slidesToShow > o.slideCount && (o.slideOffset = (e + o.options.slidesToShow - o.slideCount) * o.slideWidth, r = (e + o.options.slidesToShow - o.slideCount) * n), o.slideCount <= o.options.slidesToShow && (o.slideOffset = 0, r = 0), o.options.centerMode === !0 && o.options.infinite === !0 ? o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2) - o.slideWidth : o.options.centerMode === !0 && (o.slideOffset = 0, o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2)), t = o.options.vertical === !1 ? e * o.slideWidth * -1 + o.slideOffset : e * n * -1 + r, o.options.variableWidth === !0 && (i = o.slideCount <= o.options.slidesToShow || o.options.infinite === !1 ? o.$slideTrack.children(".slick-slide").eq(e) : o.$slideTrack.children(".slick-slide").eq(e + o.options.slidesToShow),
            t = o.options.rtl === !0 ? i[0] ? -1 * (o.$slideTrack.width() - i[0].offsetLeft - i.width()) : 0 : i[0] ? -1 * i[0].offsetLeft : 0, o.options.centerMode === !0 && (i = o.slideCount <= o.options.slidesToShow || o.options.infinite === !1 ? o.$slideTrack.children(".slick-slide").eq(e) : o.$slideTrack.children(".slick-slide").eq(e + o.options.slidesToShow + 1), t = o.options.rtl === !0 ? i[0] ? -1 * (o.$slideTrack.width() - i[0].offsetLeft - i.width()) : 0 : i[0] ? -1 * i[0].offsetLeft : 0, t += (o.$list.width() - i.outerWidth()) / 2)), t
    }, t.prototype.getOption = t.prototype.slickGetOption = function (e) {
        var t = this;
        return t.options[e]
    }, t.prototype.getNavigableIndexes = function () {
        var e, t = this, n = 0, i = 0, o = [];
        for (t.options.infinite === !1 ? e = t.slideCount : (n = -1 * t.options.slidesToScroll, i = -1 * t.options.slidesToScroll, e = 2 * t.slideCount); e > n;) o.push(n), n = i + t.options.slidesToScroll, i += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
        return o
    }, t.prototype.getSlick = function () {
        return this
    }, t.prototype.getSlideCount = function () {
        var t, n, i, o = this;
        return i = o.options.centerMode === !0 ? o.slideWidth * Math.floor(o.options.slidesToShow / 2) : 0, o.options.swipeToSlide === !0 ? (o.$slideTrack.find(".slick-slide").each(function (t, r) {
            return r.offsetLeft - i + e(r).outerWidth() / 2 > -1 * o.swipeLeft ? (n = r, !1) : void 0
        }), t = Math.abs(e(n).attr("data-slick-index") - o.currentSlide) || 1) : o.options.slidesToScroll
    }, t.prototype.goTo = t.prototype.slickGoTo = function (e, t) {
        var n = this;
        n.changeSlide({data: {message: "index", index: parseInt(e)}}, t)
    }, t.prototype.init = function (t) {
        var n = this;
        e(n.$slider).hasClass("slick-initialized") || (e(n.$slider).addClass("slick-initialized"), n.buildRows(), n.buildOut(), n.setProps(), n.startLoad(), n.loadSlider(), n.initializeEvents(), n.updateArrows(), n.updateDots()), t && n.$slider.trigger("init", [n]), n.options.accessibility === !0 && n.initADA()
    }, t.prototype.initArrowEvents = function () {
        var e = this;
        e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow.on("click.slick", {message: "previous"}, e.changeSlide), e.$nextArrow.on("click.slick", {message: "next"}, e.changeSlide))
    }, t.prototype.initDotEvents = function () {
        var t = this;
        t.options.dots === !0 && t.slideCount > t.options.slidesToShow && e("li", t.$dots).on("click.slick", {message: "index"}, t.changeSlide), t.options.dots === !0 && t.options.pauseOnDotsHover === !0 && t.options.autoplay === !0 && e("li", t.$dots).on("mouseenter.slick", e.proxy(t.setPaused, t, !0)).on("mouseleave.slick", e.proxy(t.setPaused, t, !1))
    }, t.prototype.initializeEvents = function () {
        var t = this;
        t.initArrowEvents(), t.initDotEvents(), t.$list.on("touchstart.slick mousedown.slick", {action: "start"}, t.swipeHandler), t.$list.on("touchmove.slick mousemove.slick", {action: "move"}, t.swipeHandler), t.$list.on("touchend.slick mouseup.slick", {action: "end"}, t.swipeHandler), t.$list.on("touchcancel.slick mouseleave.slick", {action: "end"}, t.swipeHandler), t.$list.on("click.slick", t.clickHandler), e(document).on(t.visibilityChange, e.proxy(t.visibility, t)), t.$list.on("mouseenter.slick", e.proxy(t.setPaused, t, !0)), t.$list.on("mouseleave.slick", e.proxy(t.setPaused, t, !1)), t.options.accessibility === !0 && t.$list.on("keydown.slick", t.keyHandler), t.options.focusOnSelect === !0 && e(t.$slideTrack).children().on("click.slick", t.selectHandler), e(window).on("orientationchange.slick.slick-" + t.instanceUid, e.proxy(t.orientationChange, t)), e(window).on("resize.slick.slick-" + t.instanceUid, e.proxy(t.resize, t)), e("[draggable!=true]", t.$slideTrack).on("dragstart", t.preventDefault), e(window).on("load.slick.slick-" + t.instanceUid, t.setPosition), e(document).on("ready.slick.slick-" + t.instanceUid, t.setPosition)
    }, t.prototype.initUI = function () {
        var e = this;
        e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow.show(), e.$nextArrow.show()), e.options.dots === !0 && e.slideCount > e.options.slidesToShow && e.$dots.show(), e.options.autoplay === !0 && e.autoPlay()
    }, t.prototype.keyHandler = function (e) {
        var t = this;
        e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && t.options.accessibility === !0 ? t.changeSlide({data: {message: "previous"}}) : 39 === e.keyCode && t.options.accessibility === !0 && t.changeSlide({data: {message: "next"}}))
    }, t.prototype.lazyLoad = function () {
        function t(t) {
            e("img[data-lazy]", t).each(function () {
                var t = e(this), n = e(this).attr("data-lazy"), i = document.createElement("img");
                i.onload = function () {
                    t.animate({opacity: 0}, 100, function () {
                        t.attr("src", n).animate({opacity: 1}, 200, function () {
                            t.removeAttr("data-lazy").removeClass("slick-loading")
                        })
                    })
                }, i.src = n
            })
        }

        var n, i, o, r, s = this;
        s.options.centerMode === !0 ? s.options.infinite === !0 ? (o = s.currentSlide + (s.options.slidesToShow / 2 + 1), r = o + s.options.slidesToShow + 2) : (o = Math.max(0, s.currentSlide - (s.options.slidesToShow / 2 + 1)), r = 2 + (s.options.slidesToShow / 2 + 1) + s.currentSlide) : (o = s.options.infinite ? s.options.slidesToShow + s.currentSlide : s.currentSlide, r = o + s.options.slidesToShow, s.options.fade === !0 && (o > 0 && o--, r <= s.slideCount && r++)), n = s.$slider.find(".slick-slide").slice(o, r), t(n), s.slideCount <= s.options.slidesToShow ? (i = s.$slider.find(".slick-slide"), t(i)) : s.currentSlide >= s.slideCount - s.options.slidesToShow ? (i = s.$slider.find(".slick-cloned").slice(0, s.options.slidesToShow), t(i)) : 0 === s.currentSlide && (i = s.$slider.find(".slick-cloned").slice(-1 * s.options.slidesToShow), t(i))
    }, t.prototype.loadSlider = function () {
        var e = this;
        e.setPosition(), e.$slideTrack.css({opacity: 1}), e.$slider.removeClass("slick-loading"), e.initUI(), "progressive" === e.options.lazyLoad && e.progressiveLazyLoad()
    }, t.prototype.next = t.prototype.slickNext = function () {
        var e = this;
        e.changeSlide({data: {message: "next"}})
    }, t.prototype.orientationChange = function () {
        var e = this;
        e.checkResponsive(), e.setPosition()
    }, t.prototype.pause = t.prototype.slickPause = function () {
        var e = this;
        e.autoPlayClear(), e.paused = !0
    }, t.prototype.play = t.prototype.slickPlay = function () {
        var e = this;
        e.paused = !1, e.autoPlay()
    }, t.prototype.postSlide = function (e) {
        var t = this;
        t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.setPosition(), t.swipeLeft = null, t.options.autoplay === !0 && t.paused === !1 && t.autoPlay(), t.options.accessibility === !0 && t.initADA()
    }, t.prototype.prev = t.prototype.slickPrev = function () {
        var e = this;
        e.changeSlide({data: {message: "previous"}})
    }, t.prototype.preventDefault = function (e) {
        e.preventDefault()
    }, t.prototype.progressiveLazyLoad = function () {
        var t, n, i = this;
        t = e("img[data-lazy]", i.$slider).length, t > 0 && (n = e("img[data-lazy]", i.$slider).first(), n.attr("src", null), n.attr("src", n.attr("data-lazy")).removeClass("slick-loading").load(function () {
            n.removeAttr("data-lazy"), i.progressiveLazyLoad(), i.options.adaptiveHeight === !0 && i.setPosition()
        }).error(function () {
            n.removeAttr("data-lazy"), i.progressiveLazyLoad()
        }))
    }, t.prototype.refresh = function (t) {
        var n, i, o = this;
        i = o.slideCount - o.options.slidesToShow, o.options.infinite || (o.slideCount <= o.options.slidesToShow ? o.currentSlide = 0 : o.currentSlide > i && (o.currentSlide = i)), n = o.currentSlide, o.destroy(!0), e.extend(o, o.initials, {currentSlide: n}), o.init(), t || o.changeSlide({
            data: {
                message: "index",
                index: n
            }
        }, !1)
    }, t.prototype.registerBreakpoints = function () {
        var t, n, i, o = this, r = o.options.responsive || null;
        if ("array" === e.type(r) && r.length) {
            o.respondTo = o.options.respondTo || "window";
            for (t in r) if (i = o.breakpoints.length - 1, n = r[t].breakpoint, r.hasOwnProperty(t)) {
                for (; i >= 0;) o.breakpoints[i] && o.breakpoints[i] === n && o.breakpoints.splice(i, 1), i--;
                o.breakpoints.push(n), o.breakpointSettings[n] = r[t].settings
            }
            o.breakpoints.sort(function (e, t) {
                return o.options.mobileFirst ? e - t : t - e
            })
        }
    }, t.prototype.reinit = function () {
        var t = this;
        t.$slides = t.$slideTrack.children(t.options.slide).addClass("slick-slide"), t.slideCount = t.$slides.length, t.currentSlide >= t.slideCount && 0 !== t.currentSlide && (t.currentSlide = t.currentSlide - t.options.slidesToScroll), t.slideCount <= t.options.slidesToShow && (t.currentSlide = 0), t.registerBreakpoints(), t.setProps(), t.setupInfinite(), t.buildArrows(), t.updateArrows(), t.initArrowEvents(), t.buildDots(), t.updateDots(), t.initDotEvents(), t.checkResponsive(!1, !0), t.options.focusOnSelect === !0 && e(t.$slideTrack).children().on("click.slick", t.selectHandler), t.setSlideClasses(0), t.setPosition(), t.$slider.trigger("reInit", [t]), t.options.autoplay === !0 && t.focusHandler()
    }, t.prototype.resize = function () {
        var t = this;
        e(window).width() !== t.windowWidth && (clearTimeout(t.windowDelay), t.windowDelay = window.setTimeout(function () {
            t.windowWidth = e(window).width(), t.checkResponsive(), t.unslicked || t.setPosition()
        }, 50))
    }, t.prototype.removeSlide = t.prototype.slickRemove = function (e, t, n) {
        var i = this;
        return "boolean" == typeof e ? (t = e, e = t === !0 ? 0 : i.slideCount - 1) : e = t === !0 ? --e : e, i.slideCount < 1 || 0 > e || e > i.slideCount - 1 ? !1 : (i.unload(), n === !0 ? i.$slideTrack.children().remove() : i.$slideTrack.children(this.options.slide).eq(e).remove(), i.$slides = i.$slideTrack.children(this.options.slide), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.append(i.$slides), i.$slidesCache = i.$slides, void i.reinit())
    }, t.prototype.setCSS = function (e) {
        var t, n, i = this, o = {};
        i.options.rtl === !0 && (e = -e), t = "left" == i.positionProp ? Math.ceil(e) + "px" : "0px", n = "top" == i.positionProp ? Math.ceil(e) + "px" : "0px", o[i.positionProp] = e, i.transformsEnabled === !1 ? i.$slideTrack.css(o) : (o = {}, i.cssTransitions === !1 ? (o[i.animType] = "translate(" + t + ", " + n + ")", i.$slideTrack.css(o)) : (o[i.animType] = "translate3d(" + t + ", " + n + ", 0px)", i.$slideTrack.css(o)))
    }, t.prototype.setDimensions = function () {
        var e = this;
        e.options.vertical === !1 ? e.options.centerMode === !0 && e.$list.css({padding: "0px " + e.options.centerPadding}) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow), e.options.centerMode === !0 && e.$list.css({padding: e.options.centerPadding + " 0px"})), e.listWidth = e.$list.width(), e.listHeight = e.$list.height(), e.options.vertical === !1 && e.options.variableWidth === !1 ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow), e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : e.options.variableWidth === !0 ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth), e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
        var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
        e.options.variableWidth === !1 && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t)
    }, t.prototype.setFade = function () {
        var t, n = this;
        n.$slides.each(function (i, o) {
            t = n.slideWidth * i * -1, n.options.rtl === !0 ? e(o).css({
                position: "relative",
                right: t,
                top: 0,
                zIndex: n.options.zIndex - 2,
                opacity: 0
            }) : e(o).css({position: "relative", left: t, top: 0, zIndex: n.options.zIndex - 2, opacity: 0})
        }), n.$slides.eq(n.currentSlide).css({zIndex: n.options.zIndex - 1, opacity: 1})
    }, t.prototype.setHeight = function () {
        var e = this;
        if (1 === e.options.slidesToShow && e.options.adaptiveHeight === !0 && e.options.vertical === !1) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.css("height", t)
        }
    }, t.prototype.setOption = t.prototype.slickSetOption = function (t, n, i) {
        var o, r, s = this;
        if ("responsive" === t && "array" === e.type(n)) for (r in n) if ("array" !== e.type(s.options.responsive)) s.options.responsive = [n[r]]; else {
            for (o = s.options.responsive.length - 1; o >= 0;) s.options.responsive[o].breakpoint === n[r].breakpoint && s.options.responsive.splice(o, 1), o--;
            s.options.responsive.push(n[r])
        } else s.options[t] = n;
        i === !0 && (s.unload(), s.reinit())
    }, t.prototype.setPosition = function () {
        var e = this;
        e.setDimensions(), e.setHeight(), e.options.fade === !1 ? e.setCSS(e.getLeft(e.currentSlide)) : e.setFade(), e.$slider.trigger("setPosition", [e])
    }, t.prototype.setProps = function () {
        var e = this, t = document.body.style;
        e.positionProp = e.options.vertical === !0 ? "top" : "left", "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"), (void 0 !== t.WebkitTransition || void 0 !== t.MozTransition || void 0 !== t.msTransition) && e.options.useCSS === !0 && (e.cssTransitions = !0), e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex), void 0 !== t.OTransform && (e.animType = "OTransform", e.transformType = "-o-transform", e.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.MozTransform && (e.animType = "MozTransform", e.transformType = "-moz-transform", e.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)), void 0 !== t.webkitTransform && (e.animType = "webkitTransform", e.transformType = "-webkit-transform", e.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.msTransform && (e.animType = "msTransform", e.transformType = "-ms-transform", e.transitionType = "msTransition", void 0 === t.msTransform && (e.animType = !1)), void 0 !== t.transform && e.animType !== !1 && (e.animType = "transform", e.transformType = "transform", e.transitionType = "transition"), e.transformsEnabled = e.options.useTransform && null !== e.animType && e.animType !== !1
    }, t.prototype.setSlideClasses = function (e) {
        var t, n, i, o, r = this;
        n = r.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), r.$slides.eq(e).addClass("slick-current"), r.options.centerMode === !0 ? (t = Math.floor(r.options.slidesToShow / 2), r.options.infinite === !0 && (e >= t && e <= r.slideCount - 1 - t ? r.$slides.slice(e - t, e + t + 1).addClass("slick-active").attr("aria-hidden", "false") : (i = r.options.slidesToShow + e, n.slice(i - t + 1, i + t + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === e ? n.eq(n.length - 1 - r.options.slidesToShow).addClass("slick-center") : e === r.slideCount - 1 && n.eq(r.options.slidesToShow).addClass("slick-center")), r.$slides.eq(e).addClass("slick-center")) : e >= 0 && e <= r.slideCount - r.options.slidesToShow ? r.$slides.slice(e, e + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : n.length <= r.options.slidesToShow ? n.addClass("slick-active").attr("aria-hidden", "false") : (o = r.slideCount % r.options.slidesToShow, i = r.options.infinite === !0 ? r.options.slidesToShow + e : e, r.options.slidesToShow == r.options.slidesToScroll && r.slideCount - e < r.options.slidesToShow ? n.slice(i - (r.options.slidesToShow - o), i + o).addClass("slick-active").attr("aria-hidden", "false") : n.slice(i, i + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")), "ondemand" === r.options.lazyLoad && r.lazyLoad()
    }, t.prototype.setupInfinite = function () {
        var t, n, i, o = this;
        if (o.options.fade === !0 && (o.options.centerMode = !1), o.options.infinite === !0 && o.options.fade === !1 && (n = null, o.slideCount > o.options.slidesToShow)) {
            for (i = o.options.centerMode === !0 ? o.options.slidesToShow + 1 : o.options.slidesToShow, t = o.slideCount; t > o.slideCount - i; t -= 1) n = t - 1, e(o.$slides[n]).clone(!0).attr("id", "").attr("data-slick-index", n - o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");
            for (t = 0; i > t; t += 1) n = t, e(o.$slides[n]).clone(!0).attr("id", "").attr("data-slick-index", n + o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");
            o.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
                e(this).attr("id", "")
            })
        }
    }, t.prototype.setPaused = function (e) {
        var t = this;
        t.options.autoplay === !0 && t.options.pauseOnHover === !0 && (t.paused = e, e ? t.autoPlayClear() : t.autoPlay())
    }, t.prototype.selectHandler = function (t) {
        var n = this, i = e(t.target).is(".slick-slide") ? e(t.target) : e(t.target).parents(".slick-slide"),
            o = parseInt(i.attr("data-slick-index"));
        return o || (o = 0), n.slideCount <= n.options.slidesToShow ? (n.setSlideClasses(o), void n.asNavFor(o)) : void n.slideHandler(o)
    }, t.prototype.slideHandler = function (e, t, n) {
        var i, o, r, s, a = null, l = this;
        return t = t || !1, l.animating === !0 && l.options.waitForAnimate === !0 || l.options.fade === !0 && l.currentSlide === e || l.slideCount <= l.options.slidesToShow ? void 0 : (t === !1 && l.asNavFor(e), i = e, a = l.getLeft(i), s = l.getLeft(l.currentSlide), l.currentLeft = null === l.swipeLeft ? s : l.swipeLeft, l.options.infinite === !1 && l.options.centerMode === !1 && (0 > e || e > l.getDotCount() * l.options.slidesToScroll) ? void (l.options.fade === !1 && (i = l.currentSlide, n !== !0 ? l.animateSlide(s, function () {
            l.postSlide(i)
        }) : l.postSlide(i))) : l.options.infinite === !1 && l.options.centerMode === !0 && (0 > e || e > l.slideCount - l.options.slidesToScroll) ? void (l.options.fade === !1 && (i = l.currentSlide, n !== !0 ? l.animateSlide(s, function () {
            l.postSlide(i)
        }) : l.postSlide(i))) : (l.options.autoplay === !0 && clearInterval(l.autoPlayTimer), o = 0 > i ? l.slideCount % l.options.slidesToScroll !== 0 ? l.slideCount - l.slideCount % l.options.slidesToScroll : l.slideCount + i : i >= l.slideCount ? l.slideCount % l.options.slidesToScroll !== 0 ? 0 : i - l.slideCount : i, l.animating = !0, l.$slider.trigger("beforeChange", [l, l.currentSlide, o]), r = l.currentSlide, l.currentSlide = o, l.setSlideClasses(l.currentSlide), l.updateDots(), l.updateArrows(), l.options.fade === !0 ? (n !== !0 ? (l.fadeSlideOut(r), l.fadeSlide(o, function () {
            l.postSlide(o)
        })) : l.postSlide(o), void l.animateHeight()) : void (n !== !0 ? l.animateSlide(a, function () {
            l.postSlide(o)
        }) : l.postSlide(o))))
    }, t.prototype.startLoad = function () {
        var e = this;
        e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(), e.$nextArrow.hide()), e.options.dots === !0 && e.slideCount > e.options.slidesToShow && e.$dots.hide(), e.$slider.addClass("slick-loading")
    }, t.prototype.swipeDirection = function () {
        var e, t, n, i, o = this;
        return e = o.touchObject.startX - o.touchObject.curX, t = o.touchObject.startY - o.touchObject.curY, n = Math.atan2(t, e), i = Math.round(180 * n / Math.PI), 0 > i && (i = 360 - Math.abs(i)), 45 >= i && i >= 0 ? o.options.rtl === !1 ? "left" : "right" : 360 >= i && i >= 315 ? o.options.rtl === !1 ? "left" : "right" : i >= 135 && 225 >= i ? o.options.rtl === !1 ? "right" : "left" : o.options.verticalSwiping === !0 ? i >= 35 && 135 >= i ? "left" : "right" : "vertical"
    }, t.prototype.swipeEnd = function () {
        var e, t = this;
        if (t.dragging = !1, t.shouldClick = !(t.touchObject.swipeLength > 10), void 0 === t.touchObject.curX) return !1;
        if (t.touchObject.edgeHit === !0 && t.$slider.trigger("edge", [t, t.swipeDirection()]), t.touchObject.swipeLength >= t.touchObject.minSwipe) switch (t.swipeDirection()) {
            case"left":
                e = t.options.swipeToSlide ? t.checkNavigable(t.currentSlide + t.getSlideCount()) : t.currentSlide + t.getSlideCount(), t.slideHandler(e), t.currentDirection = 0, t.touchObject = {}, t.$slider.trigger("swipe", [t, "left"]);
                break;
            case"right":
                e = t.options.swipeToSlide ? t.checkNavigable(t.currentSlide - t.getSlideCount()) : t.currentSlide - t.getSlideCount(), t.slideHandler(e), t.currentDirection = 1, t.touchObject = {}, t.$slider.trigger("swipe", [t, "right"])
        } else t.touchObject.startX !== t.touchObject.curX && (t.slideHandler(t.currentSlide), t.touchObject = {})
    }, t.prototype.swipeHandler = function (e) {
        var t = this;
        if (!(t.options.swipe === !1 || "ontouchend" in document && t.options.swipe === !1 || t.options.draggable === !1 && -1 !== e.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1, t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, t.options.verticalSwiping === !0 && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold), e.data.action) {
            case"start":
                t.swipeStart(e);
                break;
            case"move":
                t.swipeMove(e);
                break;
            case"end":
                t.swipeEnd(e)
        }
    }, t.prototype.swipeMove = function (e) {
        var t, n, i, o, r, s = this;
        return r = void 0 !== e.originalEvent ? e.originalEvent.touches : null, !s.dragging || r && 1 !== r.length ? !1 : (t = s.getLeft(s.currentSlide), s.touchObject.curX = void 0 !== r ? r[0].pageX : e.clientX, s.touchObject.curY = void 0 !== r ? r[0].pageY : e.clientY, s.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(s.touchObject.curX - s.touchObject.startX, 2))), s.options.verticalSwiping === !0 && (s.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(s.touchObject.curY - s.touchObject.startY, 2)))), n = s.swipeDirection(), "vertical" !== n ? (void 0 !== e.originalEvent && s.touchObject.swipeLength > 4 && e.preventDefault(), o = (s.options.rtl === !1 ? 1 : -1) * (s.touchObject.curX > s.touchObject.startX ? 1 : -1), s.options.verticalSwiping === !0 && (o = s.touchObject.curY > s.touchObject.startY ? 1 : -1), i = s.touchObject.swipeLength, s.touchObject.edgeHit = !1, s.options.infinite === !1 && (0 === s.currentSlide && "right" === n || s.currentSlide >= s.getDotCount() && "left" === n) && (i = s.touchObject.swipeLength * s.options.edgeFriction, s.touchObject.edgeHit = !0), s.options.vertical === !1 ? s.swipeLeft = t + i * o : s.swipeLeft = t + i * (s.$list.height() / s.listWidth) * o, s.options.verticalSwiping === !0 && (s.swipeLeft = t + i * o), s.options.fade === !0 || s.options.touchMove === !1 ? !1 : s.animating === !0 ? (s.swipeLeft = null, !1) : void s.setCSS(s.swipeLeft)) : void 0)
    }, t.prototype.swipeStart = function (e) {
        var t, n = this;
        return 1 !== n.touchObject.fingerCount || n.slideCount <= n.options.slidesToShow ? (n.touchObject = {}, !1) : (void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (t = e.originalEvent.touches[0]), n.touchObject.startX = n.touchObject.curX = void 0 !== t ? t.pageX : e.clientX, n.touchObject.startY = n.touchObject.curY = void 0 !== t ? t.pageY : e.clientY, void (n.dragging = !0))
    }, t.prototype.unfilterSlides = t.prototype.slickUnfilter = function () {
        var e = this;
        null !== e.$slidesCache && (e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.appendTo(e.$slideTrack), e.reinit())
    }, t.prototype.unload = function () {
        var t = this;
        e(".slick-cloned", t.$slider).remove(), t.$dots && t.$dots.remove(), t.$prevArrow && t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove(), t.$nextArrow && t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove(), t.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    }, t.prototype.unslick = function (e) {
        var t = this;
        t.$slider.trigger("unslick", [t, e]), t.destroy()
    }, t.prototype.updateArrows = function () {
        var e, t = this;
        e = Math.floor(t.options.slidesToShow / 2), t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && !t.options.infinite && (t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === t.currentSlide ? (t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - t.options.slidesToShow && t.options.centerMode === !1 ? (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - 1 && t.options.centerMode === !0 && (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
    }, t.prototype.updateDots = function () {
        var e = this;
        null !== e.$dots && (e.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), e.$dots.find("li").eq(Math.floor(e.currentSlide / e.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
    }, t.prototype.visibility = function () {
        var e = this;
        document[e.hidden] ? (e.paused = !0, e.autoPlayClear()) : e.options.autoplay === !0 && (e.paused = !1, e.autoPlay())
    }, t.prototype.initADA = function () {
        var t = this;
        t.$slides.add(t.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({tabindex: "-1"}), t.$slideTrack.attr("role", "listbox"), t.$slides.not(t.$slideTrack.find(".slick-cloned")).each(function (n) {
            e(this).attr({role: "option", "aria-describedby": "slick-slide" + t.instanceUid + n})
        }), null !== t.$dots && t.$dots.attr("role", "tablist").find("li").each(function (n) {
            e(this).attr({
                role: "presentation",
                "aria-selected": "false",
                "aria-controls": "navigation" + t.instanceUid + n,
                id: "slick-slide" + t.instanceUid + n
            })
        }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"), t.activateADA()
    }, t.prototype.activateADA = function () {
        var e = this;
        e.$slideTrack.find(".slick-active").attr({"aria-hidden": "false"}).find("a, input, button, select").attr({tabindex: "0"})
    }, t.prototype.focusHandler = function () {
        var t = this;
        t.$slider.on("focus.slick blur.slick", "*", function (n) {
            n.stopImmediatePropagation();
            var i = e(this);
            setTimeout(function () {
                t.isPlay && (i.is(":focus") ? (t.autoPlayClear(), t.paused = !0) : (t.paused = !1, t.autoPlay()))
            }, 0)
        })
    }, e.fn.slick = function () {
        var e, n, i = this, o = arguments[0], r = Array.prototype.slice.call(arguments, 1), s = i.length;
        for (e = 0; s > e; e++) if ("object" == typeof o || "undefined" == typeof o ? i[e].slick = new t(i[e], o) : n = i[e].slick[o].apply(i[e].slick, r), "undefined" != typeof n) return n;
        return i
    }
});
var amenuOptions = {menuId: "accordion-left", linkIdToMenuHtml: null, expand: "single", speed: 200, license: "2a8e9"},
    amenu = new McAcdnMenu(amenuOptions);