Rails.application.routes.draw do
  get 'welcome/index'

  resources :actions
  resources :price
  resources :contacts

  root 'welcome#index'
end
